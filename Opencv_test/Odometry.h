
#ifndef ODOMETRY_H
#define ODOMETRY_H
#include "opencv2/core.hpp"
#include "opencv2/core/utility.hpp"
int GetCorrectRotT(cv::Mat inliers1, cv::Mat inliers2,
				   cv::Mat R1, cv::Mat R2, cv::Mat t1, cv::Mat t2, cv::Mat& correctR, cv::Mat& correctt);
int matchFeatures(std::vector<cv::KeyPoint>& keypoints1, cv::Mat& descriptor1,
				  std::vector<cv::KeyPoint>& keypoints2, cv::Mat& descriptor2, std::vector<cv::DMatch>& matches);
int extractGoodFeatures(cv::Mat img, std::vector<cv::KeyPoint>& keypoints, cv::Mat& descriptors);
int decomposeAndCheckE(cv::Mat E, cv::Mat& R1, cv::Mat& R2, cv::Mat& t);
int getRelativePose(std::vector<cv::Point2f> keypoints1, std::vector<cv::Point2f> keypoints2, 
					float focal, cv::Point2d pp, cv::Mat& R, cv::Mat& t, cv::Mat& mask);
int dosingleMatchTestod();
int doOdometry();
int doOdometryWithMatching();

#endif