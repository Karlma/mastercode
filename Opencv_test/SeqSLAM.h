#ifndef SEQ_SLAM_H
#define SEQ_SLAM_H

#include "opencv2/core.hpp"
#include "opencv2/core/utility.hpp"

int preprocessImage(cv::Mat img, cv::Mat& out);
double compareImages(cv::Mat img1, cv::Mat img2);
int compareToOld(cv::Mat img, std::vector<cv::Mat>& prevImgs, cv::Mat& vect);
int compSystem(cv::Mat img, std::vector<cv::Mat>& prevImgs, cv::Mat& system, int ii);
int getMatch(cv::Mat img, std::vector<cv::Mat>& prevImgs, cv::Mat& system, int ii, int minlookback);
int seq_run_on_sequence(char *imagepatch, int start, int end);

#endif // DEBUG