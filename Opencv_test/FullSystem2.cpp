#include <stdio.h>
#include <iostream>
#include <fstream>
#include <chrono>
#include "opencv2/core.hpp"
#include "opencv2/core/utility.hpp"
#include "opencv2/features2d.hpp"
#include "opencv2/xfeatures2d.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/calib3d.hpp"
#include "opencv2/video/tracking.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "Odometry.h"
#include "SeqSLAM.h"
#include "Tests.h"
#include "FullSystem1.h"
#include "mylib.h"

using namespace cv;
using namespace std;
using namespace std::chrono;

static vector<int> va;
static vector<int> vb;
static Ptr<ORB> detector = ORB::create(1000);
//static FastFeatureDetector detectorf = FastFeatureDetector();
static vector<Point3d> scores;
static vector<Mat> camMats;
static ofstream myfile;
static ofstream myfile2;
static Mat traj = Mat::zeros(800, 800, CV_8UC3);
static int xoffset = 280;// 280;
static int yoffset = 580;//580;
static int num_matches;
static double focal = 718.8560;
static Point2d PP(607.1928, 185.2157);

static char imagepath[100];

int test_relative_match_init()
{
	lib_read_matches(va, vb, "C:/Matlab/matches_00_25100.csv");
	num_matches = (int)va.size();
	lib_read_gps_data_kitti(scores, "C:/Matlab/datasets/kitti/data_odometry_poses/dataset/poses/00.txt");
	lib_read_kitti_poses(camMats, "C:/Matlab/datasets/kitti/data_odometry_poses/dataset/poses/00.txt");
	myfile.open("C:/Matlab/relativeposes_on_matches.csv");
	myfile2.open("C:/Matlab/relativeposes_on_matches2.csv");
	sprintf_s(imagepath, "C:/Matlab/datasets/kitti/00/image_0");
	return 1;
}

int test_relative_pose_using_matches_matching()
{
	char path[100];
	sprintf_s(path, "%s/%06d.png", imagepath, 1);
	Mat imga = imread(path, IMREAD_GRAYSCALE);
	Mat combined = Mat(imga.rows * 2, imga.cols, imga.type());
	for (int i = 0; i < num_matches; i++)
	{
		high_resolution_clock::time_point t1 = high_resolution_clock::now();
		int now = va[i];
		int match = vb[i];
		sprintf_s(path, "%s/%06d.png", imagepath, now);
		imga = imread(path, IMREAD_GRAYSCALE);
		sprintf_s(path, "%s/%06d.png", imagepath, match);
		//sprintf_s(path, "C:/Matlab/datasets/kitti/00/image_1/%06d.png", now);
		Mat imgb = imread(path, IMREAD_GRAYSCALE);
		//lib_combine_two_images(imga, imgb, combined);
		
		vector<KeyPoint> kp1;
		vector<KeyPoint> kp2;
		Mat desc1;
		Mat desc2;
		lib_extract_better_features(detector, imga, kp1, 31, 16, 6, 3, 8);
		cout << "kp1: "<<kp1.size() << "\n";
		lib_extract_better_features(detector, imgb, kp2, 31, 16, 6, 3, 8);
		cout << "kp2: " << kp2.size() << "\n";
		high_resolution_clock::time_point t2 = high_resolution_clock::now();
		auto duratation = duration_cast<milliseconds>(t2 - t1).count();
		cout << "grid_features: " << duratation << "ms\n";
		t1 = high_resolution_clock::now();
		detector->compute(imga, kp1, desc1);
		detector->compute(imgb, kp2, desc2);
		t2 = high_resolution_clock::now();
		duratation = duration_cast<milliseconds>(t2 - t1).count();
		cout << "descriptor: " << duratation << "ms\n";
		t1 = high_resolution_clock::now();
		vector<DMatch> matches;
		lib_match_descriptors(desc1, desc2, matches);
		lib_filter_matches(kp1, kp2, matches);
		t2 = high_resolution_clock::now();
		duratation = duration_cast<milliseconds>(t2 - t1).count();
		cout << "Matching: " << duratation << "ms\n";
		t1 = high_resolution_clock::now();
		Mat t, R, mask;
		vector<Point2f> good_points1;
		vector<Point2f> good_points2;
		KeyPoint::convert(kp1, good_points1);
		KeyPoint::convert(kp2, good_points2);
		int inliers = lib_get_relative_pose_from_points(good_points1, good_points2, focal, PP, R, t, mask);
		
		vector<KeyPoint> kpf1;
		vector<KeyPoint> kpf2;
		for (int i = 0; i < mask.rows; i++)
		{
			if (mask.at<char>(i, 0))
			{
				kpf1.push_back(cv::KeyPoint(good_points1[i], 1));
				kpf2.push_back(cv::KeyPoint(good_points2[i], 1));
			}
		}
		t2 = high_resolution_clock::now();
		duratation = duration_cast<milliseconds>(t2 - t1).count();
		cout << "RelativePose: " << duratation << "ms\n";
		t1 = high_resolution_clock::now();
		//drawKeypoints(imga, kpf1, imga);
		//drawKeypoints(imgb, kpf2, imgb);
		//lib_combine_two_images(imga, imgb, combined);
		//imshow("Match_a", imga);
		//imshow("Match_b", imgb);
		cout << "Inliers: " << inliers << endl;
		Mat PP = camMats[match];
		Mat RK = PP.rowRange(0, 3).colRange(0, 3);
		Point2d tt2d(scores[match].x, scores[match].z);
		Point2d now2d(scores[now].x, scores[now].z);
		double scale = norm(tt2d - now2d);
		//t.at<double>(2, 0) = -t.at<double>(2, 0);
		//Mat T = t*scale;
		//T.push_back(Mat::ones(1, 1, CV_64F));
		//T = PP*T;
		//cout << "t from recover" << t << endl;
		//cout << "T from recover" << T << endl;
		//cout << "now" << scores[now] << endl;
		//cout << "match" << scores[match] << endl;
		//pt = pt * scale;
		//Mat t2 = RK*(t * scale);
		//for stereo
		//Mat t2 = RK*(t * scale);
		//Point2d pt2d(t2.at<double>(0), t2.at<double>(2));
		//cout << "tafter" << pt2d << endl;
		//cout << "fasit" << now2d - tt2d << endl;
		//double scale2 = norm(now2d - (tt2d + pt2d));

		Point3d pt(t.at<double>(0), t.at<double>(1), t.at<double>(2));

		Point3d tthen = scores[match] + pt;
		char text[100];
		sprintf_s(text, "%.4f,%.4f,%.4f,%d,%d\n", t.at<double>(0), t.at<double>(1), t.at<double>(2), match, i);
		myfile << text;
		//sprintf_s(text, "%.4f,%.4f,%.4f,%d,%d\n", t2.at<double>(0), t2.at<double>(1), t2.at<double>(2), match, i);
		//myfile2 << text;
		//double scale2 = norm(dist2);
		Point3d tnow = scores[now];
		//printf("%d -> %d s: %f, s2:%f\n", now, match, scale, scale2);
		printf("%d : %d\n", i, num_matches);
	/*	circle(traj, Point(xoffset + tnow.x, yoffset - tnow.z) * 2, 0.5, CV_RGB(255, 255, 0), -1);
		circle(traj, Point(xoffset + scores[match].x, yoffset - scores[match].z) * 3, 0.5, CV_RGB(0, 255, 255), -1);
		circle(traj, Point(xoffset + tthen.x, yoffset - tthen.z) * 2, 0.5, CV_RGB(0, 0, 255), -1);
		imshow("Trajectory", traj);*/
		waitKey(0);
	}
	return 1;
}

