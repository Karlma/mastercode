#include <stdio.h>
#include <iostream>
#include <fstream>
#include "opencv2/core.hpp"
#include "opencv2/core/utility.hpp"
#include "opencv2/features2d.hpp"
#include "opencv2/xfeatures2d.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/calib3d.hpp"
#include "opencv2/video/tracking.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include "SeqSLAM.h"
#include "mylib.h"
using namespace cv;
using namespace std;

//TODO convert to SMART, and check sequences not single images.

// resize and patchnormalize
int preprocessImage(Mat img, Mat& out)
{
	Mat dd;
	resize(img, dd, Size(32, 16));
	lib_patch_normalize(dd);
	dd.copyTo(out);
	return 1;
}

// compare two patchnormalized images with SAD.
double compareImages(Mat img1, Mat img2)
{
	Mat diff;
	absdiff(img1, img2, diff);
	double score = sum(diff)[0];
	return score / (img1.rows * img1.cols);
}

// compare two patchnormalized images with SAD.
double seq_comp_smart(Mat img1, Mat img2, int roffset,int coffset, int *bestcoff, int *bestroff)
{
	Mat diff;
	double bestScore = std::numeric_limits<double>::max();
	for (int roff = -roffset; roff <= roffset; roff++)
	{
		for (int coff = -coffset; coff <= coffset; coff++)
		{
			int rmina = -roff >= 0 ? -roff : 0;
			int rmaxa = (img1.rows - 1) - roff < img1.rows ? (img1.rows - 1) - roff : (img1.rows - 1);
			int cmina = -coff >= 0 ? -coff : 0;
			int cmaxa = (img1.cols - 1) - coff < img1.cols ? (img1.cols - 1) - coff : (img1.cols - 1);
			Mat imga = img1.rowRange(rmina, rmaxa).colRange(cmina, cmaxa);
			int rminb = roff >= 0 ? roff : 0;
			int rmaxb = (img1.rows - 1) + roff < img1.rows ? (img1.rows - 1) + roff : (img1.rows - 1);
			int cminb = coff >= 0 ? coff : 0;
			int cmaxb = (img1.cols - 1) + coff < img1.cols ? (img1.cols - 1) + coff : (img1.cols - 1);
			Mat imgb = img2.rowRange(rminb, rmaxb).colRange(cminb, cmaxb);
			Mat diff;
			absdiff(imga, imgb, diff);
			double score = sum(diff)[0];
			score = score / (imga.rows*imga.cols);
			if (score < bestScore) {
				bestScore = score;
				*bestroff = roff;
				*bestcoff = coff;
			}
		}
	}
	return bestScore;
}


// takes patchnormalized image and return comparasion vector.
int compareToOld(Mat img, vector<Mat>& prevImgs, Mat& vect)
{
	//int N = prevImgs.size();
	int N = vect.rows;
	if (N > 1)
	{
		Mat vv = Mat::zeros(N,1, CV_64F);
		for (int i = 0; i < N; i++)
		{
			Mat m = prevImgs[i];
			double score = compareImages(img, m);
			int roff, coff;
			//double score = seq_comp_smart(img, m, 0, 2, &coff, &roff);
			vv.at<double>(i) = score;
		}
		vv.copyTo(vect);
	}
	return 1;
}

// insert comparison vector into system
int compSystem(Mat img, vector<Mat>& prevImgs, Mat& system, int ii)
{
	Mat vect = system.col(ii).rowRange(0, ii);
	return compareToOld(img, prevImgs, vect);
}

// returns match index if match, othervise 0
int getMatch(Mat img, vector<Mat>& prevImgs, Mat& system, int ii , int minlookback)
{
	if (prevImgs.size() <= minlookback+10)
	{
		return -1;
	}
	//int N = prevImgs.size();
	Mat vect = system.col(ii).rowRange(0, prevImgs.size() - minlookback);
	compareToOld(img, prevImgs, vect);
	Mat vect2;
	vect.copyTo(vect2);
	int Rwindows = 10;
	/*for (int i = 0; i < vect.rows - 2; i++) 
	{
		int a = i - Rwindows / 2;
		a = a < 0 ? 0 : a;
		int b = i + Rwindows / 2;
		b = b >= vect.rows ? vect.rows-1 : b;

		Mat win = vect2.rowRange(a, b);
		if (1){
			Mat mea, stdev;
			meanStdDev(win, mea, stdev);
			double m = mea.at<double>(0,0);
			double s = stdev.at<double>(0, 0);
			s = s <= 0 ? 0.01 : s;
			vect.at<double>(i, 0) = (vect2.at<double>(i, 0) - m) / s;
		}
		else
		{
			double min, max;
			minMaxIdx(win, &min, &max);
			double diff = max - min;
			diff = diff <= 0.1 ? 1 : diff;
			double value1 = vect2.at<double>(i, 0);
			double value2 = ((value1 - min) / (diff))*max;
			vect.at<double>(i, 0) = value2;
		}
	}*/
	double min, max;
	Point min_loc, max_loc;
	minMaxLoc(vect, &min, &max, &min_loc, &max_loc);
	
	//if (min < 21500) { // value from matlab testing
	return min_loc.y;
	//}
	//return 0;
}

double seq_get_line_sum(Mat img, int left, int right)
{
	return 0;
}

// verion one, does not know the start and end of a sequence
int seq_search_seq(vector<Mat>& prevImgs, Mat& system, int ii)
{
	int backlook = 40;

	Mat tosearch;
	system.colRange(ii - backlook, ii).rowRange(0, ii - backlook).copyTo(tosearch);

	double min;
	double max;
	minMaxIdx(tosearch, &min, &max);
	tosearch = tosearch - min;

	double vel_dt = 4;
	double vel_min = 4;
	double vel_max = backlook;
	double minsum = 1000000000000000000;
	int starta, startb;
	for (int i = 0; i < ii - backlook*2; i++)
	{
		for (int j = i+vel_min; j <= i+vel_max; j+=vel_dt)
		{
			double dy = (j - i) / (double)backlook;
			double y = i;
			double sum = 0;
			for (int k = 0; k < backlook;k++)
			{
				sum += tosearch.at<double>((int)y, k);
				y += dy;
			}
			if (sum < minsum && sum >= 0){
				minsum = sum;
				starta = i;
				startb = j;
			}
		}
	}

	printf("minsum: %f\n", minsum);
	if (minsum < 2100) {
		double dy = (startb - starta) / (double)backlook;
		double y = starta;
		Mat m;
		tosearch.copyTo(m);
		for (int k = 0; k < backlook; k++)
		{
			m.at<double>((int)y, k) = max;
			y += dy;
		}
		Mat mm;
		m.rowRange(starta, startb).copyTo(mm);
		Mat adjMap;
		convertScaleAbs(mm, adjMap, 255 / max);
		imshow("search", adjMap);
		waitKey(0);
	}
	return 1;
}


static int show_match(Mat currimg, int previdx, Point3f tmatch, Point3f tnow, int xoffset, int yoffset, Mat traj)
{
	imshow("current", currimg);
	char path[100];
	sprintf_s(path, "C:/Matlab/datasets/kitti/00/image_0/%06d.png", previdx);
	Mat matchimg = imread(path, IMREAD_GRAYSCALE);
	threshold(matchimg, matchimg, 253, 255, 4);
	imshow("match", matchimg);
	double dist = norm(tnow - tmatch);
	printf("dist: %.2f\n", dist);
	if (dist < 15)
	{
		//circle(traj, Point(xoffset + tnow.x, yoffset - tnow.z), 1, CV_RGB(0, 255, 0), -1);
	}
	else {
		//circle(traj, Point(xoffset + tnow.x, yoffset - tnow.z), 1, CV_RGB(255, 0, 0), -1);
		//circle(traj, Point(xoffset + tnow.x, yoffset - tnow.z), 1, CV_RGB(255, 0, 0), -1);
		//line(traj, Point(xoffset + tmatch.x, yoffset - tmatch.z), Point(xoffset + tnow.x, yoffset - tnow.z), CV_RGB(255, 0, 0), 1, 8, 0);
	}
	return 1;
}


static struct singleMatch{
	int a;
	int b;
	bool fail;
};

int seq_run_on_sequence(char *imagepatch, int start, int end)
{
	vector<Mat> camMats;
	lib_read_kitti_poses(camMats, "C:/Matlab/datasets/kitti/data_odometry_poses/dataset/poses/00.txt");

	Mat traj = Mat::zeros(800, 800, CV_8UC3);
	//int Nimg = 4540;
	int Nimg = end;
	int frameskip = 1;
	Mat system = Mat::zeros(Nimg / frameskip + 5, Nimg / frameskip + 5, CV_64F);
	vector<Mat> prevImgs;
	vector<int> prevImgsIdx;
	ofstream myfile;
	myfile.open("C:/Matlab/relativeposes_stereo2.csv");
	int xoffset = 280;// 280;
	int yoffset = 580;//580;
	char path[100];
	int const MAX_SKIP = 5;
	int const MIN_SEQ = 15;

	vector<int> bestMatch(Nimg);
	vector<singleMatch> *correctMatches = new vector<singleMatch>;
	vector<singleMatch> *currentSeqMatches = new vector<singleMatch>;

	int cStreak = 0;
	int cFail = 0;
	int seqStarted = 0;
	int prevMatch;
	int ii = 0;
	int jj = 0;
	for (int i = start; i < end; i += frameskip)
	{
		sprintf_s(path, "C:/Matlab/datasets/kitti/00/image_0/%06d.png", i);
		Mat currimg = imread(path, IMREAD_GRAYSCALE);
		threshold(currimg, currimg, 253, 255, 4);

		//Mat hist = lib_get_histogram(currimg);
		//Mat histImg = lib_draw_histogram(hist);

		Mat normalizedimg;
		preprocessImage(currimg, normalizedimg);
		double score = 100000000000;
		if (jj > 0){
			Mat m = prevImgs[jj-1];
			score = compareImages(normalizedimg, m);
		}
		if (score > 20)
		{
			Point3f tnow(camMats[i].at<double>(0, 3),
					 	 camMats[i].at<double>(1, 3),
					 	 camMats[i].at<double>(2, 3));
			circle(traj, Point(xoffset + tnow.x, yoffset - tnow.z), 1, CV_RGB(0, 0, 255), -1);

			int match = getMatch(normalizedimg, prevImgs, system, jj, 300);
			prevImgsIdx.push_back(i);
			if (match >= 0) {
				bestMatch[ii] = prevImgsIdx[match];

				Point3f tmatch(camMats[prevImgsIdx[match]].at<double>(0, 3),
					camMats[prevImgsIdx[match]].at<double>(1, 3),
					camMats[prevImgsIdx[match]].at<double>(2, 3));

				double vall = system.at<double>(match, jj);
				int matchdiff = match - prevMatch;
				printf("%d -> %d,  %.2f\n", match, jj, vall);
				if (vall < 35 || (seqStarted && vall < 41 && matchdiff >= 0 && matchdiff < 4))
				{
					seqStarted = 1;
					singleMatch sm = singleMatch();
					sm.a = ii;
					sm.b = prevImgsIdx[match];
					sm.fail = false;
					currentSeqMatches->push_back(sm);
					cStreak++;
					if (cStreak >= MIN_SEQ) {
						
						show_match(currimg, prevImgsIdx[match], tmatch, tnow, xoffset, yoffset, traj);
						waitKey(0);
					}
					prevMatch = match;
				}
				else 
				{
					if (seqStarted) {
						singleMatch sm = singleMatch();
						sm.a = ii;
						sm.b = prevImgsIdx[match];
						sm.fail = true;
						currentSeqMatches->push_back(sm);
						if (cStreak >= MIN_SEQ) {
							show_match(currimg, prevImgsIdx[match], tmatch, tnow, xoffset, yoffset, traj);
						}
						cFail++;
					}
					if (cFail >= MAX_SKIP || (cFail >= MAX_SKIP/2 && vall > 48)) {
						bool firstreal = false;
						if (cStreak >= MIN_SEQ) {
							for (int jal = currentSeqMatches->size() - 1; jal >= 0; jal--)
							{
								if (firstreal){
									singleMatch smold = (*currentSeqMatches)[jal];
									singleMatch sm = singleMatch();
									sm.a = smold.a;
									sm.b = smold.b;
									sm.fail = smold.fail;
									correctMatches->push_back(sm);
								}
								else if (!(*currentSeqMatches)[jal].fail) {
									firstreal = true;
								}
							}
						}
						seqStarted = 0;
						cStreak = 0;
						cFail = 0;
						currentSeqMatches->clear();
					}
				}
			}

			for (int jal = 0; jal < correctMatches->size(); jal++)
			{
				int oldmatcha = (*correctMatches)[jal].a;
				Point3f pointaa(camMats[oldmatcha].at<double>(0, 3),
					camMats[oldmatcha].at<double>(1, 3),
					camMats[oldmatcha].at<double>(2, 3));
				int oldmatchb = (*correctMatches)[jal].b;
				Point3f pointbb(camMats[oldmatchb].at<double>(0, 3),
					camMats[oldmatchb].at<double>(1, 3),
					camMats[oldmatchb].at<double>(2, 3));
				circle(traj, Point(xoffset + pointaa.x, yoffset - pointaa.z), 1, CV_RGB(0, 255, 255), -1);
				circle(traj, Point(xoffset + pointbb.x, yoffset - pointbb.z), 1, CV_RGB(255, 255, 0), -1);
			}

			prevImgs.push_back(normalizedimg);
			//printf("%d: %.2f\n", jj, score);
			imshow("path", traj);
			//imshow("current", currimg);
			if (jj % 5 == 0 && jj > 200) 
			{
				//seq_search_seq(prevImgs, system, jj-100);
			}
			jj++;
		}
		else
		{
			i = i;
		}
		if (waitKey(1) == 's') {
			double min;
			double max;
			minMaxIdx(system, &min, &max);
			Mat adjMap;
			convertScaleAbs(system, adjMap, 255 / max);
			int subc = ii - 800 < 0 ? 0 : ii - 800;
			int subr = ii - 800 < 0 ? 0 : ii - 800;
			//imshow("current", adjMap.rowRange(subr, ii - 100).colRange(100 + subc, 100 + ii));
			imshow("current", adjMap);
			imwrite("C:/Matlab/system_image_1.png", adjMap);
			waitKey(0);
		}
		ii++;
	}
	imshow("current", system);
	return 1;
}
