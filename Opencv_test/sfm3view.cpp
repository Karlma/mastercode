#include <stdio.h>
#include <iostream>
#include <fstream>
#include <algorithm>
#include "opencv2/core.hpp"
#include "opencv2/core/utility.hpp"
#include "opencv2/features2d.hpp"
#include "opencv2/xfeatures2d.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/calib3d.hpp"
#include "opencv2/video/tracking.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "mylib.h"

using namespace cv;
using namespace std;
using namespace cv::xfeatures2d;

#ifdef OPENCV_2_1
#error Trifocal Tensor doens't work with opencv 2.1 . Use 2.3
#endif

//todo fix to opencv3 ranges
inline Mat sliceTensor(Mat tensor, int i) {
	return Mat(tensor.size.p[1], tensor.size.p[2],CV_32F,(float*)tensor.ptr(i));
}

// struct containing corresponding points in two frames
struct TrackedPoint {
	//w1, w2 should stay to 1
	float x1, y1, w1; //image 1
	float x2, y2, w2; //image 2
	TrackedPoint(float x1 = 0, float y1 = 0, float x2 = 0, float y2 = 0)
		:x1(x1), y1(y1), w1(1.0f), x2(x2), y2(y2), w2(1.0f) {};
	inline float getX(int i) const {//ugly hack
		return *((&x1) + 3 * i);
	};
	inline float getY(int i) const {
		return *((&y1) + 3 * i);
	};
	inline Mat getP1() {
		//ugly hack that could induce segfaults if the object
		// is destroyed before the returned Mat
		return Mat(3, 1, CV_32F, &x1);
	};
	inline Mat getP2() {
		return Mat(3, 1, CV_32F, &x2);
	};
	inline Mat getP(int i) {
		return Mat(3, 1, CV_32F,((&x1) + 3 * i));
	}
	inline const Mat getP(int i) const {
		return Mat(3, 1, CV_32F, const_cast<float*>((&x1) + 3 * i));
	}
};

struct TrackedPoint3 {
	//w1, w2 should stay to 1
	mutable float x1, y1, w1;
	mutable float x2, y2, w2;
	mutable float x3, y3, w3;// this is getting ugly (and wrong, should simply remove the const)
	TrackedPoint3(float x1, float y1, float x2, float y2, float x3, float y3)
		:x1(x1), y1(y1), w1(1.0f), x2(x2), y2(y2), w2(1.0f), x3(x3), y3(y3), w3(1.0f) {};
	inline float getX(int i) const {//ugly hack
		return *((&x1) + 3 * i);
	};
	inline float getY(int i) const {
		return *((&y1) + 3 * i);
	};
	inline Mat getP1() {
		//ugly hack that could induce segfaults if the object
		// is destroyed before the returned Mat
		return Mat(3, 1, CV_32F, &x1);
	};
	inline Mat getP2() {
		return Mat(3, 1, CV_32F, &x2);
	};
	inline Mat getP3() {
		return Mat(3, 1, CV_32F, &x3);
	};
	inline const Mat getP1() const {
		return Mat(3, 1, CV_32F, &x1);
	};
	inline const Mat getP2() const {
		return Mat(3, 1, CV_32F, &x2);
	};
	inline const Mat getP3() const {
		return Mat(3, 1, CV_32F, &x3);
	};
	inline Mat getP1NH() {
		return Mat(2, 1, CV_32F, &x1);
	};
	inline Mat getP2NH() {
		return Mat(2, 1, CV_32F, &x2);
	};
	inline Mat getP3NH() {
		return Mat(2, 1, CV_32F, &x3);
	};
	inline const Mat getP1NH() const {
		return Mat(2, 1, CV_32F, &x1);
	};
	inline const Mat getP2NH() const {
		return Mat(2, 1, CV_32F, &x2);
	};
	inline const Mat getP3NH() const {
		return Mat(2, 1, CV_32F, &x3);
	};

	inline Mat getP(int i) {
		return Mat(3, 1, CV_32F, (&x1) + 3 * i);
	}
	inline TrackedPoint getTP1() const {
		return TrackedPoint(x1, y1, x2, y2);
	};
	inline TrackedPoint getTP2() const {
		return TrackedPoint(x2, y2, x3, y3);
	};
	inline TrackedPoint getTP13() const {
		return TrackedPoint(x1, y1, x3, y3);
	};
};

/*
void displayTrackedPoints3(const Mat& im, const vector<TrackedPoint3> & trackedPoints) {
	Mat im_cpy;
	im.copyTo(im_cpy);
	for (vector<TrackedPoint3>::const_iterator it = trackedPoints.begin();
		it != trackedPoints.end(); ++it) {
		line(im_cpy, Point(it->x1, it->y1), Point(it->x2, it->y2), Scalar(255, 0, 0), 3);
		line(im_cpy, Point(it->x2, it->y2), Point(it->x3, it->y3), Scalar(0, 0, 255), 3);
	}
	display(im_cpy);
} */

void GetTrackedPoints3(const Mat& im1, const Mat& im2, const Mat& im3,
	vector<TrackedPoint3> & points_out) {
	// goodFeaturesToTrack parameters
	const int maxCorners = 3000;
	const float qualityLevel = 0.01f;
	const float minDistance = 10.0f;
	const int blockSize = 10;
	const int useHarrisDetector = 0;
	const float k = 0.04f;
	// calcOpticalFlowPyrLK parameters
	const Size winSize(10, 10);
	const int maxLevel = 5;
	const TermCriteria criteria = TermCriteria(TermCriteria::COUNT + TermCriteria::EPS, 20, 0.3);
	const double derivLambda = 0;
	const int flags = 0;
	assert((im1.size() == im2.size()) && (im2.size() == im3.size()));
	Mat corners1, corners2;
	vector<uchar> status;
	Mat err;
	Mat im1gray;
	cvtColor(im1, im1gray, CV_BGR2GRAY);
	goodFeaturesToTrack(im1gray, corners1, maxCorners, qualityLevel, minDistance, noArray(), blockSize, useHarrisDetector, k);
	calcOpticalFlowPyrLK(im1, im2, corners1, corners2, status, err, winSize, maxLevel, criteria, derivLambda, flags);

	int nTrackedPoints12 = 0;
	for (int i = 0; i < corners1.size().height; ++i)
	{
		if (status[i])
		{
			++nTrackedPoints12;
		}
	}

	Mat corners1good(nTrackedPoints12, 2,CV_32F);
	Mat corners2good(nTrackedPoints12, 2, CV_32F);
	Mat corners3;
	for (int i = 0, k = 0; i < corners1.size().height; ++i)
		if (status[i]) {
			corners1good.at<float>(k, 0) = corners1.at<float>(i, 0);
			corners1good.at<float>(k, 1) = corners1.at<float>(i, 1);
			corners2good.at<float>(k, 0) = corners2.at<float>(i, 0);
			corners2good.at<float>(k, 1) = corners2.at<float>(i, 1);
			++k;
		}
	calcOpticalFlowPyrLK(im2, im3, corners2good, corners3, status, err, winSize, maxLevel,
		criteria, derivLambda, flags);
	for (int i = 0; i < corners2good.size().height; ++i)
		if (status[i])
		{
			points_out.push_back(TrackedPoint3(corners1good.at<float>(i, 0), corners1good.at<float>(i, 1),
											   corners2good.at<float>(i, 0), corners2good.at<float>(i, 1),
											   corners3.at<float>(i, 0), corners3.at<float>(i, 1)));
		}
}

void GetFundamentalMatsFromTP3(const vector<TrackedPoint3> & trackedPoints,
	Mat& F1, Mat& F2, vector<TrackedPoint3> & inliers) {
	//TODO: some code here is redundant with GetTrackedPoints3 and should be merged
	const int method = FM_RANSAC;
	const double param1 = 3;
	const double param2 = 0.99;

	Mat pts1(trackedPoints.size(), 2, CV_32F);
	Mat pts2(trackedPoints.size(), 2, CV_32F);

	for (size_t i = 0; i < trackedPoints.size(); ++i) {
		pts1.at<float>(i, 0) = trackedPoints[i].x1;
		pts1.at<float>(i, 1) = trackedPoints[i].y1;
		pts2.at<float>(i, 0) = trackedPoints[i].x2;
		pts2.at<float>(i, 1) = trackedPoints[i].y2;
	}
	vector<uchar> status;
	F1 = findFundamentalMat(pts1, pts2, method, param1, param2, status);

	int nInliers12 = 0;
	for (int i = 0; i < status.size(); ++i)
		if (status[i])
		{
			++nInliers12;
		}
	Mat pts1good(nInliers12, 2, CV_32F);
	Mat pts2good(nInliers12, 2, CV_32F);
	Mat pts3(nInliers12, 2, CV_32F);
	for (int i = 0, k = 0; i < status.size(); ++i){
		if (status[i]) {
			pts1good.at<float>(k, 0) = pts1.at<float>(i, 0);
			pts1good.at<float>(k, 1) = pts1.at<float>(i, 1);
			pts2good.at<float>(k, 0) = pts2.at<float>(i, 0);
			pts2good.at<float>(k, 1) = pts2.at<float>(i, 1);
			pts3.at<float>(k, 0) = trackedPoints[i].x3;
			pts3.at<float>(k, 1) = trackedPoints[i].y3;
			++k;
		}
	}
	//TODO maybe the parameters shouldn't be the same since there is less points
	F2 = findFundamentalMat(pts2good, pts3, method, param1, param2, status);
	inliers.clear();
	for (int i = 0; i < status.size(); ++i) {
		if (status[i]) {
			inliers.push_back(TrackedPoint3(pts1good.at<float>(i, 0), pts1good.at<float>(i, 1),
				pts2good.at<float>(i, 0), pts2good.at<float>(i, 1),
				pts3.at<float>(i, 0), pts3.at<float>(i, 1)));
		}
	}
}

//HZ p312, Section 12.2
Mat Triangulate3(const Mat& P1, const Mat& P2, const Mat& P3, const TrackedPoint3 & p) {
	Mat A(9, 4, CV_32F);
	for (int i = 0; i < 4; ++i) {
		A.at<float>(0, i) = p.x1 * P1.at<float>(2, i) - P1.at<float>(0, i);
		A.at<float>(1, i) = p.y1 * P1.at<float>(2, i) - P1.at<float>(1, i);
		A.at<float>(2, i) = p.x2 * P2.at<float>(2, i) - P2.at<float>(0, i);
		A.at<float>(3, i) = p.y2 * P2.at<float>(2, i) - P2.at<float>(1, i);
		A.at<float>(4, i) = p.x3 * P3.at<float>(2, i) - P3.at<float>(0, i);
		A.at<float>(5, i) = p.y3 * P3.at<float>(2, i) - P3.at<float>(1, i);
		// the 3 following equations could be removed in most of the cases.
		// however, sometimes it induces errors if some (which ones) coefficients are
		// too close to 0
		A.at<float>(6, i) = p.x1 * P1.at<float>(1, i) - p.y1 * P1.at<float>(0, i);
		A.at<float>(7, i) = p.x2 * P2.at<float>(1, i) - p.y2 * P2.at<float>(0, i);
		A.at<float>(8, i) = p.x3 * P3.at<float>(1, i) - p.y3 * P3.at<float>(0, i);
	}
	SVD svd(A, SVD::MODIFY_A);
	Mat p3d(3, 1,CV_32F);
	for (int i = 0; i < 3; ++i){
		p3d.at<float>(i, 0) = svd.vt.at<float>(3, i) / svd.vt.at<float>(3, 3);
	}
	return p3d;
}

//auxiliary function for GetTrifocalTensorFromCameraMatrices. (some variation of epscov)
inline Mat epsc(const Mat& A) {
	Mat B(4, 4,CV_32F);
	B.at<float>(0, 0) = 0.0f; B.at<float>(0, 1) = A.at<float>(2, 3); B.at<float>(0, 2) = A.at<float>(3, 1); B.at<float>(0, 3) = A.at<float>(1, 2);
	B.at<float>(1, 0) = A.at<float>(3, 2); B.at<float>(1, 1) = 0.0f; B.at<float>(1, 2) = A.at<float>(0, 3); B.at<float>(1, 3) = A.at<float>(2, 0);
	B.at<float>(2, 0) = A.at<float>(1, 3); B.at<float>(2, 1) = A.at<float>(3, 0); B.at<float>(2, 2) = 0.0f; B.at<float>(2, 3) = A.at<float>(0, 1);
	B.at<float>(3, 0) = A.at<float>(2, 1); B.at<float>(3, 1) = A.at<float>(0, 2); B.at<float>(3, 2) = A.at<float>(1, 0); B.at<float>(3, 3) = 0.0f;
	return B;
}

void GetTrifocalTensorFromCameraMatrices(const Mat& A, const Mat& B, const Mat& C, Mat& trifocal_out) {
	int dims[3] = { 3, 3, 3 };
	trifocal_out = cv::Mat(3, dims, CV_32SC1);
	sliceTensor(trifocal_out, 0) = B * epsc(A.row(1).t()*A.row(2) - A.row(2).t()*A.row(1)) * C.t();
	sliceTensor(trifocal_out, 1) = B * epsc(A.row(2).t()*A.row(0) - A.row(0).t()*A.row(2)) * C.t();
	sliceTensor(trifocal_out, 2) = B * epsc(A.row(0).t()*A.row(1) - A.row(1).t()*A.row(0)) * C.t();
}

// almost equal
template<typename T, typename T2> inline bool epsEqual(T a, T2 b, double eps = 0.01) {
	return (a - eps < b) && (b < a + eps);
}
//returns true if the 3 points are collinear in AT LEAST one view
bool Collinear(const Mat& p1, const Mat& p2, const Mat& p3) {
	float epsilon = 0.25f;
	for (int i = 0; i < 3; ++i) {
		const float x1 = p2.at<float>(i * 2, 0) - p1.at<float>(i * 2, 0);
		const float y1 = p2.at<float>(i * 2 + 1, 0) - p1.at<float>(i * 2 + 1, 0);
		const float x2 = p3.at<float>(i * 2, 0) - p1.at<float>(i * 2, 0);
		const float y2 = p3.at<float>(i * 2 + 1, 0) - p1.at<float>(i * 2 + 1, 0);
		if (epsEqual(x1*y2 - y1*x2, epsilon))
			return true;
	}
	return false;
}

inline void copyRow(const Mat& src, Mat& dst, int row_src, int row_dst) 
{
	src.row(row_src).copyTo(dst.row(row_dst));
}

//finds 4 points in which each 3 points are not collinear in any view,
// or returns false if this is impossible
bool FindNonCollinearPoints(const Mat& points, Mat& points_out) {
	const size_t n = points.size().height;
	assert(n == 6);
	points_out = Mat(6, 6, CV_32F);
	size_t i1, i2, i3, i4, k = 0;
	for (i1 = 0; i1 < n; ++i1)
		for (i2 = i1 + 1; i2 < n; ++i2)
			for (i3 = i2 + 1; i3 < n; ++i3)
				for (i4 = i3 + 1; i4 < n; ++i4)
					if ((!Collinear(points.row(i1), points.row(i2), points.row(i3))) &&
						(!Collinear(points.row(i1), points.row(i2), points.row(i4))) &&
						(!Collinear(points.row(i1), points.row(i3), points.row(i4))) &&
						(!Collinear(points.row(i2), points.row(i3), points.row(i4)))) {
						for (size_t i = 0; i < n; ++i)
							if ((i != i1) && (i != i2) && (i != i3) && (i != i4))
								copyRow(points, points_out, i, k++);
						copyRow(points, points_out, i1, k++);
						copyRow(points, points_out, i2, k++);
						copyRow(points, points_out, i3, k++);
						copyRow(points, points_out, i4, k++);
						return true;
					}
	return false;
}

bool FindNonCollinearPoints(const Mat& points, const vector<int> & pointsToUse, Mat& points_out) {
	const size_t n = 6;
	points_out = Mat(6, 6,CV_32F);
	size_t i1_, i2_, i3_, i4_, i1, i2, i3, i4, k = 0;
	for (i1_ = 0; i1_ < n; ++i1_) {
		i1 = pointsToUse[i1_];
		for (i2_ = i1_ + 1; i2_ < n; ++i2_) {
			i2 = pointsToUse[i2_];
			for (i3_ = i2_ + 1; i3_ < n; ++i3_) {
				i3 = pointsToUse[i3_];
				for (i4_ = i3_ + 1; i4_ < n; ++i4_) {
					i4 = pointsToUse[i4_];
					if ((!Collinear(points.row(i1), points.row(i2), points.row(i3))) &&
						(!Collinear(points.row(i1), points.row(i2), points.row(i4))) &&
						(!Collinear(points.row(i1), points.row(i3), points.row(i4))) &&
						(!Collinear(points.row(i2), points.row(i3), points.row(i4)))) {
						for (size_t i = 0; i < n; ++i)
							if ((i != i1_) && (i != i2_) && (i != i3_) && (i != i4_))
								copyRow(points, points_out, pointsToUse[i], k++);
						copyRow(points, points_out, i1, k++);
						copyRow(points, points_out, i2, k++);
						copyRow(points, points_out, i3, k++);
						copyRow(points, points_out, i4, k++);
						return true;
					}
				}
			}
		}
	}
	return false;
}

//HZ p91, Algo 4.1, modified to work with null last homogeneous coordinate
Mat HomographyFromNPoints(const Mat& pts1, const Mat& pts2) {
	// opencv seems not to be able to compute homography fro homogeneous points since 2.1 .
	// Unfortunately, this is required here. I stick to the C++ opencv function prototype
	assert(pts1.size() == pts2.size());
	assert(pts1.size().height >= 4);
	assert(pts1.size().width == 3);
	const int n = pts1.size().height;
	Mat A(3 * n, 9,CV_64F);
	for (int i = 0; i < n; ++i)
		for (int j = 0; j < 3; ++j) {
			A.at<double>(3 * i, j) = 0.0f;
			A.at<double>(3 * i, j + 3) = -pts2.at<double>(i, 2) * pts1.at<double>(i, j);
			A.at<double>(3 * i, j + 6) = pts2.at<double>(i, 1) * pts1.at<double>(i, j);
			A.at<double>(3 * i + 1, j) = pts2.at<double>(i, 2) * pts1.at<double>(i, j);
			A.at<double>(3 * i + 1, j + 3) = 0.0f;
			A.at<double>(3 * i + 1, j + 6) = -pts2.at<double>(i, 0) * pts1.at<double>(i, j);
			A.at<double>(3 * i + 2, j) = -pts2.at<double>(i, 1) * pts1.at<double>(i, j);
			A.at<double>(3 * i + 2, j + 3) = pts2.at<double>(i, 0) * pts1.at<double>(i, j);
			A.at<double>(3 * i + 2, j + 6) = 0.0f;
		}

	SVD svd(A, SVD::MODIFY_A);
	//cout << "HOM: " << svd.w << endl;
	Mat homography(3, 3,CV_32F);
	for (int i = 0; i < 3; ++i)
		for (int j = 0; j < 3; ++j)
			homography.at<float>(i, j) = svd.vt.at<double>(8, i * 3 + j);
	return homography;
}

//Numerical Recipes 2nd edition, p184-185
// returns the number of solutions
int SolveCubicEquation(float a, float b, float c, float* x1, float* x2, float* x3) {
	const float Q = (a*a - 3.0f*b) / 9.0f;
	const float R = (a*(2.0f*a*a - 9.0f*b) + 27.0f*c) / 54.0f;
	const float R2 = R*R;
	const float Q3 = Q*Q*Q;
	if (R2 < Q3) {
		const float m2rtQ = -2.0f * sqrt(Q);
		const float theta = acos(R / sqrt(Q3));
		*x1 = m2rtQ * cos(theta / 3.0f) - a / 3.0f;
		*x2 = m2rtQ * cos((theta + 2.0f*CV_PI) / 3.0f) - a / 3.0f;
		*x3 = m2rtQ * cos((theta - 2.0f*CV_PI) / 3.0f) - a / 3.0f;
		return 3;
	}
	else {
		const float A = -((R<0) ? -1.0f : 1.0f)*pow(abs(R) + sqrt(R2 - Q3), 1.0f / 3.0f);
		const float B = (abs(A) < 1e-15) ? 0.0f : Q / A;
		*x1 = A + B - a / 3.0f;
		return 1;
	}
}

//HZ p178, Section 7.1, modified to work with null last homogeneous coordinate
// Returns the camera matrix P from 3D-2D points correspondences
// AND it can work with null last homogeneous coordinate
// (unlike opencv calibrateCamera)
Mat DLT(const Mat& pts3d, const Mat& pts2d) {
	// this could be unified with HomographyFromNPoints
	assert(pts3d.size().height == pts2d.size().height);
	assert(pts3d.size().height >= 6);
	assert(pts3d.size().width == 4);
	assert(pts2d.size().width == 3);
	const int n = pts3d.size().height;
	Mat A(3 * n, 12, CV_64F);
	for (int i = 0; i < n; ++i)
		for (int j = 0; j < 4; ++j) {
			A.at<double>(3 * i, j) = 0.0f;
			A.at<double>(3 * i, j + 4) = -pts2d.at<double>(i, 2) * pts3d.at<double>(i, j);
			A.at<double>(3 * i, j + 8) = pts2d.at<double>(i, 1) * pts3d.at<double>(i, j);
			A.at<double>(3 * i + 1, j) = pts2d.at<double>(i, 2) * pts3d.at<double>(i, j);
			A.at<double>(3 * i + 1, j + 4) = 0.0f;
			A.at<double>(3 * i + 1, j + 8) = -pts2d.at<double>(i, 0) * pts3d.at<double>(i, j);
			A.at<double>(3 * i + 2, j) = -pts2d.at<double>(i, 1) * pts3d.at<double>(i, j);
			A.at<double>(3 * i + 2, j + 4) = pts2d.at<double>(i, 0) * pts3d.at<double>(i, j);
			A.at<double>(3 * i + 2, j + 8) = 0.0f;
		}

	SVD svd(A, SVD::MODIFY_A);
	//cout << "DLT: " << svd.w << endl;
	Mat P(3, 4,CV_32F);
	for (int i = 0; i < 3; ++i)
		for (int j = 0; j < 4; ++j)
			P.at<float>(i, j) = svd.vt.at<double>(11, i * 4 + j);
	return P;
}

//HZ p511, Algo 20.1
bool SixPointsAlgorithm(const Mat& points2d, vector<vector<Mat> > & cameras_out, vector<Mat> & points3d_out, const vector<int>* pointsToUse) {
	if (!pointsToUse)
		assert(points2d.size().height == 6);
	cameras_out.clear();
	points3d_out.clear();

	// step (i)
	Mat reorderedPoints2d;
	if (pointsToUse) {
		if (!FindNonCollinearPoints(points2d, *pointsToUse, reorderedPoints2d))
			return false;
	}
	else {
		if (!FindNonCollinearPoints(points2d, reorderedPoints2d))
			return false;
	}

	Mat A = Mat::zeros(5, 5,CV_32F);
	vector<Mat> Ts;
	for (int iView = 0; iView < 3; ++iView) {

		// step (ii)
		Mat pts1(4, 3, CV_32F);
		Mat pts2(4, 3, 0.0f, CV_32F);
		for (int k = 0; k < 4; ++k) {
			pts1.at<float>(k, 0) = reorderedPoints2d.at<float>(2 + k, iView * 2);
			pts1.at<float>(k, 1) = reorderedPoints2d.at<float>(2 + k, iView * 2 + 1);
			pts1.at<float>(k, 2) = 1.0f;
		}
		//cout << pts1 << endl;
		pts2.at<float>(0, 0) = pts2.at<float>(1, 1) = pts2.at<float>(2, 2) = 1.0f;
		pts2.at<float>(3, 0) = pts2.at<float>(3, 1) = pts2.at<float>(3, 2) = 1.0f;
		Mat T = HomographyFromNPoints(pts1, pts2);
		double Tmax;
		minMaxLoc(T, NULL, &Tmax);
		T = T / Tmax;
		Mat x1(3, 1, CV_32F);
		Mat x2(3, 1, CV_32F); //HZ's \hat{x_1} -> x1,  \hat{x_2} -> x2
		x1.at<float>(0, 0) = reorderedPoints2d.at<float>(0, iView * 2);
		x1.at<float>(1, 0) = reorderedPoints2d.at<float>(0, iView * 2 + 1);
		x1.at<float>(2, 0) = 1.0f;
		x1 = T * x1;
		x2.at<float>(0, 0) = reorderedPoints2d.at<float>(1, iView * 2);
		x2.at<float>(1, 0) = reorderedPoints2d.at<float>(1, iView * 2 + 1);
		x2.at<float>(2, 0) = 1.0f;
		x2 = T * x2;
		Ts.push_back(T);

		// step (iii)
		A.at<float>(iView, 0) = x1.at<float>(1, 0) * x2.at<float>(0, 0) - x1.at<float>(1, 0) * x2.at<float>(2, 0);
		A.at<float>(iView, 1) = x1.at<float>(2, 0) * x2.at<float>(0, 0) - x1.at<float>(1, 0) * x2.at<float>(2, 0);
		A.at<float>(iView, 2) = x1.at<float>(0, 0) * x2.at<float>(1, 0) - x1.at<float>(1, 0) * x2.at<float>(2, 0);
		A.at<float>(iView, 3) = x1.at<float>(2, 0) * x2.at<float>(1, 0) - x1.at<float>(1, 0) * x2.at<float>(2, 0);
		A.at<float>(iView, 4) = x1.at<float>(0, 0) * x2.at<float>(2, 0) - x1.at<float>(1, 0) * x2.at<float>(2, 0);
	}

	vector<Mat> possibleF;

	{
		// step (iv)
		SVD svd(A, SVD::MODIFY_A);
		const float p1 = svd.vt.at<float>(3, 0);
		const float q1 = svd.vt.at<float>(3, 1);
		const float r1 = svd.vt.at<float>(3, 2);
		const float s1 = svd.vt.at<float>(3, 3);
		const float t1 = svd.vt.at<float>(3, 4);
		const float a1 = p1 + q1 + r1 + s1 + t1;
		const float p2 = svd.vt.at<float>(4, 0);
		const float q2 = svd.vt.at<float>(4, 1);
		const float r2 = svd.vt.at<float>(4, 2);
		const float s2 = svd.vt.at<float>(4, 3);
		const float t2 = svd.vt.at<float>(4, 4);
		const float a2 = p2 + q2 + r2 + s2 + t2;
		Mat F1 = Mat::zeros(3, 3, CV_32F);
		Mat F2 = Mat::zeros(3, 3, CV_32F);
		F1.at<float>(0, 1) = p1;
		F1.at<float>(0, 2) = q1;
		F1.at<float>(1, 0) = r1;
		F1.at<float>(1, 2) = s1;
		F1.at<float>(2, 0) = t1;
		F1.at<float>(2, 1) = -a1;
		F2.at<float>(0, 1) = p2;
		F2.at<float>(0, 2) = q2;
		F2.at<float>(1, 0) = r2;
		F2.at<float>(1, 2) = s2;
		F2.at<float>(2, 0) = t2;
		F2.at<float>(2, 1) = -a2;

		// step (v)
		const float a = p1*t1*s1 - a1*q1*r1;
		const float b = p2*t1*s1 + p1*t2*s1 + p1*t1*s2 - a2*q1*r1 - a1*q2*r1 - a1*q1*r2;
		const float c = p1*t2*s2 + p2*t1*s2 + p2*t2*s1 - a1*q2*r2 - a2*q1*r2 - a2*q2*r1;
		const float d = p2*t2*s2 - a2*q2*r2;
		float x[3];
		if ((-1e-10 < a) && (a < 1e-10)) {
			cout << "a is small" << endl;
			if ((d < -1e-10) || (1e-10 < d)) {
				//TODO: in this case, this is no cubic equation.
				// I don't know if this can actually happen.
				cerr << "not a cubic equation (TODO: implement it)" << endl;
				return false;
			}
			int nSolutions = SolveCubicEquation(c / d, b / d, a / d, x, x + 1, x + 2);
			for (int i = 0; i < nSolutions; ++i)
				possibleF.push_back(F1 + x[i] * F2);
		}
		else {
			int nSolutions = SolveCubicEquation(b / a, c / a, d / a, x, x + 1, x + 2);
			for (int i = 0; i < nSolutions; ++i)
				possibleF.push_back(x[i] * F1 + F2);
		}
	}

	for (size_t iF = 0; iF < possibleF.size(); ++iF) {
		const Mat& F = possibleF[iF];

		// step (vi)
		SVD svd;
		Mat M(3, 3,CV_32F);
		M.at<float>(0, 0) = F.at<float>(0, 2); M.at<float>(0, 1) = F.at<float>(1, 2); M.at<float>(0, 2) = 0.0f;
		M.at<float>(1, 0) = F.at<float>(0, 1); M.at<float>(1, 1) = 0.0f; M.at<float>(1, 2) = F.at<float>(2, 1);
		M.at<float>(2, 0) = 0.0f; M.at<float>(2, 1) = F.at<float>(1, 0); M.at<float>(2, 2) = F.at<float>(2, 0);
		svd(M, SVD::MODIFY_A);
		const float a1 = svd.vt.at<float>(2, 0);
		const float a2 = svd.vt.at<float>(2, 1);
		const float a3 = svd.vt.at<float>(2, 2);
		M.at<float>(0, 0) = F.at<float>(0, 1); M.at<float>(0, 1) = F.at<float>(1, 0); M.at<float>(0, 2) = 0.0f;
		M.at<float>(1, 0) = F.at<float>(0, 2); M.at<float>(1, 1) = 0.0f; M.at<float>(1, 2) = F.at<float>(2, 0);
		M.at<float>(2, 0) = 0.0f; M.at<float>(2, 1) = F.at<float>(1, 2); M.at<float>(2, 2) = F.at<float>(2, 1);
		svd(M, SVD::MODIFY_A);
		const float b1 = svd.vt.at<float>(2, 0);
		const float b2 = svd.vt.at<float>(2, 1);
		const float b3 = svd.vt.at<float>(2, 2);
		Mat A = Mat::zeros(6, 4,CV_32F);
		A.at<float>(0, 0) = b2; A.at<float>(0, 1) = -b1;
		A.at<float>(1, 1) = b3; A.at<float>(1, 2) = -b2;
		A.at<float>(2, 0) = -b3; A.at<float>(2, 2) = b1;
		A.at<float>(3, 0) = -a2; A.at<float>(3, 1) = a1; A.at<float>(3, 3) = a2 - a1;
		A.at<float>(4, 1) = -a3; A.at<float>(4, 2) = a2; A.at<float>(4, 3) = a3 - a2;
		A.at<float>(5, 0) = a3; A.at<float>(5, 2) = -a1; A.at<float>(5, 3) = a1 - a3;
		svd(A, SVD::MODIFY_A);
		const float a = svd.vt.at<float>(3, 0);
		const float b = svd.vt.at<float>(3, 1);
		const float c = svd.vt.at<float>(3, 2);
		const float d = svd.vt.at<float>(3, 3);

		// step (vii)
		Mat pts3d = Mat::zeros(6, 4,CV_32F);
		pts3d.at<float>(0, 0) = 1.0f; pts3d.at<float>(0, 1) = 1.0f; pts3d.at<float>(0, 2) = 1.0f; pts3d.at<float>(0, 3) = 1.0f;
		pts3d.at<float>(1, 0) = a; pts3d.at<float>(1, 1) = b; pts3d.at<float>(1, 2) = c; pts3d.at<float>(1, 3) = d;
		pts3d.at<float>(2, 0) = 1.0f;
		pts3d.at<float>(3, 1) = 1.0f;
		pts3d.at<float>(4, 2) = 1.0f;
		pts3d.at<float>(5, 3) = 1.0f;
		points3d_out.push_back(pts3d);
		cameras_out.push_back(vector<Mat>());
		Mat pts2d(6, 3,CV_32F);
		for (int iCam = 0; iCam < 3; ++iCam) {
			for (int i = 0; i < 6; ++i) {
				pts2d.at<float>(i, 0) = reorderedPoints2d.at<float>(i, iCam * 2);
				pts2d.at<float>(i, 1) = reorderedPoints2d.at<float>(i, iCam * 2 + 1);
				pts2d.at<float>(i, 2) = 1.0f;
			}
			Mat P = DLT(pts3d, pts2d);
			cameras_out.back().push_back(P);
		}
	}
	return true;
}

//HZ p394 Algo 16.1
//the H's are homographies to "denormalize" (unlike HZ)
void NormalizePoints(vector<TrackedPoint3> & points2d, vector<TrackedPoint3> & points2d_out, vector<Mat> & H_out) {
	int n = points2d.size();
	Mat means[3];
	means[0] = Mat::zeros(3, 1, CV_32F);
	means[1] = Mat::zeros(3, 1, CV_32F);
	means[2] = Mat::zeros(3, 1, CV_32F);
	for (int iView = 0; iView < 3; ++iView) {
		for (int i = 0; i < n; ++i)
			means[iView] += points2d[i].getP(iView);
		means[iView] = means[iView] / (float)n;
	}
	for (int i = 0; i < n; ++i)
		points2d_out.push_back(TrackedPoint3(points2d[i].x1 - means[0].at<float>(0, 0),
		points2d[i].y1 - means[0].at<float>(1, 0),
		points2d[i].x2 - means[1].at<float>(0, 0),
		points2d[i].y2 - means[1].at<float>(1, 0),
		points2d[i].x3 - means[2].at<float>(0, 0),
		points2d[i].y3 - means[2].at<float>(1, 0)));
	float meandist[3];
	meandist[0] = meandist[1] = meandist[2] = 0.0f;
	float x, y;
	for (int iView = 0; iView < 3; ++iView) {
		for (int i = 0; i < n; ++i) {
			x = points2d_out[i].getX(iView);
			y = points2d_out[i].getY(iView);
			meandist[iView] += sqrt(x*x + y*y);
		}
		meandist[iView] = meandist[iView] / ((float)n * sqrt(2));
	}
	for (int i = 0; i < n; ++i) {
		points2d_out[i] = TrackedPoint3(points2d_out[i].x1 / meandist[0],
			points2d_out[i].y1 / meandist[0],
			points2d_out[i].x2 / meandist[1],
			points2d_out[i].y2 / meandist[1],
			points2d_out[i].x3 / meandist[2],
			points2d_out[i].y3 / meandist[2]);
	}
	H_out.clear();
	for (int i = 0; i < 3; ++i) {
		H_out.push_back(Mat::zeros(3, 3, CV_32F));
		H_out.back().at<float>(0, 0) = H_out.back().at<float>(1, 1) = meandist[i];
		H_out.back().at<float>(0, 2) = means[i].at<float>(0, 0);
		H_out.back().at<float>(1, 2) = means[i].at<float>(1, 0);
		H_out.back().at<float>(2, 2) = 1.0f;
	}
}

//HZ p595, Algo 5.6
// Finds x that minimizes ||Ax|| subject to ||x|| = 1 and x = ||G\hat{x}||
// G has rank r
Mat CondLeastSquares(const Mat & A, const Mat & G, int r) {
	//if we wanted to be optimized, these buffers should actually be globals
	SVD svd(G);
	if (r == svd.u.size().width) {
		SVD svd2(A*svd.u, SVD::MODIFY_A);
		return svd.u * svd2.vt.row(svd2.vt.size().height - 1).t();
	}
	else {
		// use ROI or range to implement subMat
		Mat Uprime = subMat(svd.u, 0, svd.u.size().height, 0, r);
		SVD svd2(A*Uprime, SVD::MODIFY_A);
		return Uprime * svd2.vt.row(svd2.vt.size().height - 1).t();
	}
}

//HZ p395, Section 16.3, "Retrieving the epipoles"
// Finds the epipoles in the second and third images for the first
void GetEpipolesFromTrifocalTensor(const Mat& trifocal, Mat& e2, Mat& e3) {
	int i;
	SVD svd;
	Mat V(3, 3,CV_32F);
	for (i = 0; i < 3; ++i) {
		svd(sliceTensor(trifocal, i).t());
		copyRow(svd.vt, V, 2, i);
	}
	svd(V, SVD::MODIFY_A);
	e2 = svd.vt.row(2).t();
	e2 = e2 / norm(e2);
	for (i = 0; i < 3; ++i) {
		svd(sliceTensor(trifocal, i));
		copyRow(svd.vt, V, 2, i);
	}
	svd(V, SVD::MODIFY_A);
	e3 = svd.vt.row(2).t();
	e3 = e3 / norm(e3);
}

Mat epscov(const Mat & x) {
	Mat A(3, 3,CV_32F);
	A.at<float>(0, 0) = 0.0f; A.at<float>(0, 1) = x.at<float>(2, 0); A.at<float>(0, 2) = -x.at<float>(1, 0);
	A.at<float>(1, 0) = -x.at<float>(2, 0); A.at<float>(1, 1) = 0.0f; A.at<float>(1, 2) = x.at<float>(0, 0);
	A.at<float>(2, 0) = x.at<float>(1, 0); A.at<float>(2, 1) = -x.at<float>(0, 0); A.at<float>(2, 2) = 0.0f;
	return A;
}

Mat createTensor(int a, int b, int c){
	int dims[3] = { a,b,c };
	return Mat(3, dims, CV_32SC1);
}
//HZ p396, Algo 16.2
// Finds the trifocal tensor minimizing algebraic error
// not sure this isn't buggy
void EstimateTrifocalTensor(vector<TrackedPoint3> & points2d_in, Mat& trifocal_out, bool linear) 
{
	int n = points2d_in.size();
	assert(n >= 7);

	vector<TrackedPoint3> points2d;
	vector<Mat> normalizations;
	NormalizePoints(points2d_in, points2d, normalizations);

	// finding initial guess
	Mat A(9 * n, 27,CV_32F); // change to double?
	Mat x1(3, 1, CV_32F);
	Mat x2eps(3, 3, CV_32F);
	Mat x3eps(3, 3, CV_32F);
	int iPoint, i, j, k, s, t;
	for (iPoint = 0; iPoint < n; ++iPoint) {
		//TODO: ok, this is more than necessary, but otherwise the formula get really ugly
		x1 = points2d[iPoint].getP1();
		x2eps = epscov(points2d[iPoint].getP2());
		x3eps = epscov(points2d[iPoint].getP3());
		for (i = 0; i < 3; ++i)
			for (j = 0; j < 3; ++j)
				for (k = 0; k < 3; ++k)
					for (s = 0; s < 3; ++s)
						for (t = 0; t < 3; ++t)
							A.at<float>(9 * iPoint + s * 3 + t, 9 * i + 3 * j + k) = x1.at<float>(i, 0) * x2eps.at<float>(j, s) * x3eps.at<float>(k, t);
	}
	//cout << A << endl;
	SVD svd(A);
	Mat T = createTensor(3, 3, 3);
	//cout << svd.w << endl;
	for (i = 0; i < 3; ++i)
		for (j = 0; j < 3; ++j)
			for (k = 0; k < 3; ++k)
				T.at<float>(i, j, k) = svd.vt.at<double>(26, i * 9 + j * 3 + k);

	if (!linear) {
		// compute T with the right parametrization
		Mat e2, e3;
		GetEpipolesFromTrifocalTensor(T, e2, e3);

		Mat E = Mat::zeros(27, 18, CV_32F);
		for (i = 0; i < 3; ++i)
			for (j = 0; j < 3; ++j)
				for (k = 0; k < 3; ++k) {
					E.at<float>(9 * i + 3 * j + k, 3 * i + j) = e3.at<float>(k, 0);
					E.at<float>(9 * i + 3 * j + k, 9 + 3 * i + k) = -e2.at<float>(j, 0);
				}

		Mat T2 = CondLeastSquares(A, E, 18);
		for (i = 0; i < 3; ++i)
			for (j = 0; j < 3; ++j)
				for (k = 0; k < 3; ++k)
					T.at<float>(i, j, k) = T2.at<float>(i * 9 + j * 3 + k, 0);
	}

	// denormalize
	Mat Tp = createTensor(3, 3, 3);
	trifocal_out = createTensor(3, 3, 3);
	for (i = 0; i < 3; ++i)
		sliceTensor(Tp, i) = normalizations[1] * sliceTensor(T, i) * normalizations[2].t();
	Mat Hinv = normalizations[0].inv();
	for (i = 0; i < 3; ++i)
		sliceTensor(trifocal_out, i) = Hinv.at<float>(0, i) * sliceTensor(Tp, 0)
		+ Hinv.at<float>(1, i) * sliceTensor(Tp, 1) + Hinv.at<float>(2, i) * sliceTensor(Tp, 2);
}
inline Mat homogeneous(const Mat & p) {
	const int n = p.size().height;
	Mat ret(n + 1, 1,CV_32F);
	for (int i = 0; i < n; ++i)
		ret.at<float>(i, 0) = p.at<float>(i, 0);
	ret.at<float>(n, 0) = 1.0f;
	return ret;
}

class EstimateTFTNL {
public:
	Mat f(const Mat & a_, const Mat b_, int i) const {
		Mat ret(6, 1,CV_32F);
		Mat P2(3, 4, CV_32F, ((float*)a_.ptr(0)));
		Mat P3(3, 4, CV_32F, ((float*)a_.ptr(0)) + 12);
		Mat b = homogeneous(b_);
		Mat p = b_;
		ret.at<float>(0, 0) = p.at<float>(0, 0) / p.at<float>(2, 0);
		ret.at<float>(1, 0) = p.at<float>(1, 0) / p.at<float>(2, 0);
		p = P2 * b;
		ret.at<float>(2, 0) = p.at<float>(0, 0) / p.at<float>(2, 0);
		ret.at<float>(3, 0) = p.at<float>(1, 0) / p.at<float>(2, 0);
		p = P3 * b;
		ret.at<float>(4, 0) = p.at<float>(0, 0) / p.at<float>(2, 0);
		ret.at<float>(5, 0) = p.at<float>(1, 0) / p.at<float>(2, 0);
		return ret;
	}

	Mat dA(const Mat & a_, const Mat & b_, int i) const {
		//TODO maybe parametrize a with only 11 parameters
		Mat ret = Mat::zeros(6, 24, CV_32F);
		Mat P2(3, 4,CV_32F, ((float*)a_.ptr(0)));
		Mat P3(3, 4, CV_32F, ((float*)a_.ptr(0)) + 12);
		Mat b = homogeneous(b_);
		Mat p = P2 * b;
		for (int k = 0; k < 2; ++k) {
			for (int j = 0; j < 3; ++j) {
				ret.at<float>(k + 2, k * 4 + j) = b_.at<float>(j, 0) / p.at<float>(2, 0);
				//ret(k+2,    (1-k)*4+j) = 0.0f;
				ret.at<float>(k + 2, 2 * 4 + j) = -b_.at<float>(j, 0) * p.at<float>(k, 0) / (p.at<float>(2, 0)*p.at<float>(2, 0));
			}
			ret.at<float>(k + 2, k * 4 + 3) = 1.0f / p.at<float>(2, 0);
			//ret(k+2,    (1-k)*4+3) = 0.0f;
			ret.at<float>(k + 2, 2 * 4 + 3) = -p.at<float>(k, 0) / (p.at<float>(2, 0)*p.at<float>(2, 0));
		}
		p = P3 * b;
		for (int k = 0; k < 2; ++k) {
			for (int j = 0; j < 3; ++j) {
				ret.at<float>(k + 4, 12 + k * 4 + j) = b_.at<float>(j, 0) / p.at<float>(2, 0);
				//ret(k+4, 12+(1-k)*4+j) = 0.0f;
				ret.at<float>(k + 4, 12 + 2 * 4 + j) = -b_.at<float>(j, 0) * p.at<float>(k, 0) / (p.at<float>(2, 0)*p.at<float>(2, 0));
			}
			ret.at<float>(k + 4, 12 + k * 4 + 3) = 1.0f / p.at<float>(2, 0);
			//ret(k+4, 12+(1-k)*4+3) = 0.0f;
			ret.at<float>(k + 4, 12 + 2 * 4 + 3) = -p.at<float>(k, 0) / (p.at<float>(2, 0)*p.at<float>(2, 0));
		}
		return ret;
	}

	Mat dB(const Mat & a_, const Mat & b_, int i) const {
		Mat P2(3, 4,CV_32F, ((float*)a_.ptr(0)));
		Mat P3(3, 4, CV_32F, ((float*)a_.ptr(0)) + 12);
		Mat ret(6, 3, CV_32F);
		Mat b = homogeneous(b_);
		Mat p = b_;
		ret.at<float>(0, 0) = 1.0f / p.at<float>(2, 0);
		ret.at<float>(0, 1) = 0.0f;
		ret.at<float>(0, 2) = -p.at<float>(0, 0) / (p.at<float>(2, 0)*p.at<float>(2, 0));
		ret.at<float>(1, 0) = 0.0f;
		ret.at<float>(1, 1) = 1.0f / p.at<float>(2, 0);
		ret.at<float>(1, 2) = -p.at<float>(1, 0) / (p.at<float>(2, 0)*p.at<float>(2, 0));
		p = P2 * b;
		for (int k = 0; k < 2; ++k)
			for (int i = 0; i < 3; ++i)
				ret.at<float>(k + 2, i) = (P2.at<float>(k, i) * p.at<float>(2, 0) - P2.at<float>(2, i) * p.at<float>(k, 0)) / (p.at<float>(2, 0)*p.at<float>(2, 0));
		p = P3 * b;
		for (int k = 0; k < 2; ++k)
			for (int i = 0; i < 3; ++i)
				ret.at<float>(k + 4, i) = (P3.at<float>(k, i) * p.at<float>(2, 0) - P3.at<float>(2, i) * p.at<float>(k, 0)) / (p.at<float>(2, 0)*p.at<float>(2, 0));
		return ret;
	}
};

//HZ p397, Algo 16.3
// Finds the trifocal tensor minimizing geometric error
void EstimateTrifocalTensorNonLinear(const Mat & points2d, Mat & trifocal_out, Mat* initial_guessP2, Mat* initial_guessP3, Mat* initial_guessX) 
{
	int nPoints = points2d.size().height;

	// compute initial guesses if not provided
	Mat init_P1, init_P2, init_P3, init_X(nPoints, 3,CV_32F);
	if (!(initial_guessP2 && initial_guessP3 && initial_guessX)) {
		assert(false); //EstimateTrifocalTensor doesn't work.
		/*
		EstimateTrifocalTensor(points2d, trifocal_out);
		GetCameraMatricesFromTrifocalTensor(trifocal_out, init_P1, init_P2, init_P3);
		initial_guessP2 = &init_P2;
		initial_guessP3 = &init_P3;
		for (int i = 0; i < nPoints; ++i) {
		cout << Triangulate(init_P1, init_P2, points2d[i].getTP1()) << endl;
		copyRow(Triangulate(init_P1, init_P2, points2d[i].getTP1()).t(), init_X, 0, i);
		}
		initial_guessX = &init_X;
		*/
	}
	Mat & initialP2 = *initial_guessP2;
	Mat & initialP3 = *initial_guessP3;
	Mat & initialX = *initial_guessX;

	Mat initial_a(24, 1,CV_32F);
	for (int i = 0; i < 12; ++i) {
		initial_a.at<float>(i, 0) = *(((float*)initialP2.ptr(0)) + i);
		initial_a.at<float>(i + 12, 0) = *(((float*)initialP3.ptr(0)) + i);
	}
	EstimateTFTNL etftnl;
	Mat a_, b;
	//TODO sigma = I...
	SparseLM(points2d, initial_a, initialX, etftnl, a_, b);
	Mat P1 = Mat::zeros(3, 4, CV_32F);
	P1.at<float>(0, 0) = P1.at<float>(1, 1) = P1.at<float>(2, 2) = 1.0f;
	Mat P2(3, 4, CV_32F, ((float*)a_.ptr(0)));
	Mat P3(3, 4, CV_32F, ((float*)a_.ptr(0)) + 12);
	GetTrifocalTensorFromCameraMatrices(P1, P2, P3, trifocal_out);
}

//HZ p 375, Algo 15.1 (iii)
// Finds three cameras matrices compatible with the trifocal tensor, the first
// one being the trivial camera (I|0) .
// If e1 and e2 are not provided, they are computed.
void GetCameraMatricesFromTrifocalTensor(const Mat& trifocal, Mat & P1, Mat & P2, Mat & P3,const Mat* e2_, const Mat* e3_) {
	// compute e2 and e3 if necessary
	Mat e2b, e3b;
	if ((e2_ == NULL) || (e3_ == NULL)) {
		GetEpipolesFromTrifocalTensor(trifocal, e2b, e3b);
		e2_ = &e2b;
		e3_ = &e3b;
	}
	const Mat & e2 = *e2_;
	const Mat & e3 = *e3_;

	// compute P1
	P1 = Mat::zeros(3, 4, CV_32F);
	P1.at<float>(0, 0) = P1.at<float>(1, 1) = P1.at<float>(2, 2) = 1.0f;

	// compute P2
	P2 = Mat(3, 4,CV_32F);
	for (int i = 0; i < 3; ++i)
		copyCol(sliceTensor(trifocal, i) * e3, P2, 0, i);
	copyCol(e2, P2, 0, 3);

	// compute P3
	P3 = Mat(3, 4,CV_32F);
	Mat M1 = e3 * e3.t();
	M1.at<float>(0, 0) -= 1.0f;
	M1.at<float>(1, 1) -= 1.0f;
	M1.at<float>(2, 2) -= 1.0f;
	Mat M2(3, 3,CV_32F);
	for (int i = 0; i < 3; ++i)
		copyCol(sliceTensor(trifocal, i).t() * e2, M2, 0, i);
	M1 = M1 * M2;
	for (int i = 0; i < 3; ++i)
		copyCol(M1, P3, i, i);
	copyCol(e3, P3, 0, 3);
}

class DistRobustTFT {
private:
	const Mat P1, P2, P3;
public:
	DistRobustTFT(const Mat & P1, const Mat & P2, const Mat & P3)
		:P1(P1), P2(P2), P3(P3) {}; //TODO could be optimized since P1 = (I|0)
	Mat f(const Mat & a_) const {
		Mat ret(6, 1,CV_32F);
		Mat a = homogeneous(a_);
		Mat p = P1 * a;
		ret.at<float>(0, 0) = p.at<float>(0, 0) / p.at<float>(2, 0);
		ret.at<float>(1, 0) = p.at<float>(1, 0) / p.at<float>(2, 0);
		p = P2 * a;
		ret.at<float>(2, 0) = p.at<float>(0, 0) / p.at<float>(2, 0);
		ret.at<float>(3, 0) = p.at<float>(1, 0) / p.at<float>(2, 0);
		p = P3 * a;
		ret.at<float>(4, 0) = p.at<float>(0, 0) / p.at<float>(2, 0);
		ret.at<float>(5, 0) = p.at<float>(1, 0) / p.at<float>(2, 0);
		return ret;
	}
	Mat dA(const Mat & a_) const {
		Mat ret(6, 3,CV_32F);
		Mat a = homogeneous(a_);
		Mat p = P1 * a;
		for (int k = 0; k < 2; ++k)
			for (int i = 0; i < 3; ++i)
				ret.at<float>(k, i) = (P1.at<float>(k, i) * p.at<float>(2, 0) - P1.at<float>(2, i) * p.at<float>(k, 0)) / (p.at<float>(2, 0)*p.at<float>(2, 0));
		p = P2 * a;
		for (int k = 0; k < 2; ++k)
			for (int i = 0; i < 3; ++i)
				ret.at<float>(k + 2, i) = (P2.at<float>(k, i) * p.at<float>(2, 0) - P2.at<float>(2, i) * p.at<float>(k, 0)) / (p.at<float>(2, 0)*p.at<float>(2, 0));
		p = P3 * a;
		for (int k = 0; k < 2; ++k)
			for (int i = 0; i < 3; ++i)
				ret.at<float>(k + 4, i) = (P3.at<float>(k, i) * p.at<float>(2, 0) - P3.at<float>(2, i) * p.at<float>(k, 0)) / (p.at<float>(2, 0)*p.at<float>(2, 0));
		return ret;
	}
};

Mat Triangulate3NonLinear(const Mat & P1, const Mat & P2, const Mat & P3,
	const Mat & X) {
	DistRobustTFT drtft(P1, P2, P3);
	Mat a_;
	LM(X, Triangulate(P1, P2, TrackedPoint(X.at<float>(0, 0), X.at<float>(1, 0), X.at<float>(2, 0), X.at<float>(3, 0))), drtft, a_);
	return a_;
}

float GetRobustTFTDist(const Mat & P1, const Mat & P2, const Mat & P3,
	const Mat & point2d, const DistRobustTFT & drtft) {
	Mat a_;
	LM(point2d, Triangulate(P1, P2, point2d), drtft, a_, 10, 1.0f);
	Mat a = homogeneous(a_);
	Mat p1 = P1 * a, p2 = P2 * a, p3 = P3 * a;
	Mat p(6, 1,CV_32F);
	p.at<float>(0, 0) = p1.at<float>(0, 0) / p1.at<float>(2, 0);
	p.at<float>(1, 0) = p1.at<float>(1, 0) / p1.at<float>(2, 0);
	p.at<float>(2, 0) = p2.at<float>(0, 0) / p2.at<float>(2, 0);
	p.at<float>(3, 0) = p2.at<float>(1, 0) / p2.at<float>(2, 0);
	p.at<float>(4, 0) = p3.at<float>(0, 0) / p3.at<float>(2, 0);
	p.at<float>(5, 0) = p3.at<float>(1, 0) / p3.at<float>(2, 0);
	return (p - point2d).dot(p - point2d);
}

//HZ p401, Algo 16.4
// Uses RANSAC to compute the trifocal tensor
void RobustTrifocalTensor(const Mat & points2d, Mat & trifocal_out, Mat* inliers_out) {
	const float proba = 0.99f; //once the RANSAC is done, this is the probability that
	//at least ont inlier has been choosen in an iteration. Reduce to speed up
	float thres = 15.0f;
	const size_t nPoints = points2d.size().height;
	vector<int> shuffle(nPoints);
	for (size_t i = 0; i < nPoints; ++i)
		shuffle[i] = i;
	int N = 100000;
	vector<vector<Mat> > cameras;
	vector<Mat> points3d_6pts, cameras_best;
	vector<int> inliers, best;
	size_t bestNInliers;
	float epsilon, sumErr, bestErr = 1e25f, d;
	for (int iIter = 0; iIter < N; ++iIter) {
		random_shuffle(shuffle.begin(), shuffle.end());
		cameras.clear(); //just to be sure there is no memory sharing with bests
		bestNInliers = 0;
		SixPointsAlgorithm(points2d, cameras, points3d_6pts, &shuffle);
		//TODO could do pruning

		for (size_t icam = 0; icam < cameras.size(); ++icam) {
			sumErr = 0.0f;
			inliers.clear();
			DistRobustTFT drtft(cameras[icam][0], cameras[icam][1], cameras[icam][2]);

			for (size_t i = 6; i < nPoints; ++i) {
				d = GetRobustTFTDist(cameras[icam][0], cameras[icam][1], cameras[icam][2],
					points2d.row(shuffle[i]).t(), drtft);
				if (d < thres) {
					inliers.push_back(shuffle[i]);
					sumErr += d;
				}
			}

			if ((inliers.size() + 6 > best.size()) ||
				((inliers.size() + 6 == best.size()) && (sumErr < bestErr))) {
				cout << "RANSAC: found a solution with " << inliers.size() + 6 << " inliers"
					<< " over " << points2d.size().height << ", err = " << sumErr << endl;
				best = inliers; //TODO no copy;
				best.insert(best.end(), shuffle.begin(), shuffle.begin() + 6);
				cameras_best = cameras[icam];
				bestErr = sumErr;
			}
			if (inliers.size() + 6 > bestNInliers)
				bestNInliers = inliers.size() + 6;
		}
		epsilon = (float)bestNInliers / (float)points2d.size().height;
		N = log(1.0f - proba) / log(1.0f - pow(epsilon, 6));
	}
	cout << "Final N=" << N << endl;
	Mat pointsInliers(best.size(), 6,CV_32F);
	for (size_t i = 0; i < best.size(); ++i)
		copyRow(points2d, pointsInliers, best[i], i);

	Mat P1, P2, P3; //TODO there must be a simpler way to set the first matrix to (I|0)
	GetTrifocalTensorFromCameraMatrices(cameras_best[0], cameras_best[1], cameras_best[2],
		trifocal_out);
	GetCameraMatricesFromTrifocalTensor(trifocal_out, P1, P2, P3);
	Mat pts3d(best.size(), 3,CV_32F);
	for (size_t i = 0; i < best.size(); ++i) {
		copyRow(Triangulate(P1, P2, subMat(points2d, i, i + 1, 0, 4)), pts3d, 0, i);
	}

	EstimateTrifocalTensorNonLinear(pointsInliers, trifocal_out, &P2, &P3, &pts3d);
	if (inliers_out)
		*inliers_out = pointsInliers;
}

//HZ p374, Section 15.1.4
void GetFundamentalMatricesFromTrifocalTensor(const Mat & trifocal, Mat & F1, Mat & F2,
	const Mat* e2_, const Mat* e3_) {
	// compute e2 and e3 if necessary
	Mat e2b, e3b;
	if ((e2_ == NULL) || (e3_ == NULL)) {
		GetEpipolesFromTrifocalTensor(trifocal, e2b, e3b);
		e2_ = &e2b;
		e3_ = &e3b;
	}
	const Mat & e2 = *e2_;
	const Mat & e3 = *e3_;

	F1 = Mat(3, 3,CV_32F);
	for (int i = 0; i < 3; ++i)
		copyCol(sliceTensor(trifocal, i) * e3, F1, 0, i);
	F1 = epscov(e2) * F1;

	F2 = Mat(3, 3, CV_32F);
	for (int i = 0; i < 3; ++i)
		copyCol(sliceTensor(trifocal, i).t() * e2, F2, 0, i);
	F2 = epscov(e3) * F2;
}

void GetMetricCamerasFromTrifocalTensor(const Mat & trifocal, const Mat & K,
	const TrackedPoint3 & one_point,
	Mat & P1, Mat & P2, Mat & P3,
	const Mat* e2, const Mat* e3) {
	GetCameraMatricesFromTrifocalTensor(trifocal, P1, P2, P3);
	Mat F1, F2;
	GetFundamentalMatricesFromTrifocalTensor(trifocal, F1, F2, e2, e3);
	Mat E1 = K.t() * F1 * K;
	Mat E2 = K.t() * F2 * K;
	Mat P2p = K * GetExtrinsicsFromEssential(E1, one_point.getTP1());
	Mat MK = P2(Range(0, 3), Range(0, 3)) * K;
	Mat m = Mat::zeros(9, 4, CV_32F);
	Mat a(9, 1, CV_32F), b;
	for (int i = 0; i < 3; ++i)
		for (int j = 0; j < 3; ++j) {
			m.at<float>(3 * i + j, 0) = P2p.at<float>(j, i);
			m.at<float>(3 * i + j, i + 1) = -P2.at<float>(j, 3);
			a.at<float>(3 * i + j, 0) = MK.at<float>(j, i);
		}
	b = m.inv(DECOMP_SVD) * a;
	float s = 0.0f;
	for (int i = 0; i < 3; ++i)
		s += P2p.at<float>(i, 3) / P2.at<float>(i, 3);
	s *= b.at<float>(0, 0) / 3.0f;
	Mat H(4, 4, 0.0f);
	for (int i = 0; i < 3; ++i) {
		for (int j = 0; j < 3; ++j)
			H.at<float>(i, j) = K.at<float>(i, j);
		H.at<float>(3, i) = b.at<float>(i + 1, 0);
	}
	H.at<float>(3, 3) = s;

	P1 = Mat::zeros(3, 4, CV_32F);
	P1.at<float>(0, 0) = P1.at<float>(1, 1) = P1.at<float>(2, 2) = 1.0f;
	P1 = P1 * H;
	P2 = P2 * H;
	P3 = P3 * H;
}

// Returns a matrix that should be close to 0 if the points are a match
Mat Check3PointsWithTrifocalTensor(const Mat & trifocal,
	const Mat & p1, const Mat & p2, const Mat & p3) {
	Mat p2cross = epscov(p2);
	Mat p3cross = epscov(p3);
	Mat T = sliceTensor(trifocal, 0) * p1.at<float>(0, 0) + sliceTensor(trifocal, 1) * p1.at<float>(1, 0)
		+ sliceTensor(trifocal, 2) * p1.at<float>(2, 0);
	return p2cross * T * p3cross;
}