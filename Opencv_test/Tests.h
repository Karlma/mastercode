#ifndef TESTS_H
#define TESTS_H

#include "opencv2/core.hpp"
#include "opencv2/core/utility.hpp"
#include "mylib.h"

#define TRIANG_LINEAR 0
#define TRIANG_OPENCV 1

int test_patch_normalize();
int test_seqslam();
int test_seqslam_result();
int test_seqslam_on_match();
int test_preprocess_seq();
int test_extract_features_grid();
int test_extract_features_grid_new();
int test_viz_installation();
int test_relative_init();
int test_relative_pose_using_matches();
int test_relative_match_init();
int test_relative_pose_using_matches_matching();
int test_stereo_pnp_match_init();
int test_stereo_pnp_relative_pose_using_matches_matching();
int test_trackbar();
int test_kslam();
int test_kslam_fab();
int test_kslam_lip6();
//todo move
int train_first_seq(int width, int height, int startidx, int endidx, char *pathleft, char *pathright);
int run_first_seq(char* trainpath, int startidx, int endidx, char *pathleft);
int test_read_one_file(char *path, int idx);

int test_singlematch_pnp(cv::Mat camleft, cv::Mat camright, 
						 cv::Mat left_a, cv::Mat right_a, 
						 cv::Mat left_b, cv::Point3f & Tout, cv::Mat & Rout);

int test_triangulate_linear(cv::Mat cameraleft, cv::Mat cameraright, 
	cv::Mat imgleft, cv::Mat imgright,
	std::vector<cv::KeyPoint> & kpleft, std::vector<cv::KeyPoint> & kpright,
	cv::Mat & descleft, cv::Mat & descright,
	grid_params params,
	std::vector<cv::Point3f>& points3d,
	cv::Mat & colors,
	int methode);

#endif // DEBUG