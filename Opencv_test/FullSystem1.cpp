#include <stdio.h>
#include <iostream>
#include <fstream>
#include "opencv2/core.hpp"
#include "opencv2/core/utility.hpp"
#include "opencv2/features2d.hpp"
#include "opencv2/xfeatures2d.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/calib3d.hpp"
#include "opencv2/video/tracking.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "Odometry.h"
#include "SeqSLAM.h"
#include "Tests.h"
#include "mylib.h"

using namespace cv;
using namespace std;

int relative_no_scale_test()
{
	vector<Point3d> scores;
	lib_read_gps_data_kitti(scores, "C:/Matlab/datasets/kitti/data_odometry_poses/dataset/poses/00.txt");
	vector<Mat> camMats;
	lib_read_kitti_poses(camMats, "C:/Matlab/datasets/kitti/data_odometry_poses/dataset/poses/00.txt");

	Mat traj = Mat::zeros(800, 800, CV_8UC3);
	//int Nimg = 4540;
	int Nimg = 4000;
	Mat system = Mat::zeros(Nimg, Nimg, CV_64F);
	vector<Mat> prevImgs;
	ofstream myfile;
	myfile.open("C:/Matlab/relativeposes_stereo.csv");
	ofstream myfile2;
	myfile2.open("C:/Matlab/relativeposes_stereo2.csv");
	int xoffset = 200;// 280;
	int yoffset = 565;//580;
	float focal = 718.8560;
	Point2d PP(607.1928, 185.2157);
	Ptr<ORB> detector = ORB::create(3000);
	for (int i = 0; i < Nimg; i++)
	{
		char path[100];
		sprintf_s(path, "C:/Matlab/datasets/kitti/00_preprocess/image_0/%06d.png", i);
		Mat img1 = imread(path, IMREAD_GRAYSCALE);
		//imshow("preprocessed", img1);
		Mat pimg;
		int match = 0;
		if (i > 3700){
			preprocessImage(img1, pimg);
			match = getMatch(pimg, prevImgs, system, i,100);
			if (match > 0)
			{
				sprintf_s(path, "C:/Matlab/datasets/kitti/00/image_0/%06d.png", i);
				Mat imga = imread(path, IMREAD_GRAYSCALE);
				//sprintf(path, "C:/Matlab/datasets/kitti/00/image_0/%06d.png", match);
				sprintf_s(path, "C:/Matlab/datasets/kitti/00/image_1/%06d.png", i);
				Mat imgb = imread(path, IMREAD_GRAYSCALE);
				vector<KeyPoint> kp1;
				vector<KeyPoint> kp2;
				Mat desc1;
				Mat desc2;
				vector<Point2f> poi1;
				vector<Point2f> poi2;
				//extractGoodFeatures(imgb, kp2, desc1);
				lib_extract_better_features(detector, imgb, kp2, 31, 64, 19,3,5);
				//detector->detect(imgb, kp2);
				KeyPoint::convert(kp2, poi2, vector<int>());
				vector<uchar> status;
				lib_feature_tracking(imgb, imga, poi2, poi1, status);
				
				//extractGoodFeatures(imgb, kp2, desc2);
				//vector<DMatch> matches;
				//matchFeatures(kp1, desc1, kp2, desc2, matches);
				//Mat img_matches;
				//drawMatches(imga, kp1, imgb, kp2, matches, img_matches);
				//imshow("match", img_matches);
				//waitKey(0);
				//KeyPoint::convert(poi1, kp1);
				//KeyPoint::convert(poi2, kp2);
				//drawKeypoints(imga, kp1, imga);
				//drawKeypoints(imgb, kp2, imgb);
				//Mat combined = Mat(imga.rows * 2, imga.cols,imga.type());
				//imga.copyTo(combined.colRange(0, imga.cols).rowRange(0, imga.rows));
				//imgb.copyTo(combined.colRange(0, imga.cols).rowRange(imga.rows, imga.rows* 2));
				//imshow("Match", combined);

				//KeyPoint::convert(kp1, poi1, vector<int>());
				//KeyPoint::convert(kp2, poi2, vector<int>());
				Point3d dist = scores[i] - scores[match];
				//double scale = norm(dist);
				Mat R, t, mask;
				int inliers = lib_get_relative_pose_from_points(poi1, poi2, focal, PP, R, t, mask);
				cout << "Inliers: " << inliers << endl;
				Mat PP = camMats[match];
				Mat RK = PP.rowRange(0, 3).colRange(0, 3);
				cout << "PP match" << PP << endl;
				Point2d tt2d(scores[match].x, scores[match].z);
				Point2d now2d(scores[i].x, scores[i].z);
				double scale = norm(tt2d-now2d);
				Point3d tnow = scores[i];
				cout << "t from recover" << t << endl;
				//pt = pt * scale;
				//Mat t2 = RK*(t * scale);
				// for stereo
				Mat t2 = RK*(t * 1);

				Point2d pt2d(t2.at<double>(0), t2.at<double>(2));

				cout << "tafter" << pt2d << endl;
				cout << "fasit" << now2d-tt2d << endl;
				double scale2 =  norm(now2d - (tt2d + pt2d));

				Point3d pt(t.at<double>(0), t.at<double>(1), t.at<double>(2));
				
				Point3d tthen = scores[match] + pt;
				Point3d dist2 = tnow - tthen;
				char text[100];
				sprintf_s(text, "%.4f,%.4f,%.4f,%d,%d\n", t.at<double>(0), t.at<double>(1), t.at<double>(2),match,i);
				myfile << text;
				sprintf_s(text, "%.4f,%.4f,%.4f,%d,%d\n", t2.at<double>(0), t2.at<double>(1), t2.at<double>(2), match, i);
				myfile2 << text;
				//double scale2 = norm(dist2);
				printf("%d -> %d s: %f, s2:%f\n", i, match,scale, scale2);
				circle(traj, Point(xoffset + tnow.x, yoffset - tnow.z) * 3, 0.5, CV_RGB(255, 255, 0), -1);
				circle(traj, Point(xoffset + scores[match].x, yoffset - scores[match].z) * 3, 0.5, CV_RGB(0, 255, 255), -1);
				circle(traj, Point(xoffset + tthen.x, yoffset - tthen.z)*3, 0.5, CV_RGB(0, 0, 255), -1);
				imshow("Trajectory", traj);
				//waitKey(0);
				//rectangle(traj, Point(10, 30), Point(550, 50), CV_RGB(0, 0, 0), CV_FILLED);
				//char text[100];
				//sprintf(text, "Coordinates: x = %.2f y = %01f z = %.2f  P: %d", tnow.x, tnow.y, tnow.z, i);
				//putText(traj, text, Point(10, 50), FONT_HERSHEY_PLAIN, 1, Scalar::all(255), 1, 8);
				//line(traj, Point(xoffset + tthen.x, yoffset - tthen.z), Point(xoffset + tnow.x, yoffset - tnow.z), CV_RGB(0, 0, 255), 1, 8, 0);
			}
		}
		Point3f tnow = scores[i];
		if (match)
		{
			circle(traj, Point(xoffset + tnow.x, yoffset - tnow.z)*3, 0.5, CV_RGB(0, 255, 0), -1);
		}
		else
		{
			circle(traj, Point(xoffset + tnow.x, yoffset - tnow.z)*3, 0.5, CV_RGB(255, 0, 0), -1);
		}
		prevImgs.push_back(img1);
		//prevImgs.push_back(pimg);
		//printf("%d\n", i);
		//waitKey(100);
	}
	imshow("Trajectory", traj);
	waitKey(0);
	double min;
	double max;
	minMaxIdx(system, &min, &max);
	Mat adjMap;
	convertScaleAbs(system, adjMap, 255 / max);
	imwrite("C:/Matlab/Gray_Image.png", adjMap);
	Mat sub = adjMap.rowRange(360, 960).colRange(3300, 3900);
	imshow("compsystem", sub);
	imwrite("C:/Matlab/Gray_Image.png", adjMap);
	Mat sub2 = system.rowRange(360, 960).colRange(3300, 3900);
	FileStorage file("C:/Matlab/Gray_Image.yml", FileStorage::WRITE);
	file << "system" << sub2;
	file.release();
	waitKey(0);
	return 1;
}