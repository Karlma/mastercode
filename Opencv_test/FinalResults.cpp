
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <chrono>
#include <time.h>
#include <ctime>
#include <math.h>
#include "opencv2/core.hpp"
#include "opencv2/core/utility.hpp"
#include "opencv2/features2d.hpp"
#include "opencv2/xfeatures2d.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/calib3d.hpp"
#include "opencv2/video/tracking.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/viz.hpp"
#include "Odometry.h"
#include "SeqSLAM.h"
#include "KSLAM.h"
#include "Tests.h"
#include "FullSystem1.h"
#include "mylib.h"
#include "trainandrun.h"
#include "ThreadVersion.h"
using namespace cv;
using namespace std;
using namespace std::chrono;


struct feature_data
{
	double x_mean = 0;
	double y_mean = 0;
	double x_std = 0;
	double y_std = 0;
	double avg_dist = 0;
};

// calculate x and y mean and std for keypoints.
int calculate_feature_data(feature_data * data, vector<KeyPoint> kp)
{
	double dist_sum = 0;
	int dists = 0;
	int W = 1241;
	int H = 376;
	const int divider = 16;
	auto array = new int[divider][divider]();
	for (int i = 0; i < kp.size(); i++)
	{
		Point2f p1 = kp[i].pt;
		int a = p1.x / (W / divider);
		int b = p1.y / (H / divider);
		array[a][b] += 1;
	}
	double avg = 0; 
	for (int i = 0; i < divider; i++)
	{
		for (int j = 0; j < divider; j++)
		{
			double test = (2000.0 / (divider*divider));
			//avg += array[i][j] <= test ? array[i][j] : test;
			avg += pow(array[i][j] - test, 2);
		}
	}

	data->avg_dist = sqrt(avg) / (divider * divider);

	double x_mean = 0;
	double y_mean = 0;
	double x_std = 0;
	double y_std = 0;
	for (int i = 0; i < kp.size(); i++)
	{
		x_mean += kp[i].pt.x;
		y_mean += kp[i].pt.y;
	}
	x_mean = x_mean / kp.size();
	y_mean = y_mean / kp.size();
	for (int i = 0; i < kp.size(); i++)
	{
		x_std += pow(kp[i].pt.x - x_mean, 2);
		y_std += pow(kp[i].pt.y - y_mean, 2);
	}
	x_std = sqrt(x_std) / kp.size();
	y_std = sqrt(y_std) / kp.size();
	data->x_mean = x_mean;
	data->y_mean = y_mean;
	data->x_std = x_std;
	data->y_std = y_std;
	return 1;
}

// My grid function is slow becasue of the intriquit steps of the opencv Orb detector.
// it can be improved in the same way ORBSLAM is doing it, by calculating the FAST corners first
// in the whole image. And then do the grid detection. This will improve performance to the same as
// opencvs implementation.
// one can clearly see from this algorithm that it improves the sperad of the feature points.
// making everything better( HOW is it better? how can it be shown?)
int run_orb_grid_results(Mat image, int imgidx, int maxfeaturs, int fromcols, int tocols, float mindist)
{
	int percell = 0;
	Ptr<ORB> grid_detector = ORB::create(200);

	char path[200];
	sprintf_s(path, "C:/Matlab/results/grid/grid_data_%d.csv", imgidx);
	ofstream gridfile;
	gridfile.open(path);

	ofstream normalfile;
	sprintf_s(path, "C:/Matlab/results/grid/normal_data_%d.csv", imgidx);
	normalfile.open(path);

	for (int cols = fromcols; cols <= tocols; cols += cols%2 == 0?2:1)
	{
		int rows = cols / 2;
		rows = rows > 0 ? rows : 1;
		int percell = maxfeaturs / (rows * cols);

		vector<KeyPoint> grid_kp;
		// will set max feature detector find to percell+6
		high_resolution_clock::time_point t1 = high_resolution_clock::now();
		lib_extract_better_features(grid_detector, image, grid_kp, 31, cols, rows, percell, mindist);
		high_resolution_clock::time_point t2 = high_resolution_clock::now();
		int grid_time = (int)duration_cast<milliseconds>(t2 - t1).count();

		feature_data grid_data;
		calculate_feature_data(&grid_data, grid_kp);

		Mat normal_image = Mat(image);
		Ptr<ORB> normal_detector = ORB::create(maxfeaturs);
		vector<KeyPoint> normal_kp;
		t1 = high_resolution_clock::now();
		normal_detector->detect(image, normal_kp);
		t2 = high_resolution_clock::now();
		int normal_time = (int)duration_cast<milliseconds>(t2 - t1).count();
		feature_data normal_data;
		calculate_feature_data(&normal_data, normal_kp);

		Mat grid_image = Mat(image);
		drawKeypoints(image, grid_kp, grid_image);
		drawKeypoints(image, normal_kp, normal_image);
		imshow("grid", grid_image);
		//imshow("normal", normal_image);

		sprintf_s(path, "C:/Matlab/results/grid/grid_%d_%d_%d_%.1f.jpg",cols, rows, percell, mindist);
		imwrite(path, grid_image);
		sprintf_s(path, "C:/Matlab/results/grid/normal_%d.jpg", maxfeaturs);
		imwrite(path, normal_image);
		
		char stringdata[200];
		sprintf_s(stringdata, "%d, %.4f, %.4f, %.4f, %.4f, %.4f, %d\n", cols, grid_data.x_mean, grid_data.x_std, grid_data.y_mean, grid_data.y_std, grid_data.avg_dist, grid_time);
		gridfile << stringdata;

		sprintf_s(stringdata, "%d, %.4f, %.4f, %.4f, %.4f, %.4f, %d\n", cols, normal_data.x_mean, normal_data.x_std, normal_data.y_mean, normal_data.y_std, normal_data.avg_dist, normal_time);
		normalfile << stringdata;
		waitKey(1);
	}
	gridfile.flush();
	gridfile.close();

	normalfile.flush();
	normalfile.close();
	return 1;
}

// simplefied just for pretty code.
int lib_extract_better_features_def(cv::Ptr<cv::ORB> detector, cv::Mat img, std::vector<cv::KeyPoint>& kpt)
{
	lib_extract_better_features(detector, img, kpt, 40, 16, 8, 6, 4);
	return 1;
}

Point2f reproject(Point3f p3d, Mat cam)
{
	Mat pp = Mat(4, 1, CV_64F);
	pp.at<double>(0, 0) = p3d.x;
	pp.at<double>(1, 0) = p3d.y;
	pp.at<double>(2, 0) = p3d.z;
	pp.at<double>(3, 0) = 1;

	Mat lepr = cam * pp;
	Point2f lepr2;
	double z = lepr.at<double>(2, 0);
	lepr2.x = lepr.at<double>(0, 0) / z;
	lepr2.y = lepr.at<double>(1, 0) / z;
	return lepr2;
}
// move to a function and use it everywhere.
// TODO calculate normal vector for epilines and use that for match filter instead.
// possible perforamnce boost here by searching epilines instead of matching,
// but for another time.
int verify_stereo_matching(Mat camera_left, Mat camera_right, Mat left, Mat right, int imgidx)
{
	Ptr<ORB> detector = ORB::create(200);
	char path[400];
	int from = 1;
	int too = 24;

	grid_params gp;
	gp.detector = ORB::create(200);
	gp.cellcols = 12;
	gp.cellrows = 6;
	gp.percell = 2000/(gp.cellcols * gp.cellrows);
	gp.maxdist = 1;
	ThreadConf tc;
	tc.cuttoffz = 6000;
	tc.stereo_matching_method = STEREO_BF;
	tc.use_sub_pixecl = 0;
	tc.gp = gp;
	tc.camleft = camera_left;
	tc.camright = camera_right;

	sprintf_s(path, "C:/Matlab/mathcing/stereo_vary_grid_BF_%d.csv", imgidx);
	ofstream gridfile;
	gridfile.open(path);
	char stringdata[200];

	for (int feats = 2; feats <= 68; feats+=2)
	{
		PreCalc * pc = new PreCalc();
		//tc.gp.cellcols = feats;
		//gridr = gridc / 2;
		tc.gp.percell = feats * 2;
		vector<KeyPoint> left_kp, right_kp;
		lib_extract_grid_features_params(left, left_kp, tc.gp);
		int features_found = left_kp.size();
		Mat left_desc;
		tc.gp.detector->compute(left, left_kp, left_desc);
		triangulate_thread(left, right, left_kp, left_desc, pc, &tc);

		int matches = pc->kp1.size();
		int numcorrect = 0;
		int numwrong = 0;
		vector<Point2f> left_repr, right_repr;
		double avgerr = 0;
		double avgerl = 0;
		for (int k = 0; k < pc->kp1.size(); k++)
		{
			Point2f pp1 = pc->kp1[k].pt;
			Point2f pp2 = pc->kp2[k].pt;
			if (pp1.y > pp2.y - 2 && pp1.y < pp2.y + 2 && pp1.x >= pp2.x - 40 && pp1.x <= pp2.x + 100)
			{
				numcorrect++;
			}
			else{
				numwrong++;
			}

			Point3f p3d = pc->pointsnear[k];
			
			Point2f lepr2 = reproject(p3d, camera_left);
			avgerl += norm(lepr2 - pp1);
			Point2f repr2 = reproject(p3d, camera_right);
			avgerr += norm(repr2 - pp2);
			left_repr.push_back(lepr2);
			right_repr.push_back(repr2);

		}
		avgerl = avgerl / matches;
		avgerr = avgerr / matches;
		cout << "all: " << features_found << endl;
		cout << "Avg left re: " << avgerl << endl;
		cout << "avg right re: " << avgerr << endl;
		cout << "feats: " << feats << endl;
		cout << "correct: " << numcorrect << endl;
		cout << "wrong: " << numwrong << endl;
		double pressition = ((numcorrect*1.0) / matches) * 100;
		cout << "pres: " <<pressition<<" %" << endl;

		sprintf_s(stringdata, "%d, %d, %d, %d, %f, %f\n", feats, features_found, numcorrect, numwrong, avgerl, avgerr);
		gridfile << stringdata;

		//imshow("left", left);
		//waitKey(10);
	}
	gridfile.flush();
	gridfile.close();
	return 1;
}

// not much i can do. only compare disparity to check if matches is correct.
int verify_triangulation(Mat camera_left, Mat camera_right, Mat left, Mat right)
{
	grid_params gp;
	gp.detector = ORB::create(2000);
	gp.cellcols = 16;
	gp.cellrows = 16;
	gp.percell = 40;
	gp.maxdist = 2;

	Mat leftcam(camera_left);
	Mat rightcam(camera_right);
	Mat K, rot, trans;
	decomposeProjectionMatrix(leftcam, K, rot, trans);
	Ptr<ORB> detector = ORB::create(200);
	vector<KeyPoint> left_kp, right_kp;
	Mat left_desc, right_desc;
	Mat allColors(0, 0, CV_8UC3);
	vector<Point3f> points3d;
	int pointsfound = test_triangulate_linear(leftcam, rightcam,
		left, right,
		left_kp, right_kp,
		left_desc, right_desc,
		gp, points3d, allColors, TRIANG_LINEAR);

	viz::Viz3d myWindow("Coordinate Frame");
	/// Add coordinate axes
	myWindow.showWidget("Coordinate Widget", viz::WCoordinateSystem());
	Mat testt(points3d);
	viz::WCloud cws(testt, allColors);
	cws.setRenderingProperty(viz::POINT_SIZE, 3);
	myWindow.showWidget("CloudWidget2", cws);
	/// Add coordinate axes
	myWindow.showWidget("Coordinate Widget", viz::WCoordinateSystem());
	/// Let's assume camera has the following properties
	Vec3d cam_pos(1.0f, 0.0f, 0.0f), cam_focal_point(1, 0, 1), cam_y_dir(0.0f, 1.0f, 0.0f);
	/// We can get the pose of the cam using makeCameraPose
	Affine3f cam_pose = viz::makeCameraPose(cam_pos, cam_focal_point, cam_y_dir);
	viz::WCameraPosition cpw(0.5); // Coordinate axes
	viz::WCameraPosition cpw_frustum(Vec2f(0.889484, 0.523599)); // Camera frustum
	myWindow.showWidget("CPW", cpw, cam_pose);
	myWindow.showWidget("CPW_FRUSTUM", cpw_frustum, cam_pose);
	myWindow.spinOnce(1, true);

	Mat imga, imgb, imgc;
	//drawKeypoints(imgb, kp1, imgb);
	drawKeypoints(left, left_kp, imga);
	drawKeypoints(right, right_kp, imgb);

	//drawKeypoints(imgc, kp2, imgc);
	//lib_combine_two_images(imga, imgb, combined);
	imshow("Match_a", imga);
	//imshow("Match_c", imgc);
	imshow("Match_b", imgb);
	waitKey(0);
	for (int i = 0; i < points3d.size(); i++)
	{
		cout << points3d[i] << endl;
	}
	return 1;
}

int verify_pnp_with_odometry(Mat camleft, Mat camright)
{

	Mat Rtot = Mat::eye(3, 3, CV_64FC1);
	Mat ttot = Mat::zeros(3, 1, CV_64FC1);
	char myfile_path[200];
	char myfile_path2[200];
	char text[400];
	long filetime = std::time(0);
	sprintf_s(myfile_path, "C:/Matlab/pnp_odometry/final_%d.csv", filetime);
	sprintf_s(myfile_path2, "C:/Matlab/pnp_odometry/relative_%d.csv", filetime);
	ofstream finalf, relative;
	finalf.open(myfile_path);
	relative.open(myfile_path2);
	// test on 200 first images.
	int frames_to_skip = 1;
	Mat R = Mat::eye(3, 3, CV_64FC1);
	Mat t = Mat::zeros(3, 1, CV_64FC1);
	for (int i = frames_to_skip; i < 4540; i += frames_to_skip)
	{
		high_resolution_clock::time_point t1 = high_resolution_clock::now();
		int now = i;
		int prev = i - frames_to_skip;

		char path[100];
		sprintf_s(path, "C:/Matlab/datasets/kitti/00/image_0/%06d.png", prev);
		Mat left_a = imread(path, IMREAD_GRAYSCALE);
		sprintf_s(path, "C:/Matlab/datasets/kitti/00/image_1/%06d.png", prev);
		Mat right_a = imread(path, IMREAD_GRAYSCALE);
		sprintf_s(path, "C:/Matlab/datasets/kitti/00/image_0/%06d.png", now);
		Mat left_b = imread(path, IMREAD_GRAYSCALE);

		Point3f T;
		Mat CR;
		test_singlematch_pnp(camleft, camright, left_a, right_a, left_b, T, CR);
		cout << "from: " << prev << " -> " << now << endl;
		cout << "T: " << T << endl;

		Mat Ct(3,1, CV_64FC1);
		Ct.at<double>(0, 0) = T.x;
		Ct.at<double>(1, 0) = T.y;
		Ct.at<double>(2, 0) = T.z;
		Mat ttt = R*Ct;
		t = t  + ttt;
		R = CR * R;

		sprintf_s(text, "%.4f,%.4f,%.4f,%.4f, %.4f,%.4f,%.4f,%.4f, %.4f,%.4f,%.4f,%.4f\n",
			R.at<double>(0, 0), R.at<double>(0, 1), R.at<double>(0, 2), t.at<double>(0, 0),
			R.at<double>(1, 0), R.at<double>(1, 1), R.at<double>(1, 2), t.at<double>(1, 0),
			R.at<double>(2, 0), R.at<double>(2, 1), R.at<double>(2, 2), t.at<double>(2, 0));
		finalf << text;
		finalf.flush();

		sprintf_s(text, "%.4f,%.4f,%.4f,%d,%d\n",T.x,T.y,T.z, prev, now);
		relative << text;
		relative.flush();
		high_resolution_clock::time_point t2 = high_resolution_clock::now();
		auto duratation = duration_cast<milliseconds>(t2 - t1).count();
		cout << duratation << "ms\n";
	}
	return 1;
}

int verify_odometry_threaded_kitti(Mat camleft, Mat camright)
{
	char text[400];
	char path[200];
	int methods[] = { SOLVEPNP_P3P, SOLVEPNP_DLS, SOLVEPNP_EPNP, SOLVEPNP_ITERATIVE };
	//for (int method = 1; method < 4; method++){
		for (int j = 8; j <= 11; j++)
		{
			int dataset = j;

			long filetime = std::time(0);
			sprintf_s(text, "%02d.txt", dataset);
			string data = text;
			string filea = "C:/Matlab/with_loop/" + data;
			sprintf_s(path, "C:/Matlab/datasets/kitti/%02d/times.txt", dataset);
			vector<double> poses;
			lib_read_times(poses, path);
			grid_params gp;
			gp.detector = ORB::create(1000);
			gp.cellcols = 12;
			gp.cellrows = 6;
			gp.percell = 18;
			gp.maxdist = 1;

			ThreadConf tc;
			tc.use_kalman = 0;
			tc.use_sub_pixecl = 0;
			tc.cuttoffz = 5000;
			tc.stereo_matching_method = STEREO_SAD;
			tc.PNP_method = SOLVEPNP_P3P;
			tc.gp = gp;
			tc.use_pr = 1;
			tc.camleft = camleft;
			tc.camright = camright;
			//tc.KSLAM_thresh = 0.62; //pyramid
			tc.KSLAM_thresh = 42;
			tc.KSLAM_seq_length = 10;
			tc.use_refinement = 0;
			tc.PNP_projection_error = 1;

			init_system(filea, tc);
			//poses.size()
			for (int i = 0; i <poses.size(); i += 1)
			{
				if (i%100 == 0) {
					write_all_to_file();
				}
				sprintf_s(path, "C:/Matlab/datasets/kitti/%02d/image_0/%06d.png", dataset, i);
				Mat left = imread(path, IMREAD_GRAYSCALE);
				sprintf_s(path, "C:/Matlab/datasets/kitti/%02d/image_1/%06d.png", dataset, i);
				Mat right = imread(path, IMREAD_GRAYSCALE);
				step_first(left, right);
				left.release();
				right.release();
			}
			cout << "seq: " << dataset << " done" << endl;
			write_all_to_file();
		}
	//}
	return 1;
}

int verify_odometry_threaded_diplod()
{
	double P0[3 * 4] = { 4.880000e+002, 0.000000e+000, 1.456488e+002, 0.000000e+000,
						 0.000000e+000, 4.880000e+002, 1.260785e+002, 0.000000e+000,
						 0.000000e+000, 0.000000e+000, 1.000000e+000, 0.000000e+000 };

	double P1[3 * 4] = { 4.880000e+002, 0.000000e+000, 1.456488e+002, -4.685706e+004,
						 0.000000e+000, 4.880000e+002, 1.260785e+002, 0.000000e+000,
						 0.000000e+000, 0.000000e+000, 1.000000e+000, 0.000000e+000 };

	Mat left_cam = Mat(3, 4, CV_64F, &P0);
	Mat right_cam = Mat(3, 4, CV_64F, &P1);

	cout << left_cam << endl;
	cout << right_cam << endl;

	char text[400];
	char path[200];
	string filea = "C:/Matlab/maria/final.txt";
	grid_params gp;
	gp.detector = ORB::create(10000);
	gp.cellcols = 8;
	gp.cellrows = 4;
	gp.percell = 60;
	gp.maxdist = 1;
	ThreadConf tc;
	tc.camleft = left_cam;
	tc.camright = right_cam;
	init_system(filea, tc);
	for (int i = 0; i <440; i += 1)
	{
		sprintf_s(path, "C:/Matlab/datasets/gtseq/diplo%06d-L.png", i);
		Mat left = imread(path, IMREAD_GRAYSCALE);
		sprintf_s(path, "C:/Matlab/datasets/gtseq/diplo%06d-R.png", i);
		Mat right = imread(path, IMREAD_GRAYSCALE);
		step_first(left, right);
		//cout << i << endl;
	}
	write_all_to_file();
	return 1;
}


int verify_odometry_threaded_malaga()
{
	double P0[3 * 4] = { 795.11588, 0.000000e+000, 517.12973, 0.000000e+000,
		0.000000e+000, 795.11588, 395.59665, 0.000000e+000,
						0.000000e+000, 0.000000e+000, 1.000000e+000, 0.000000e+000 };

	double P1[3 * 4] = { 795.11588, 0.000000e+000, 517.12973, -94.9933,
		0.000000e+000, 795.11588, 395.59665, 0.000000e+000,
		0.000000e+000, 0.000000e+000, 1.000000e+000, 0.000000e+000 };

	Mat left_cam = Mat(3, 4, CV_64F, &P0);
	Mat right_cam = Mat(3, 4, CV_64F, &P1);

	Mat K, rot, trans;
	decomposeProjectionMatrix(right_cam, K, rot, trans);

	cout << K << endl;
	cout << rot << endl;
	cout << trans << endl;
	trans.at<double>(0, 0) = trans.at<double>(0, 0) / trans.at<double>(3, 0);
	trans.at<double>(1, 0) = trans.at<double>(1, 0) / trans.at<double>(3, 0);
	trans.at<double>(2, 0) = trans.at<double>(2, 0) / trans.at<double>(3, 0);
	trans = -trans;

	int W = 1024;
	int H = 768;

	int cropaway = H / 2;
	double ratio = 0.5;

	K.at<double>(1, 2) = K.at<double>(1, 2) - cropaway;
	K.at<double>(0, 0) = K.at<double>(0, 0) * ratio;
	K.at<double>(1, 1) = K.at<double>(1, 1) * ratio;
	K.at<double>(0, 2) = K.at<double>(0, 2) * ratio;
	K.at<double>(1, 2) = K.at<double>(1, 2) * ratio;

	Mat c_right = Mat::zeros(3, 4, CV_64F);
	rot.copyTo(c_right.colRange(0, 3).rowRange(0, 3));
	trans.rowRange(0,3).copyTo(c_right.col(3));
	c_right = K*c_right;

	Mat c_left = Mat::zeros(3, 4, CV_64F);
	rot.copyTo(c_left.colRange(0, 3).rowRange(0, 3));
	c_left = K*c_left;

	cout << c_left << endl;
	cout << c_right << endl;

	vector<String> filenames; // notice here that we are using the Opencv's embedded "String" class
	String folder = "C:/Matlab/datasets/malaga-urban-dataset-extract-07/malaga-urban-dataset-extract-07_rectified_1024x768_Images/"; // again we are using the Opencv's embedded "String" class

	glob(folder, filenames); // new function that does the job ;-)

	char text[400];
	char path[200];
	string filea = "C:/Matlab/malag/final6.txt";
	grid_params gp;
	gp.detector = ORB::create(10000);
	gp.cellcols = 12;
	gp.cellrows = 6;
	gp.percell = 60;
	gp.maxdist = 1;

	ThreadConf tc;
	tc.camleft = c_left;
	tc.camright = c_right;
	init_system(filea, tc);

	for (size_t i = 0; i < filenames.size(); i+=2)
	{
		Mat left = imread(filenames[i], IMREAD_GRAYSCALE);
		left = left.rowRange(cropaway, left.rows);
		resize(left, left, Size(), ratio, ratio);
		
		Mat right = imread(filenames[i+1], IMREAD_GRAYSCALE);
		right = right.rowRange(cropaway, right.rows);
		resize(right, right, Size(), ratio, ratio);
		step_first(left, right);
	}

	write_all_to_file();
	return 1;
}
