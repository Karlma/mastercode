
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <chrono>
#include <time.h>
#include <ctime>
#include "opencv2/core.hpp"
#include "opencv2/core/utility.hpp"
#include "opencv2/features2d.hpp"
#include "opencv2/xfeatures2d.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/calib3d.hpp"
#include "opencv2/video/tracking.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "Odometry.h"
#include "SeqSLAM.h"
#include "KSLAM.h"
#include "Tests.h"
#include "FullSystem1.h"
#include "mylib.h"
#include "trainandrun.h"
#include "FinalResults.h"

using namespace cv;
using namespace std;
using namespace std::chrono;


int tets_training_writer()
{
	char *left = "C:/Matlab/datasets/kitti/00/image_0";
	char *right = "C:/Matlab/datasets/kitti/00/image_1";
	train_first_seq(0, 0, 391, 945, left, right);
	return 0;
}


int train_first_seq(int width, int height, int startidx, int endidx, char *pathleft, char *pathright)
{
	vector<Mat> camMats;
	lib_read_kitti_poses(camMats, "C:/Matlab/datasets/kitti/data_odometry_poses/dataset/poses/00.txt");
	init_runner("C:/Matlab/trainingfiles");
	for (int i = startidx; i <= endidx; i++)
	{
		// No need to patch normalize ass its allready done.
		// TODO add option for it.
		char pathleftimg[100];
		char pathrightimg[100];
		sprintf(pathleftimg, "%s/%06d.png", pathleft, i);
		sprintf(pathrightimg, "%s/%06d.png", pathright, i);
		Mat leftimg = imread(pathleftimg, IMREAD_GRAYSCALE);
		Mat rightimg = imread(pathrightimg, IMREAD_GRAYSCALE);

		int index = triangulate_and_save(leftimg, rightimg, camMats[i]);
		if (index == -1)
		{
			
		}
		printf("%d:%d\n", i - startidx, endidx - startidx);
	}
	//waitKey(0);
	return 1;
}

int test_full_run_on_pretrained()
{
	char path[100];
	char *left = "C:/Matlab/datasets/kitti/00/image_0";
	char *right = "C:/Matlab/datasets/kitti/00/image_1";
	KSLAM *kslam = new KSLAM();
	cout << "Traning seqslam" << endl;
	for (int i = 391; i < 945; i++){
		//sprintf_s(path, "C:/Matlab/datasets/kitti/00/image_0/%06d.png", i);
		sprintf_s(path, "C:/Matlab/datasets/kitti/00_preprocess/image_0/%06d.png", i);
		Mat img = imread(path, IMREAD_GRAYSCALE);
		kslam->add_normalized_image(img);
		cout << i << " : " << 945 << endl;
	}


	char myfile_path[200];
	long filetime = std::time(0);
	sprintf_s(myfile_path, "C:/Matlab/relativeposes_new_%d.csv", filetime);
	ofstream myfile;
	myfile.open(myfile_path);

	static Ptr<ORB> detector = ORB::create(10000);
	Mat traj = Mat::zeros(800, 800, CV_8UC3);
	int xoffset = 280;// 280;
	int yoffset = 580;//580;
	vector<Mat> camMats;
	lib_read_kitti_poses(camMats, "C:/Matlab/datasets/kitti/data_odometry_poses/dataset/poses/00.txt");
	int numMatches = 0;
	sprintf_s(path, "C:/Matlab/datasets/kitti/00/image_0/%06d.png", 0);
	Mat imgf = imread(path, IMREAD_GRAYSCALE);
	Mat combined = Mat(imgf.rows * 2, imgf.cols, imgf.type());
	for (int i = 3395; i < 3848; i += 1)
	{
		sprintf_s(path, "C:/Matlab/datasets/kitti/00/image_0/%06d.png", i);
		Mat leftimg = imread(path, IMREAD_GRAYSCALE);
		
		Point3f tnow(camMats[i].at<double>(0, 3), camMats[i].at<double>(1, 3), camMats[i].at<double>(2, 3));
		circle(traj, Point(xoffset + tnow.x, yoffset - tnow.z), 1, CV_RGB(0, 0, 255), -1);
		imshow("traj", traj);
		int bestMatch = kslam->find_best_from_train(leftimg);
		int bestoffset = 391 + bestMatch;

		printf("%d -> %d\n", i, bestoffset);

		char filename[100];
		sprintf(filename, "C:/Matlab/manypoints/stereo%06d.yml", bestoffset);
		PreProImg ppi = read_one_file(filename);

		high_resolution_clock::time_point t1 = high_resolution_clock::now();

		vector<KeyPoint> kp1;
		Mat desc1;
		//lib_extract_better_features(detector, leftimg, kp1, 31, 16, 6, 20, 5);
		lib_extract_better_features(detector, leftimg, kp1, 31, 16, 6, 40, 4);
		detector->compute(leftimg, kp1, desc1);
		vector<DMatch> matches1;
		lib_match_descriptors(desc1, ppi.desc1, matches1);
		lib_filter_matches(kp1, ppi.kp1, matches1);

		vector<Point3f> poi3dfor3;
		for (int j = 0; j < matches1.size(); j++)
		{
			Point3f p3f;
			Mat pmat = ppi.points3d.row(matches1[j].trainIdx);
			p3f.x = pmat.at<float>(0, 0);
			p3f.x = pmat.at<float>(0, 1);
			p3f.x = pmat.at<float>(0, 2);
			poi3dfor3.push_back(p3f);
		}

		double P0[3 * 4] = { 7.188560000000e+02, 0.000000000000e+00, 6.071928000000e+02, 0.000000000000e+00,
			0.000000000000e+00, 7.188560000000e+02, 1.852157000000e+02, 0.000000000000e+00,
			0.000000000000e+00, 0.000000000000e+00, 1.000000000000e+00, 0.000000000000e+00 };
		double P1[3 * 4] = { 7.188560000000e+02, 0.000000000000e+00, 6.071928000000e+02, -3.861448000000e+02,
			0.000000000000e+00, 7.188560000000e+02, 1.852157000000e+02, 0.000000000000e+00,
			0.000000000000e+00, 0.000000000000e+00, 1.000000000000e+00, 0.000000000000e+00 };
		Mat PPP = Mat(3, 4, CV_64F, &P0);
		Mat PPP2 = Mat(3, 4, CV_64F, &P1);
		Mat K = PPP.colRange(0, 3);
		vector<Point2f> imgapoints;
		KeyPoint::convert(kp1, imgapoints);
		Mat rvec, tvec;
		Mat pnpmask;
		solvePnPRansac(poi3dfor3, imgapoints, K, noArray(), rvec, tvec, false, 200, 20, 0.98, pnpmask, SOLVEPNP_ITERATIVE); //CV_P3P
		cout << "rvec: " << rvec << "\n";
		cout << "tvec: " << tvec << "\n";
		char text[300];
		sprintf_s(text, "%.4f,%.4f,%.4f,%d,%d\n", tvec.at<double>(0), tvec.at<double>(1), tvec.at<double>(2), bestoffset, i);
		myfile << text;


		high_resolution_clock::time_point t2 = high_resolution_clock::now();
		auto duratation = duration_cast<milliseconds>(t2 - t1).count();
		cout << duratation << "ms\n";

		vector<KeyPoint> kpfa;
		vector<KeyPoint> kpfb;
		cout << "pnpmask type: " << lib_type2str(pnpmask.type()) << endl;
		for (int j = 0; j < pnpmask.rows; j++)
		{
			int idx = pnpmask.at<int>(j, 0);
			kpfa.push_back(kp1[idx]);
			kpfb.push_back(ppi.kp1[idx]);
		}

		drawKeypoints(leftimg, kpfa, leftimg);
		imshow("curimg", leftimg);
		sprintf_s(path, "C:/Matlab/datasets/kitti/00/image_0/%06d.png", bestoffset);
		Mat img2 = imread(path, IMREAD_GRAYSCALE);
		drawKeypoints(img2, kpfb, img2);
		imshow("foundimg", img2);

		if (waitKey(1) == 's')
		{
			kslam->show_system_image();
			waitKey(0);
		}
		waitKey(0);
	}
	return 0;
}

int run_final_stuff()
{
	Mat image;
	int imageidx = 0;
	int maxfeaturs = 1600;
	int from = 12;
	int too = 14;
	float mindist = 2;

	char path[300];
	for (int i = 0; i < 100; i++)
	{
		imageidx = i * 15;
		sprintf_s(path, "C:/Matlab/datasets/kitti/00/image_0/%06d.png", imageidx);
		//image = imread(path, IMREAD_GRAYSCALE);
		//run_orb_grid_results(image, 0, maxfeaturs, from, too, mindist);
	}
	

	double P0[3 * 4] = { 7.188560000000e+02, 0.000000000000e+00, 6.071928000000e+02, 0.000000000000e+00,
		0.000000000000e+00, 7.188560000000e+02, 1.852157000000e+02, 0.000000000000e+00,
		0.000000000000e+00, 0.000000000000e+00, 1.000000000000e+00, 0.000000000000e+00 };

	double P1[3 * 4] = { 7.188560000000e+02, 0.000000000000e+00, 6.071928000000e+02, -3.861448000000e+02,
		0.000000000000e+00, 7.188560000000e+02, 1.852157000000e+02, 0.000000000000e+00,
		0.000000000000e+00, 0.000000000000e+00, 1.000000000000e+00, 0.000000000000e+00 };

	Mat left_cam = Mat(3, 4, CV_64F, &P0);
	Mat right_cam = Mat(3, 4, CV_64F, &P1);
	/*for (int i = 0; i < 43; i++)
	{
		imageidx = i * 15;
		sprintf_s(path, "C:/Matlab/datasets/kitti/00/image_0/%06d.png", imageidx);
		Mat left = imread(path, IMREAD_GRAYSCALE);
		sprintf_s(path, "C:/Matlab/datasets/kitti/00/image_1/%06d.png", imageidx);
		Mat right = imread(path, IMREAD_GRAYSCALE);
		verify_stereo_matching(left_cam, right_cam, left, right, imageidx);
	}*/
	//verify_triangulation(left_cam, right_cam, left, right);

	//verify_pnp_with_odometry(left_cam,right_cam);

	verify_odometry_threaded_kitti(left_cam, right_cam);
	//verify_odometry_threaded_diplod();
	//verify_odometry_threaded_malaga();

	return 1;
}

int run_relative_pose_on_pre_matched_image_indexes()
{

	vector<int> va;
	vector<int> vb;
	lib_read_matches(va, vb, "C:/Matlab/ksalm/matches_15_42_1456358588.csv");
	int num_matches = (int)va.size();
	Mat Rtot = Mat::eye(3, 3, CV_64FC1);
	Mat ttot = Mat::zeros(3, 1, CV_64FC1);
	char myfile_path[200];
	long filetime = std::time(0);
	sprintf_s(myfile_path, "C:/Matlab/rel_on_match_test_%d.csv", filetime);
	ofstream myfile;
	myfile.open(myfile_path);
	vector<Mat> GPSPoses;
	lib_read_kitti_poses(GPSPoses, "C:/Matlab/datasets/kitti/data_odometry_poses/dataset/poses/00.txt");
	double P0[3 * 4] = { 7.188560000000e+02, 0.000000000000e+00, 6.071928000000e+02, 0.000000000000e+00,
		0.000000000000e+00, 7.188560000000e+02, 1.852157000000e+02, 0.000000000000e+00,
		0.000000000000e+00, 0.000000000000e+00, 1.000000000000e+00, 0.000000000000e+00 };

	double P1[3 * 4] = { 7.188560000000e+02, 0.000000000000e+00, 6.071928000000e+02, -3.861448000000e+02,
		0.000000000000e+00, 7.188560000000e+02, 1.852157000000e+02, 0.000000000000e+00,
		0.000000000000e+00, 0.000000000000e+00, 1.000000000000e+00, 0.000000000000e+00 };
	Mat camleft = Mat(3, 4, CV_64F, &P0);
	Mat camright = Mat(3, 4, CV_64F, &P1);

	for (int i = 0; i < num_matches; i++)
	{
		int now = va[i];
		int match = vb[i];

		Mat poseb;
		GPSPoses[now].copyTo(poseb);
		Mat row = Mat::zeros(1, 4, CV_64F);
		row.at<double>(0, 3) = 1;
		poseb.push_back(row);
		Mat posb;
		poseb.col(3).copyTo(posb);
		//cout << posb << endl;

		Mat posea;
		GPSPoses[match].copyTo(posea);
		row = Mat::zeros(1, 4, CV_64F);
		row.at<double>(0, 3) = 1;
		posea.push_back(row);
		Mat posa;
		posea.col(3).copyTo(posa);
		//cout << posa << endl;
		Mat relative = posea.inv()*posb;
		//cout << relative << endl;

		//Point3f rel = tleft_a - tleft_b;
		Point3f rel = Point3f(relative.at<double>(0, 0), relative.at<double>(1, 0), relative.at<double>(2, 0));
		Point3f T;
		Mat R;
		char path[100];
		sprintf(path, "C:/Matlab/datasets/kitti/00/image_0/%06d.png", match);
		Mat left_a = imread(path, IMREAD_GRAYSCALE);
		sprintf(path, "C:/Matlab/datasets/kitti/00/image_1/%06d.png", match);
		Mat right_a = imread(path, IMREAD_GRAYSCALE);
		sprintf(path, "C:/Matlab/datasets/kitti/00/image_0/%06d.png", now);
		Mat left_b = imread(path, IMREAD_GRAYSCALE);
		test_singlematch_pnp(camleft, camright, left_a, right_a, left_b, T, R);
		cout << "from: " << match << " -> " << now << endl;
		cout << "rel: " << rel << endl;
		cout << "T: " << T << endl;
		Mat TT = Mat::zeros(3, 1, CV_64FC1);
		TT.at<double>(0, 0) = T.x;
		TT.at<double>(1, 0) = T.y;
		TT.at<double>(2, 0) = T.z;

		char text[300];
		sprintf_s(text, "%.4f,%.4f,%.4f,%d,%d\n", T.x, T.y, T.z, match, now);
		myfile << text;

		myfile.flush();
		//ttot = ttot + (R*TT);
		//Rtot = R * Rtot;
		//cout << TT << endl;
		//cout << R << endl;
		//cout << Rtot << endl;
		//cout << ttot << endl;
	}
	return 1;
}

int trainandruntraining() 
{
	//train_first_seq(376, 1241, 407, 936, "C:/Matlab/datasets/kitti/00/image_0", "C:/Matlab/datasets/kitti/00/image_1");
	init_runner("C:/Matlab/trainingfiles");
	read_training_data();
	for (int i = 3412; i < 3841; i++)
	{
		char pathleftimg[100];
		sprintf(pathleftimg, "C:/Matlab/datasets/kitti/00/image_0/%06d.png", i);
		Mat leftimg = imread(pathleftimg, IMREAD_GRAYSCALE);
		do_single_step(leftimg);
	}
	return 1;
}
int main(int argc, char** argv)
{
	
	//doOdometry();
	//test_patch_normalize();
	//test_seqslam();
	//test_seqslam_result();
	//test_seqslam_on_match();
	//test_preprocess_seq();
	//test_extract_features_grid();
	//relative_no_scale_test();
	//dosingleMatchTest();
	//test_relative_init();
	//test_relative_pose_using_matches();
	//test_extract_features_grid_new();
	//test_relative_match_init();
	//test_relative_pose_using_matches_matching();
	//test_viz_installation();
	//doOdometryWithMatching();
	//trainandruntraining();
	//test_preprocess_seq();
	//run_relative_pose_on_pre_matched_image_indexes();
	run_final_stuff();

	//test_kslam();
	//test_kslam_fab();

	//test_kslam_lip6();
	
	return 1;
	//tets_training_writer();

	test_full_run_on_pretrained();
	for (int i = 391; i < 945; i++){
		test_read_one_file("C:/Matlab/savetest/", i);
	}

	//test_stereo_pnp_match_init();
	//test_stereo_pnp_relative_pose_using_matches_matching();
	seq_run_on_sequence("testkitty", 0, 4540);
	//test_trackbar();
	waitKey(0);
}
