
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <chrono>
#include "opencv2/core.hpp"
#include "opencv2/core/utility.hpp"
#include "opencv2/features2d.hpp"
#include "opencv2/xfeatures2d.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/calib3d.hpp"
#include "opencv2/video/tracking.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "Odometry.h"
#include "mylib.h"

using namespace cv;
using namespace cv::xfeatures2d;
using namespace std;
using namespace std::chrono;

static vector<Mat> positions;
static vector<Mat> rotations;
static vector<Mat> globalPositions;
static vector<Mat> globalPose;
static vector<Mat> seqImgs;
static vector<Mat> globalRotation;

int dosingleMatchTestod()
{
	char images[100];
	sprintf_s(images, "C:/Matlab/datasets/kitti/00/image_0/%06d.png", 100);
	Mat img1 = imread(images);

	sprintf_s(images, "C:/Matlab/datasets/kitti/00/image_1/%06d.png", 100);
	Mat img2 = imread(images);

	double focal = 718.8560;
	Point2d PP(607.1928, 185.2157);

	high_resolution_clock::time_point t1 = high_resolution_clock::now();

	vector<KeyPoint> keypoints1, keypoints2;
	Mat descriptor1, descriptor2;

	Ptr<ORB> detector = ORB::create(50000);
	lib_extract_better_features(detector, img1, keypoints1, 31, 64, 20, 3, 10);
	lib_extract_better_features(detector, img2, keypoints2, 31, 64, 20, 3, 10);

	detector->compute(img1, keypoints1, descriptor1);
	detector->compute(img2, keypoints2, descriptor2);

	vector<DMatch> matches;
	matchFeatures(keypoints1, descriptor1, keypoints2, descriptor2, matches);

	// filter out good matches
	vector<Point2f> good_points1;
	vector<Point2f> good_points2;
	for (int i = 0; i < matches.size(); i++)
	{
		Point2f pm1 = keypoints1[matches[i].queryIdx].pt;
		Point2f pm2 = keypoints2[matches[i].trainIdx].pt;
		good_points1.push_back(pm1);
		good_points2.push_back(pm2);
	}

	//Mat img_matches;
	//drawMatches(img1, keypoints1, img2, keypoints2, matches, img_matches);
	//imshow("Matches", img_matches);

	Mat mask;
	Mat CR, Ct;
	// calculate essential matrix with ransac and 5-point algorithm?
	/*lib_get_relative_pose_from_points(good_points2, good_points1, focal, PP, CR, Ct, mask);

	// filter inliers with mask
	vector<KeyPoint> inliers1;
	vector<KeyPoint> inliers2;
	int N = mask.rows;
	Mat pp1(2, N, CV_32FC1);
	Mat pp2(2, N, CV_32FC1);
	for (int i = 0; i < mask.rows; i++)
	{
		if (mask.at<char>(i, 0))
		{
			Point2f pm1 = good_points1[i];
			Point2f pm2 = good_points2[i];
			Mat mpm1(pm1);
			Mat mpm2(pm2);
			mpm1.copyTo(pp1.col(i));
			mpm2.copyTo(pp2.col(i));
			inliers1.push_back(cv::KeyPoint(pm1, 1));
			inliers2.push_back(cv::KeyPoint(pm2, 1));
		}
	}*/

	high_resolution_clock::time_point t2 = high_resolution_clock::now();
	auto duratation = duration_cast<milliseconds>(t2 - t1).count();
	cout << duratation << "ms\n";
	cout << Ct << endl;

	vector<KeyPoint> good_keypoints1, good_keypoints2;
	KeyPoint::convert(good_points1, good_keypoints1);
	KeyPoint::convert(good_points2, good_keypoints2);

	Mat img_keypoints1; Mat img_keypoints2;
	drawKeypoints(img1, good_keypoints1, img_keypoints1, Scalar::all(-1), DrawMatchesFlags::DEFAULT);
	drawKeypoints(img2, good_keypoints2, img_keypoints2, Scalar::all(-1), DrawMatchesFlags::DEFAULT);
	//drawKeypoints(img1, inliers1, img_keypoints1, Scalar::all(-1), DrawMatchesFlags::DEFAULT);
	//drawKeypoints(img2, inliers2, img_keypoints2, Scalar::all(-1), DrawMatchesFlags::DEFAULT);
	Mat combined = Mat(img_keypoints1.rows * 2, img_keypoints1.cols, img_keypoints1.type());
	lib_combine_two_images(img_keypoints1, img_keypoints2, combined);
	imshow("Keypoints", combined);
	return 1;
}

int doOdometry()
{
	// image for ploting path
	Mat traj = Mat::zeros(600, 600, CV_8UC3);
	namedWindow("Trajectory", WINDOW_AUTOSIZE);
	double focal = 718.8560;
	Point2d PP(607.1928, 185.2157);
	vector<Point3d> scores;
	lib_read_gps_data_kitti(scores, "C:/Matlab/datasets/kitti/data_odometry_poses/dataset/poses/00.txt");
	vector<KeyPoint> prevKPoints;
	vector<Point2f> prevPoints, cPoints;
	Mat prevDesc, cDesc;
	char images[100];
	sprintf(images, "C:/Matlab/datasets/kitti/00/image_0/%06d.png", 0);
	Mat previmg = imread(images, IMREAD_GRAYSCALE);
	//resize(previmg, previmg, Size(), 0.5, 0.5);
	//extractGoodFeatures(previmg, prevPoints, prevDesc);
	Ptr<ORB> detector = ORB::create(1500);
	detector->detect(previmg, prevKPoints);
	KeyPoint::convert(prevKPoints, prevPoints, vector<int>());

	ofstream myfile;
	myfile.open("C:/Matlab/mydata2.csv");

	Mat R = Mat::eye(3, 3, CV_64FC1);
	Mat t = Mat::zeros(3, 1, CV_64FC1);
	positions.push_back(t);
	rotations.push_back(R);
	for (int i = 1; i < 500; i++)
	{
		sprintf(images, "C:/Matlab/datasets/kitti/00/image_0/%06d.png", i);
		Mat cimg = imread(images, IMREAD_GRAYSCALE);
		//resize(cimg, cimg, Size(),0.5,0.5);
		//extractGoodFeatures(cimg, cPoints, cDesc);
		vector<uchar> status;
		lib_feature_tracking(previmg, cimg, prevPoints, cPoints, status);
		if (prevPoints.size() < 800)	{
			//cout << "Number of tracked features reduced to " << prevFeatures.size() << endl;
			//cout << "trigerring redection" << endl;
			detector->detect(previmg, prevKPoints);
			KeyPoint::convert(prevKPoints, prevPoints, vector<int>());
			lib_feature_tracking(previmg, cimg, prevPoints, cPoints, status);
		}
		Mat mask;
		Mat CR, Ct;
		// calculate essential matrix with ransac and 5-point algorithm?
		lib_get_relative_pose_from_points(prevPoints, cPoints, focal, PP, CR, Ct, mask);

		positions.push_back(Ct);
		rotations.push_back(CR);
		Point3d dist = scores[i] - scores[i - 1];
		double scale = norm(dist);
		printf("scale: %f\n", scale);
		t = t + scale * (R*Ct);
		globalPositions.push_back(t);
		R = CR * R;
		globalRotation.push_back(R);

		int x = int(t.at<double>(0)) + 300;
		int y = int(t.at<double>(2)) + 550;
		circle(traj, Point(x, y), 1, CV_RGB(255, 0, 0), 2);
		rectangle(traj, Point(10, 30), Point(550, 50), CV_RGB(0, 0, 0), CV_FILLED);
		char text[100];
		sprintf(text, "%.4f %.4f %.4f\n", t.at<double>(0), t.at<double>(1), t.at<double>(2));
		//myfile << text;
		//sprintf(text, "Coordinates: x = %.2f y = %01f z = %.2f  Points: %d", t.at<double>(0), t.at<double>(1), t.at<double>(2), prevPoints.size());
		printf("%s", text);
		putText(traj, text, Point(10, 50), FONT_HERSHEY_PLAIN, 1, Scalar::all(255), 1, 8);
		imshow("Trajectory", traj);

		previmg = cimg.clone();
		prevPoints = cPoints;
		waitKey(80);
	}
	myfile.close();
	return 1;
}

int matchFeatures(vector<KeyPoint>& keypoints1, Mat& descriptor1,
	vector<KeyPoint>& keypoints2, Mat& descriptor2, vector<DMatch>& matches)
{
	BFMatcher matcher(NORM_HAMMING,true);
	//FlannBasedMatcher matcher2(new flann::LshIndexParams(20, 10, 2));
	//matcher2.match(descriptor1,descriptor2,matches);
	matcher.match(descriptor1, descriptor2, matches);
	//matcher.knnMatch(descriptor1, descriptor2, matches);
	return 1;
}

int decomposeAndCheckE(Mat E, Mat& R1, Mat& R2, Mat& t)
{
	decomposeEssentialMat(E, R1, R2, t);

	if (determinant(R1) < -0.5f)
	{
		cout << "det(R1) == -1 flipping E" << endl;
		E = -E;
		decomposeEssentialMat(E, R1, R2, t);
	}

	if (determinant(R1) < 0.8 && determinant(R1) > -0.8)
	{
		cout << "R1 not coherent" << endl << E << endl << endl;
		return 0;
	}
	return 1;
}

int GetEssentialMatrixCode()
{
	Mat W = (Mat_<float>(3, 3) << 0, -1, 0, 1, 0, 0, 0, 0, 1);
	Mat Z = (Mat_<float>(3, 3) << 0, 1, 0, -1, 0, 0, 0, 0, 0);
	Mat U, S, Vt;
	//SVD::compute(E, S, U, Vt);
	//cout << "W" << endl << W << endl << endl;
	//cout << "U" << endl << U << endl << endl;
	//cout << "V" << endl << Vt << endl << endl;
	// U*diag(1,1,0)V^t = SVD(E); matlab version
	//if (determinant(U) < 0) U *= -1.;
	//if (determinant(Vt) < 0) Vt *= -1.;
	//Mat tt = U.col(2);
	//Mat svdR1 = U * W * Vt;
	//Mat svdR2 = U * W.t() * Vt;
	// E = SR
	//cout << "svdR1" << endl << svdR1 << endl << endl;
	//cout << "svdR2" << endl << svdR2 << endl << endl;
	//cout << "TT" << endl << tt << endl << endl;
	return 0;
}

int test3dPoints(Mat p3d, int N)
{
	int num_z = 0;
	for (int i = 0; i < N; i++)
	{
		Vec3f v3 = p3d.at<Vec3f>(i, 0);
		if (v3[2] > 0)
		{
			num_z++;
		}
	}
	if (num_z > N / 2)
	{
		return 1;
	}
	return 0;
}
int testTriangulization(Mat nhom3d, Mat P)
{
	Mat PPP1 = Mat::eye(4, 4, CV_32F);
	P.copyTo(PPP1.rowRange(0, 3).colRange(0, 4));
	Mat t3dP;

	int N = nhom3d.rows;
	perspectiveTransform(nhom3d, t3dP, PPP1);
	int num_z = 0;
	for (int i = 0; i < N; i++)
	{
		Vec3f v3 = t3dP.at<Vec3f>(i, 0);
		if (v3[2] > 0)
		{
			num_z++;
		}
	}
	if (num_z > N / 4)
	{
		return 1;
	}
	return 0;
}

int triangulateAndConvert(Mat P1, Mat P2, Mat inliers1, Mat inliers2, Mat& outt)
{
	int N = inliers1.rows;
	Mat pp4(4, N, CV_32F);
	triangulatePoints(P1, P2, inliers1, inliers2, pp4);

	pp4 = pp4.t();
	pp4 = pp4.reshape(4);
	Mat nhom3d;
	//cout << pp4.channels() << endl;
	convertPointsFromHomogeneous(pp4, outt);
	//cout << nhom3d.channels() << endl;
	return 1;
}

int GetCorrectRotT(Mat inliers1, Mat inliers2,
	Mat R1, Mat R2, Mat t1, Mat t2, Mat& correctR, Mat& correctt)
{
	int N = inliers1.cols;
	Mat pp4(4, N, CV_32F);
	Mat Pc1, Pc2, Pc3, Pc4;
	hconcat(R1, t1, Pc1);
	hconcat(R2, t1, Pc2);
	hconcat(R1, t2, Pc3);
	hconcat(R2, t2, Pc4);

	Mat Pp = Mat::eye(3, 4, CV_32F);
	Mat points3d;
	Mat points3d2;
	triangulateAndConvert(Pp, Pc1, inliers1, inliers2, points3d);
	triangulateAndConvert(Pc1, Pp, inliers1, inliers2, points3d2);
	if (!testTriangulization(points3d2, Pc1) || !testTriangulization(points3d, Pp))
	{
		triangulateAndConvert(Pp, Pc2, inliers1, inliers2, points3d);
		triangulateAndConvert(Pc2, Pp, inliers1, inliers2, points3d2);
		if (!testTriangulization(points3d2, Pc2) || !testTriangulization(points3d, Pp))
		{
			triangulateAndConvert(Pp, Pc3, inliers1, inliers2, points3d);
			triangulateAndConvert(Pc3, Pp, inliers1, inliers2, points3d2);
			if (!testTriangulization(points3d2, Pc3) || !testTriangulization(points3d, Pp))
			{
				triangulateAndConvert(Pp, Pc4, inliers1, inliers2, points3d);
				triangulateAndConvert(Pc4, Pp, inliers1, inliers2, points3d2);
				if (!testTriangulization(points3d2, Pc4) || !testTriangulization(points3d, Pp))
				{

				}
				else
				{
					R2.copyTo(correctR);
					t2.copyTo(correctt);
				}
			}
			else
			{
				R1.copyTo(correctR);
				t2.copyTo(correctt);
			}
		}
		else
		{
			R2.copyTo(correctR);
			t1.copyTo(correctt);
		}
	}
	else
	{
		R1.copyTo(correctR);
		t1.copyTo(correctt);
	}

	return 1;
}
