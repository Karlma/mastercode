#ifndef KSLAM_H
#define KSLAM_H
#include <stdio.h>
#include <iostream>
#include <fstream>
#include "opencv2/core.hpp"
#include "opencv2/core/utility.hpp"
struct singleMatch{
	int a;
	int b;
	bool fail;
	singleMatch(int aa, int bb, bool ffail)
	{
		a = aa;
		b = bb;
		fail = ffail;
	}
};

class KSLAM
{
public:
	char path[100];
	std::ofstream myfile;

	int numer_of_images = 0;
	int skip_every_frame = 0;
	int xoffset = 280;// 280;
	int yoffset = 580;//580;
	int max_frames_to_fail = 3;
	int min_frames_for_seq = 15;

	int patch_norm_rows = 0;
	int patch_norm_cols = 0;
	int patch_norm_grid_size = 0;
	
	int look_back = 300;

	double uu = 0.65;

	int current_seq_streak = 0;
	int current_seq_fail = 0;
	int current_seq_without_fail = 0;
	bool seq_started = false;
	int prev_best_match_idx = 0;
	int image_counter = 0;
	int cirrent_img_after_filter = 0;
	double currval = 0;
	int prev_find = 0;

	float part_of_seq_threshold = 40; // 40

	cv::Mat system = cv::Mat::zeros(6000, 6000, CV_64F);

	std::vector<cv::Mat> prev_imgs;
	std::vector<int> prev_imgs_idx;
	std::vector<int> *best_match_global = new std::vector<int>(5000);
	std::vector<singleMatch> *correct_matches = new std::vector<singleMatch>;
	std::vector<singleMatch> *current_seq_matches = new std::vector<singleMatch>;

	int KSLAM::show_system_image();
	int KSLAM::pre_train_step(cv::Mat currimg);
	int KSLAM::add_normalized_image(cv::Mat currimg);
	int KSLAM::find_best_from_train(cv::Mat currimg);
	int KSLAM::step_custom(cv::Mat currimg);
	int KSLAM::step_new(cv::Mat currimg);
	int KSLAM::step_seqslam(cv::Mat currimg);
	int KSLAM::step_seq(cv::Mat currimg);
	int KSLAM::generate_diff_mat(cv::Mat img);
	int KSLAM::generate_vect(cv::Mat img, cv::Mat& vect);
	KSLAM() {  }
};
#endif