#include <stdio.h>
#include <iostream>
#include <fstream>
#include "opencv2/core.hpp"
#include "opencv2/core/utility.hpp"
#include "opencv2/features2d.hpp"
#include "opencv2/xfeatures2d.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/calib3d.hpp"
#include "opencv2/video/tracking.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "Odometry.h"
#include "SeqSLAM.h"
#include "Tests.h"
#include "FullSystem1.h"
#include "mylib.h"

using namespace cv;
using namespace std;

static vector<int> va;
static vector<int> vb;
static Ptr<ORB> detector = ORB::create(3000);
static vector<Point3d> scores;
static vector<Mat> camMats;
static ofstream myfile;
static ofstream myfile2;
static Mat traj = Mat::zeros(800, 800, CV_8UC3);
static int xoffset = 200;// 280;
static int yoffset = 565;//580;
static int num_matches;

static char imagepath[100];

int test_relative_init()
{
	lib_read_matches(va, vb, "C:/Matlab/matches_00_25100.csv");
	num_matches = (int)va.size();
	lib_read_gps_data_kitti(scores, "C:/Matlab/datasets/kitti/data_odometry_poses/dataset/poses/00.txt");
	lib_read_kitti_poses(camMats, "C:/Matlab/datasets/kitti/data_odometry_poses/dataset/poses/00.txt");
	myfile.open("C:/Matlab/relativeposes_on_matches.csv");
	myfile2.open("C:/Matlab/relativeposes_on_matches2.csv");
	sprintf_s(imagepath, "C:/Matlab/datasets/kitti/00/image_0");
	return 1;
}

int test_relative_pose_using_matches()
{
	char path[100];
	sprintf_s(path, "%s/%06d.png", imagepath, 1);
	Mat imga = imread(path, IMREAD_GRAYSCALE);
	Mat combined = Mat(imga.rows * 2, imga.cols, imga.type());
	for (int i = 0; i < num_matches; i++)
	{
		int now = va[i];
		int match = vb[i];
		sprintf_s(path, "%s/%06d.png",imagepath, now);
		imga = imread(path, IMREAD_GRAYSCALE);
		sprintf_s(path, "%s/%06d.png",imagepath, match);
		Mat imgb = imread(path, IMREAD_GRAYSCALE);
		lib_combine_two_images(imga, imgb, combined);
		imshow("Match", combined);

		Mat t, R;
		int inliers = lib_get_relative_pose(detector, imga, imgb, t, R);

		cout << "Inliers: " << inliers << endl;
		Mat PP = camMats[match];
		Mat RK = PP.rowRange(0, 3).colRange(0, 3);
		Point2d tt2d(scores[match].x, scores[match].z);
		Point2d now2d(scores[now].x, scores[now].z);
		double scale = norm(tt2d - now2d);
		Point3d tnow = scores[now];
		cout << "t from recover" << t << endl;
		//pt = pt * scale;
		//Mat t2 = RK*(t * scale);
		//for stereo
		Mat t2 = RK*(t * scale);
		Point2d pt2d(t2.at<double>(0), t2.at<double>(2));
		cout << "tafter" << pt2d << endl;
		cout << "fasit" << now2d - tt2d << endl;
		double scale2 = norm(now2d - (tt2d + pt2d));

		Point3d pt(t.at<double>(0), t.at<double>(1), t.at<double>(2));

		Point3d tthen = scores[match] + pt;
		Point3d dist2 = tnow - tthen;
		char text[100];
		sprintf_s(text, "%.4f,%.4f,%.4f,%d,%d\n", t.at<double>(0), t.at<double>(1), t.at<double>(2), match, i);
		myfile << text;
		sprintf_s(text, "%.4f,%.4f,%.4f,%d,%d\n", t2.at<double>(0), t2.at<double>(1), t2.at<double>(2), match, i);
		myfile2 << text;
		//double scale2 = norm(dist2);
		printf("%d -> %d s: %f, s2:%f\n", now, match, scale, scale2);
		circle(traj, Point(xoffset + tnow.x, yoffset - tnow.z) * 2, 0.5, CV_RGB(255, 255, 0), -1);
		circle(traj, Point(xoffset + scores[match].x, yoffset - scores[match].z) * 3, 0.5, CV_RGB(0, 255, 255), -1);
		circle(traj, Point(xoffset + tthen.x, yoffset - tthen.z) * 2, 0.5, CV_RGB(0, 0, 255), -1);
		imshow("Trajectory", traj);
		waitKey(0);
	}
	return 1;
}

