#ifndef TRAINDANDRUN_H
#define TRAINDANDRUN_H

#include "opencv2/core.hpp"
#include "opencv2/core/utility.hpp"

// structure for storing a stereo match descriptor and triangulization
class PreProImg
{
public:
	int imgidx;
	int width;
	int height;
	cv::Mat pose;
	std::vector<cv::KeyPoint> kp1;
	std::vector<cv::KeyPoint> kp2;
	cv::Mat points3d;
	cv::Mat desc1;
	cv::Mat desc2;
	void write(cv::FileStorage& fs) const                        //Write serialization for this class
	{
		fs << "{" << "idx" << imgidx <<
			"pose" << pose <<
			"kp1" << kp1 <<
			"kp2" << kp2 <<
			"desc1" << desc1 <<
			"desc2" << desc2 <<
			"points3d" << points3d << "}";
	}

	void read(const cv::FileNode& node)                          //Read serialization for this class
	{
		node["idx"] >> imgidx;
		node["pose"] >> pose;
		node["kp1"] >> kp1;
		node["kp2"] >> kp2;
		node["desc1"] >> desc1;
		node["desc2"] >> desc2;
		node["points3d"] >> points3d;
	}
	PreProImg() {  }
};

PreProImg read_one_file(char *path);
int triangulate_and_save(cv::Mat left_img, cv::Mat right_img, cv::Mat left_pose);
int read_training_data();
int init_runner(char *trainsavepath);
int do_single_step(cv::Mat img);

#endif // DEBUG