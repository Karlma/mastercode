
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <time.h>
#include <ctime>
#include <chrono>
#include "opencv2/core.hpp"
#include "opencv2/core/utility.hpp"
#include "opencv2/features2d.hpp"
#include "opencv2/xfeatures2d.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/calib3d.hpp"
#include "opencv2/video/tracking.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/viz.hpp"
//#include "opencv2/sfm/projection.hpp"
#include "Odometry.h"
#include "mylib.h"
#include "SeqSLAM.h"
#include "KSLAM.h"
#include "Tests.h"
#include "FullSystem1.h"

using namespace cv;
using namespace std;
using namespace std::chrono;


int dosingleMatchTest()
{
	char images[100];
	sprintf_s(images, "C:/Matlab/datasets/kitti/00/image_0/%06d.png", 100);
	Mat img1 = imread(images);

	sprintf_s(images, "C:/Matlab/datasets/kitti/00/image_1/%06d.png", 100);
	Mat img2 = imread(images);

	double focal = 718.8560;
	Point2d PP(607.1928, 185.2157);

	high_resolution_clock::time_point t1 = high_resolution_clock::now();

	vector<KeyPoint> keypoints1, keypoints2;
	Mat descriptor1, descriptor2;

	Ptr<ORB> detector = ORB::create(50000);
	lib_extract_better_features(detector, img1, keypoints1, 31, 64, 20, 3, 10);
	lib_extract_better_features(detector, img2, keypoints2, 31, 64, 20, 3, 10);
	Mat mask;
	Mat CR, Ct;

//	lib_get_relative_pose_using_matcher(detector,img1,img2,Ct,CR);
	//Mat img_matches;
	//drawMatches(img1, keypoints1, img2, keypoints2, matches, img_matches);
	//imshow("Matches", img_matches);

	// calculate essential matrix with ransac and 5-point algorithm?
	/*lib_get_relative_pose_from_points(good_points2, good_points1, focal, PP, CR, Ct, mask);

	// filter inliers with mask
	vector<KeyPoint> inliers1;
	vector<KeyPoint> inliers2;
	int N = mask.rows;
	Mat pp1(2, N, CV_32FC1);
	Mat pp2(2, N, CV_32FC1);
	for (int i = 0; i < mask.rows; i++)
	{
	if (mask.at<char>(i, 0))
	{
	Point2f pm1 = good_points1[i];
	Point2f pm2 = good_points2[i];
	Mat mpm1(pm1);
	Mat mpm2(pm2);
	mpm1.copyTo(pp1.col(i));
	mpm2.copyTo(pp2.col(i));
	inliers1.push_back(cv::KeyPoint(pm1, 1));
	inliers2.push_back(cv::KeyPoint(pm2, 1));
	}
	}*/

	high_resolution_clock::time_point t2 = high_resolution_clock::now();
	auto duratation = duration_cast<milliseconds>(t2 - t1).count();
	cout << duratation << "ms\n";
	cout << Ct << endl;

	//vector<KeyPoint> good_keypoints1, good_keypoints2;
	//KeyPoint::convert(good_points1, good_keypoints1);
	//KeyPoint::convert(good_points2, good_keypoints2);

	/*Mat img_keypoints1; Mat img_keypoints2;
	drawKeypoints(img1, good_keypoints1, img_keypoints1, Scalar::all(-1), DrawMatchesFlags::DEFAULT);
	drawKeypoints(img2, good_keypoints2, img_keypoints2, Scalar::all(-1), DrawMatchesFlags::DEFAULT);
	//drawKeypoints(img1, inliers1, img_keypoints1, Scalar::all(-1), DrawMatchesFlags::DEFAULT);
	//drawKeypoints(img2, inliers2, img_keypoints2, Scalar::all(-1), DrawMatchesFlags::DEFAULT);
	Mat combined = Mat(img_keypoints1.rows * 2, img_keypoints1.cols, img_keypoints1.type());
	lib_combine_two_images(img_keypoints1, img_keypoints2, combined);
	imshow("Keypoints", combined);*/
	return 1;
}

int test_extract_features_grid()
{
	//1241*347
	// 17*73
	// 64 * 18; cells

	Ptr<ORB> detector = ORB::create(3000);
	char path[100];
	sprintf_s(path, "C:/Matlab/datasets/kitti/00/image_0/%06d.png", 2);
	Mat img1 = imread(path, IMREAD_GRAYSCALE);
	vector<KeyPoint> kpt;
	lib_extract_better_features(detector, img1, kpt, 31, 64, 18, 3, 5);
	drawKeypoints(img1, kpt, img1);
	imshow("gridfeatures", img1);
	return 1;
}

int test_extract_features_grid_new()
{
	//1241*347
	// 17*73
	// 64 * 18; cells

	Ptr<ORB> detector = ORB::create(3000);
	char path[100];
	sprintf_s(path, "C:/Matlab/datasets/kitti/00/image_0/%06d.png", 2);
	Mat img1 = imread(path, IMREAD_GRAYSCALE);
	vector<KeyPoint> kpt;
	high_resolution_clock::time_point t1 = high_resolution_clock::now();
	int keypoints = lib_extract_better_features(detector, img1, kpt, 31, 16, 6, 5, 2);
	high_resolution_clock::time_point t2 = high_resolution_clock::now();
	auto duratation = duration_cast<milliseconds>(t2 - t1).count();
	cout << "Time: " << duratation << "ms\n";
	cout << "keypoints: " << keypoints << "\n";
	drawKeypoints(img1, kpt, img1);
	imshow("gridfeatures", img1);
	return 1;
}

int test_kslam_fab()
{
	long filetime = std::time(0);

	int counter = 0;
	for (double sweep = 0.00; sweep < 0.30; sweep += 0.005) {

		KSLAM *kslam = new KSLAM();
		kslam->part_of_seq_threshold = counter;
		counter++;
		kslam->uu = sweep;
		static Ptr<ORB> detector = ORB::create(3000);
		Mat traj = Mat::zeros(800, 800, CV_8UC3);
		int xoffset = 280;// 280;
		int yoffset = 580;//580;
		char path[100];
		vector<Mat> camMats;
		int numMatches = 0;
		sprintf_s(path, "C:/Matlab/datasets/kitti/00_preprocess/image_0/%06d.png", 0);
		Mat imgf = imread(path, IMREAD_GRAYSCALE);
		Mat combined = Mat(imgf.rows * 2, imgf.cols, imgf.type());

		char myfile_path[200];
		sprintf_s(myfile_path, "C:/Matlab/ksalm/test2/matches_lip6_%d.csv", (int)kslam->part_of_seq_threshold);
		ofstream myfile;
		myfile.open(myfile_path);


		VideoCapture cap("C:/Matlab/datasets/openFABMAPsample/stlucia_train.avi"); // open the default camera
		if (!cap.isOpened())  // check if we succeeded
			return -1;
		for (;;)
		{
			Mat img1;
			cap >> img1; // get a new frame from camera
			if (img1.cols == 0) break;
			cvtColor(img1, img1, COLOR_BGR2GRAY);
			//imshow("curimg", img1);
			//waitKey(10);
			kslam->pre_train_step(img1);
		}

		VideoCapture cap2("C:/Matlab/datasets/openFABMAPsample/stlucia_test.avi"); // open the default camera
		if (!cap2.isOpened())  // check if we succeeded
			return -1;

		int i = 0;
		for (;;)
		{
			i++;
			Mat img1;
			cap2 >> img1; // get a new frame from camera
			if (img1.cols == 0) break;
			cvtColor(img1, img1, COLOR_BGR2GRAY);
			imshow("curimg", img1);
			int posno2 = ((i) / 2);
			Point3f tnow(0, 0, posno2);
			circle(traj, Point(xoffset + tnow.x, yoffset - tnow.z), 1, CV_RGB(0, 0, 255), -1);
			imshow("traj", traj);

			vector<singleMatch> *matches = new vector<singleMatch>();

			int ret = kslam->step_new(img1);

			if (ret > 0){
				circle(traj, Point(xoffset + tnow.x, yoffset - tnow.z), 1, CV_RGB(255, 255, 255), -1);
			}
			//int ret = kslam->step_seq(img1);
			if (waitKey(1) == 's')
			{
				kslam->show_system_image();
				waitKey(0);
			}
			if (kslam->correct_matches->size() > 0)
			{
				numMatches = kslam->correct_matches->size();
				for (int j = 0; j < numMatches; j++)
				{
					singleMatch *sm = &(kslam->correct_matches->at(j));
					myfile << sm->a << "," << sm->b << endl;
					Point3f tnow2(0, 0, sm->a / 2);
					circle(traj, Point(xoffset + tnow2.x, yoffset - tnow2.z), 1, CV_RGB(0, 255, 255), -1);
					Point3f tnow3(0, 0, sm->b / 2);
					circle(traj, Point(xoffset + tnow3.x, yoffset - tnow3.z), 1, CV_RGB(255, 0, 255), -1);
					//sprintf_s(path, "C:/Matlab/datasets/Lip6OutdoorDataSet/Images_png/outdoor_kennedylong_%06d.png", sm->b);
					//Mat img2 = imread(path, IMREAD_GRAYSCALE);
					//imshow("previmg", img2);
					waitKey(1);
				}
				kslam->correct_matches->clear();
			}
			//waitKey(1);
		}
		myfile.flush();
		myfile.close();
	}
	return 1;
}

int test_kslam_lip6()
{
	long filetime = std::time(0);

	int counter = 0;
	for (double sweep = 0.00; sweep < 0.30; sweep += 0.005) {

		KSLAM *kslam = new KSLAM();
		kslam->part_of_seq_threshold = counter;
		counter++;
		kslam->uu = sweep;
		static Ptr<ORB> detector = ORB::create(3000);
		Mat traj = Mat::zeros(800, 800, CV_8UC3);
		int xoffset = 280;// 280;
		int yoffset = 580;//580;
		char path[100];
		vector<Mat> camMats;
		int numMatches = 0;
		sprintf_s(path, "C:/Matlab/datasets/kitti/00_preprocess/image_0/%06d.png", 0);
		Mat imgf = imread(path, IMREAD_GRAYSCALE);
		Mat combined = Mat(imgf.rows * 2, imgf.cols, imgf.type());

		char myfile_path[200];
		sprintf_s(myfile_path, "C:/Matlab/ksalm/test2/matches_lip6_%d.csv",(int)kslam->part_of_seq_threshold);
		ofstream myfile;
		myfile.open(myfile_path);

		for (int i = 0; i < 1062; i += 1)
		{
			sprintf_s(path, "C:/Matlab/datasets/Lip6OutdoorDataSet/Images_png/outdoor_kennedylong_%06d.png", i);
			Mat img1 = imread(path, IMREAD_GRAYSCALE);
			printf("%d, %d\n", sweep, i);
			imshow("curimg", img1);
			int posno2 = ((i) / 2);
			Point3f tnow(0, 0, posno2);
			circle(traj, Point(xoffset + tnow.x, yoffset - tnow.z), 1, CV_RGB(0, 0, 255), -1);
			imshow("traj", traj);

			vector<singleMatch> *matches = new vector<singleMatch>();

			int ret = kslam->step_new(img1);

			if (ret > 0){
				circle(traj, Point(xoffset + tnow.x, yoffset - tnow.z), 1, CV_RGB(255, 255, 255), -1);
			}
			//int ret = kslam->step_seq(img1);
			if (waitKey(1) == 's')
			{
				kslam->show_system_image();
				waitKey(0);
			}
			if (kslam->correct_matches->size() > 0)
			{
				numMatches = kslam->correct_matches->size();
				for (int j = 0; j < numMatches; j++)
				{
					singleMatch *sm = &(kslam->correct_matches->at(j));
					myfile << sm->a << "," << sm->b << endl;
					Point3f tnow2(0,0,sm->a/2);
					circle(traj, Point(xoffset + tnow2.x, yoffset - tnow2.z), 1, CV_RGB(0, 255, 255), -1);
					Point3f tnow3(0,0,sm->b/2);
					circle(traj, Point(xoffset + tnow3.x, yoffset - tnow3.z), 1, CV_RGB(255, 0, 255), -1);
					sprintf_s(path, "C:/Matlab/datasets/Lip6OutdoorDataSet/Images_png/outdoor_kennedylong_%06d.png", sm->b);
					Mat img2 = imread(path, IMREAD_GRAYSCALE);
					imshow("previmg", img2);
					waitKey(1);
				}
				kslam->correct_matches->clear();
			}
			//waitKey(1);
		}
		myfile.flush();
		myfile.close();
	}
	return 1;
}

int test_kslam()
{
	long filetime = std::time(0);

	for (int seq = 5; seq <= 45; seq += 10){
		//for (double sweep = 0.28; sweep < 0.52; sweep += 0.04){
			for (double sweep = 0.5; sweep < 0.65; sweep += 0.01){
			//for (double sweep = 34; sweep < 50; sweep += 1){

			KSLAM *kslam = new KSLAM();
			kslam->part_of_seq_threshold = (float)sweep;
			kslam->uu = sweep;
			kslam->min_frames_for_seq = seq;
			Mat traj = Mat::zeros(800, 800, CV_8UC3);
			int xoffset = 280;// 280;
			int yoffset = 580;//580;
			char path[100];
			vector<Mat> camMats;
			int sequence = 0;
			char campath[400];
			sprintf(campath, "C:/Matlab/datasets/kitti/data_odometry_poses/dataset/poses/%02d.txt", sequence);
			lib_read_kitti_poses(camMats, campath);
			int numMatches = 0;

			char myfile_path[200];
			sprintf_s(myfile_path, "C:/Matlab/kslamspyr%02d/matches_%d_%d_%d.csv", sequence, kslam->min_frames_for_seq, (int)(kslam->part_of_seq_threshold * 1000), filetime);
			ofstream myfile;
			myfile.open(myfile_path);

			for (int i = 0; i < camMats.size(); i += 1)
			{
				sprintf_s(path, "C:/Matlab/datasets/kitti/%02d_preprocess/image_0/%06d.png", sequence, i);
				Mat img1 = imread(path, IMREAD_GRAYSCALE);
				printf("%d, %d\n", sweep * 100, i);
				//imshow("curimg", img1);
				Point3f tnow(camMats[i].at<double>(0, 3), camMats[i].at<double>(1, 3), camMats[i].at<double>(2, 3));
				circle(traj, Point(xoffset + tnow.x, yoffset - tnow.z), 1, CV_RGB(0, 0, 255), -1);
				//imshow("traj",traj);

				vector<singleMatch> *matches = new vector<singleMatch>();
				int ret = kslam->step_seqslam(img1);
				if (ret > 0){
					circle(traj, Point(xoffset + tnow.x, yoffset - tnow.z), 1, CV_RGB(255, 255, 255), -1);
				}
				//int ret = kslam->step_seq(img1);
				//if (waitKey(1) == 's')
				//{
				//	kslam->show_system_image();
				//	waitKey(0);
				//}
				if (kslam->correct_matches->size() > 0)
				{
					numMatches = kslam->correct_matches->size();
					for (int j = 0; j < numMatches; j++)
					{
						singleMatch *sm = &(kslam->correct_matches->at(j));
						myfile << sm->a << "," << sm->b << endl;
						Point3f tnow2(camMats[sm->a].at<double>(0, 3), camMats[sm->a].at<double>(1, 3), camMats[sm->a].at<double>(2, 3));
						circle(traj, Point(xoffset + tnow2.x, yoffset - tnow2.z), 1, CV_RGB(0, 255, 255), -1);
						Point3f tnow3(camMats[sm->b].at<double>(0, 3), camMats[sm->b].at<double>(1, 3), camMats[sm->b].at<double>(2, 3));
						circle(traj, Point(xoffset + tnow3.x, yoffset - tnow3.z), 1, CV_RGB(255, 0, 255), -1);
					}
					kslam->correct_matches->clear();
				}
				//waitKey(1);
			}
			myfile.flush();
			myfile.close();
		}
	}
	return 1;
}


int test_patch_normalize()
{
	char path[100];
	sprintf_s(path, "C:/Matlab/datasets/kitti/00/image_0/%06d.png", 0);
	Mat img1 = imread(path, IMREAD_GRAYSCALE);
	resize(img1, img1, Size(32, 16));
	lib_patch_normalize(img1);
	sprintf_s(path, "C:/Matlab/datasets/kitti/00/image_0/%06d.png", 1);
	Mat img2 = imread(path, IMREAD_GRAYSCALE);
	resize(img2, img2, Size(32, 16));
	lib_patch_normalize(img2);
	
	Mat diff;
	absdiff(img1, img1, diff);
	double score = sum(diff)[0];

	waitKey(0);
	return 1;
}
int test_seqslam_result()
{
	char path[100];
	sprintf_s(path, "C:/Matlab/Gray_Image.png");
	Mat img1 = imread(path, IMREAD_GRAYSCALE);
	Mat sub = img1.rowRange(360, 960).colRange(3300, 3900);
	double min;
	double max;
	cv::minMaxIdx(img1, &min, &max);
	imshow("compsystem", sub);
	waitKey(0);
	return 1;
}

int test_viz_installation()
{
	/// Create a window
	viz::Viz3d myWindow("Coordinate Frame");

	/// Add coordinate axes
	myWindow.showWidget("Coordinate Widget", viz::WCoordinateSystem());

	/// Add line to represent (1,1,1) axis
	viz::WLine axis(Point3f(0, 0, 0), Point3f(1.0f, 1.0f, 1.0f));
	axis.setRenderingProperty(viz::LINE_WIDTH, 4.0);
	myWindow.showWidget("Line Widget", axis);

	/// Let's assume camera has the following properties
	Vec3d cam_pos(3.0f, 3.0f, 3.0f), cam_focal_point(0, 0, 0), cam_y_dir(0.0f, 1.0f, 0.0f);
	/// We can get the pose of the cam using makeCameraPose
	Affine3f cam_pose = viz::makeCameraPose(cam_pos, cam_focal_point, cam_y_dir);
	viz::WCameraPosition cpw(0.5); // Coordinate axes
	viz::WCameraPosition cpw_frustum(Vec2f(0.889484, 0.523599)); // Camera frustum
	myWindow.showWidget("CPW", cpw, cam_pose);
	myWindow.showWidget("CPW_FRUSTUM", cpw_frustum, cam_pose);
	while (!myWindow.wasStopped())
	{
		myWindow.spinOnce(1, true);
	}

	return 0;
}

static int threshold_value = 200;
static int thresh_type = 4;
static int const max_value = 255;
static int const max_type = 4;
static int const max_B_value = 255;
static char* win_name = "threshtest";
static char* tracbar_type_name = "type";
static char* tracbar_value_name = "Value";
static Mat src, dst;
static void threshold_test(int, void*)
{
	threshold(src, dst, threshold_value, max_B_value, thresh_type);
	imshow(win_name, dst);
}
int test_trackbar()
{
	char path[100];
	int i = 236;
	sprintf_s(path, "C:/Matlab/datasets/kitti/00/image_0/%06d.png", i);
	src = imread(path, IMREAD_GRAYSCALE);
	namedWindow(win_name, CV_WINDOW_AUTOSIZE);

	createTrackbar(tracbar_type_name, win_name, &thresh_type, max_type, threshold_test);
	createTrackbar(tracbar_value_name, win_name, &threshold_value, max_value, threshold_test);

	while (true){
		int c;
		c = waitKey(3);
		if ((char) c == 'q'){
			break;
		}
		else if (c == ' ')
		{
			i++;
			sprintf_s(path, "C:/Matlab/datasets/kitti/00/image_0/%06d.png", i);
			src = imread(path, IMREAD_GRAYSCALE);
			threshold_test(0,0);
		}
	}
	return 0;
}


int test_seqslam_on_match()
{
	vector<Point3d> scores;
	lib_read_gps_data_kitti(scores, "C:/Matlab/datasets/kitti/data_odometry_poses/dataset/poses/00.txt");
	Mat traj = Mat::zeros(600, 600, CV_8UC3);
	int Nimg = 4540;
	//int Nimg = 4000;
	Mat system = Mat::zeros(Nimg, Nimg, CV_64F);
	ofstream myfile2;
	myfile2.open("C:/Matlab/matches_00_25100.csv");
	vector<Mat> prevImgs;
	int xoffset = 280;
	int yoffset = 580;
	for (int i = 0; i < Nimg; i++)
	{
		char path[100];
		char idxes[100];
		sprintf(path, "C:/Matlab/datasets/kitti/00_preprocess/image_0/%06d.png", i);
		Mat img1 = imread(path, IMREAD_GRAYSCALE);
		//imshow("preprocessed", img1);
		Mat pimg;
		int match = 0;
		if (i > 200){
			preprocessImage(img1, pimg);
			match = getMatch(pimg, prevImgs, system, i,100);
			if (match > 0)
			{
				//sprintf(path, "C:/Matlab/datasets/kitti/00/image_0/%06d.png", i);
				//Mat imga = imread(path, IMREAD_GRAYSCALE);
				//sprintf(path, "C:/Matlab/datasets/kitti/00/image_0/%06d.png", match);
				//Mat imgb = imread(path, IMREAD_GRAYSCALE);
				//Mat combined = Mat(imga.rows * 2, imga.cols,imga.type());
				//imga.copyTo(combined.colRange(0, imga.cols).rowRange(0, imga.rows));
				//imgb.copyTo(combined.colRange(0, imga.cols).rowRange(imga.rows, imga.rows* 2));
				//imshow("Match", combined);
				sprintf(idxes, "%d,%d\n", i, match);
				myfile2 << idxes;
				Point3f tnow = scores[i];
				Point3f tthen = scores[match];
				//rectangle(traj, Point(10, 30), Point(550, 50), CV_RGB(0, 0, 0), CV_FILLED);
				//char text[100];
				//sprintf(text, "Coordinates: x = %.2f y = %01f z = %.2f  P: %d", tnow.x, tnow.y, tnow.z, i);
				//putText(traj, text, Point(10, 50), FONT_HERSHEY_PLAIN, 1, Scalar::all(255), 1, 8);
				line(traj, Point(xoffset + tthen.x, yoffset - tthen.z), Point(xoffset + tnow.x, yoffset - tnow.z), CV_RGB(0, 0, 255), 1, 8, 0);
			}
		}
		Point3f tnow = scores[i];
		if (match)
		{
			circle(traj, Point(xoffset + tnow.x, yoffset - tnow.z), 1, CV_RGB(0, 255, 0), -1);
		}
		else{
			circle(traj, Point(xoffset + tnow.x, yoffset - tnow.z), 1, CV_RGB(255, 0, 0), -1);
		}
		prevImgs.push_back(img1);
		//prevImgs.push_back(pimg);
		printf("%d\n", i);
		//waitKey(100);
	}
	imshow("Trajectory", traj);
	waitKey(0);
	double min;
	double max;
	minMaxIdx(system, &min, &max);
	Mat adjMap;
	convertScaleAbs(system, adjMap, 255 / max);
	//imwrite("C:/Matlab/Gray_Image.png", adjMap);
	Mat sub = adjMap.rowRange(360, 960).colRange(3300, 3900);
	imshow("compsystem", sub);
	//imwrite("C:/Matlab/Gray_Image.png", adjMap);
	//Mat sub2 = system.rowRange(360, 960).colRange(3300, 3900);
	//FileStorage file("C:/Matlab/Gray_Image.yml", FileStorage::WRITE);
	//file << "system" << sub2;
	//file.release();
	waitKey(0);
	return 1;
}

int test_preprocess_seq()
{
	int Nimg = 4541;
	for (int i = 0; i < Nimg; i++)
	{
		char path[100];
		sprintf(path, "C:/Matlab/datasets/kitti/00/image_0/%06d.png", i);
		Mat img1 = imread(path, IMREAD_GRAYSCALE);
		Mat pimg;
		preprocessImage(img1, pimg);
		sprintf(path, "C:/Matlab/datasets/kitti/00_preprocess/image_0/%06d.png", i);
		if (!imwrite(path, pimg))
		{
			printf("%s\n",path);
		}
		printf("%d\n", i);
	}
	waitKey(0);
	return 1;
}

int test_seqslam()
{
	//int Nimg = 4540;
	int Nimg = 4100;
	Mat system = Mat::zeros(Nimg, Nimg, CV_64F);
	vector<Mat> prevImgs;
	for (int i = 0; i < Nimg; i++)
	{
		char path[100];
		sprintf(path, "C:/Matlab/datasets/kitti/00/image_0/%06d.png", i);
		Mat img1 = imread(path, IMREAD_GRAYSCALE);
		Mat pimg;
		preprocessImage(img1, pimg);
		compSystem(pimg, prevImgs, system, i);
		prevImgs.push_back(pimg);
		printf("%d\n", i);
	}
	double min;
	double max;
	cv::minMaxIdx(system, &min, &max);
	cv::Mat adjMap;
	cv::convertScaleAbs(system, adjMap, 255 / max);
	imwrite("C:/Matlab/Gray_Image.png", adjMap);
	imshow("compsystem", adjMap);
	waitKey(0);
	return 1;
}

//triangluta a stereo pair using simple feature matching.
int test_triangulate_linear(Mat cameraleft, Mat cameraright, Mat imgleft, Mat imgright, 
						    vector<KeyPoint> & kpleft, vector<KeyPoint> & kpright,
							Mat & descleft, Mat & descright,
							grid_params params,
							vector<Point3f>& points3d,
							Mat & colors,
							int methode)
{
	vector<KeyPoint> kp1;
	vector<KeyPoint> kp2;
	// some parameters to take into the table (performance vs speed)
	lib_extract_grid_features_params(imgleft, kp1, params);
	//lib_extract_grid_features(detector, imgleft, kp1, 64, 19, 3, 5);
	lib_extract_grid_features_params(imgright, kp2, params);
	//lib_extract_grid_features(detector, imgright, kp2, 64, 19, 3, 5);

	Mat desc1, desc2;
	params.detector->compute(imgleft, kp1, desc1);
	params.detector->compute(imgright, kp2, desc2);
	//find possigle matches
	vector<DMatch> matches1;
	lib_match_descriptors(desc1, desc2, matches1);
	lib_filter_matches(kp1, kp2, matches1);

	Mat desc1filt, desc2filt;
	for (int i = 0; i < matches1.size(); i++)
	{
		desc1filt.push_back(desc1.row(matches1[i].queryIdx));
		desc2filt.push_back(desc2.row(matches1[i].trainIdx));
	}

	Mat K, rot, trans;
	decomposeProjectionMatrix(cameraleft, K, rot, trans);

	float ss = trans.at<double>(3, 0);
	float tx = trans.at<double>(0, 0) / ss;
	float ty = trans.at<double>(1, 0) / ss;
	float tz = trans.at<double>(2, 0) / ss;
	Point3f realTrans = Point3f(tx, ty, tz);
	//cout << realTrans << endl;
	vector<Point2f> cam0pnts, undist0;
	vector<Point2f> cam1pnts, undist1;

	int cuttoffz = 50;
	// stereo match filter and triangulation
	if (methode == TRIANG_LINEAR) { // linear triangulization
		for (int j = 0; j < kp1.size(); j++)
		{
			Point2f pp1 = kp1[j].pt;
			Point2f pp2 = kp2[j].pt;
			if (pp1.y > pp2.y - 5 && pp1.y < pp2.y + 5 && pp1.x >= pp2.x && pp1.x <= pp2.x + 50) {
				cam0pnts.push_back(pp1);
				cam1pnts.push_back(pp2);
				Mat p1(3, 1, CV_64F);
				p1.at<double>(0, 0) = pp1.x;
				p1.at<double>(1, 0) = pp1.y;
				p1.at<double>(2, 0) = 1;
				Mat p2(3, 1, CV_64F);
				p2.at<double>(0, 0) = pp2.x;
				p2.at<double>(1, 0) = pp2.y;
				p2.at<double>(2, 0) = 1;
				Mat hoo = lib_triangulate_linear_ls(cameraleft, cameraright, p1, p2);
				float s = hoo.at<double>(3, 0);
				float x = hoo.at<double>(0, 0) / s;
				float y = hoo.at<double>(1, 0) / s;
				float z = hoo.at<double>(2, 0) / s;
				// one more parameter to think of
				if (z>1 && z<cuttoffz) {
					uchar b = imgleft.at<uchar>(pp1.y, pp1.x);
					Vec3b col(b, b, b);
					colors.push_back(col);
					// -x, -y?
					points3d.push_back(Point3f(x, y, z));
					//cout << "x: " << x << "y: " << y << "z: " << z << endl;
					kpleft.push_back(kp1[j]);
					kpright.push_back(kp2[j]);
					descleft.push_back(desc1filt.row(j));
					descright.push_back(desc2filt.row(j));
				}
			}
		}
	}
	else {

		vector<KeyPoint> kp1temp;
		vector<KeyPoint> kp2temp;
		Mat desc1temp, desc2temp;
		for (int j = 0; j < kp1.size(); j++)
		{
			Point2f pp1 = kp1[j].pt;
			Point2f pp2 = kp2[j].pt;
			if (pp1.y > pp2.y - 3 && pp1.y < pp2.y + 3 && pp1.x >= pp2.x && pp1.x <= pp2.x+50) {
				cam0pnts.push_back(pp1);
				cam1pnts.push_back(pp2);
				kp1temp.push_back(kp1[j]);
				kp2temp.push_back(kp2[j]);
				desc1temp.push_back(desc1filt.row(j));
				desc2temp.push_back(desc2filt.row(j));
			}
		}
		//line(imgc,)
		//undistortPoints(cam0pnts, undist0, kk, noArray());
		//undistortPoints(cam1pnts, undist1, kk, noArray());
		Mat out3d;
		//triangulatePoints(PPP, PPP2, undist0, undist1, out3d);
		triangulatePoints(cameraleft, cameraright, cam0pnts, cam1pnts, out3d);
		Mat pt_3d; 
		convertPointsFromHomogeneous(Mat(out3d.t()).reshape(4, 1), pt_3d);
		//cout << lib_type2str(pt_3d.type());
		for (int j = 0; j < out3d.cols; j++)
		{
			float s = out3d.at<float>(3, j);
			float x = out3d.at<float>(0, j) / s;
			float y = out3d.at<float>(1, j) / s;
			float z = out3d.at<float>(2, j) / s;

			float xpt = pt_3d.at<Vec3f>(j, 0)[0];
			float ypt = pt_3d.at<Vec3f>(j, 0)[1];
			float zpt = pt_3d.at<Vec3f>(j, 0)[2];

			if (z>1 && z < cuttoffz) {
				uchar b = imgleft.at<uchar>(kp1temp[j].pt.y, kp1temp[j].pt.x);
				Vec3b col(b, b, b);
				colors.push_back(col);
				points3d.push_back(Point3f(x, y, z));
				//cout << "x: " << x << "y: " << y << "z: " << z << endl;
				kpleft.push_back(kp1temp[j]);
				kpright.push_back(kp2temp[j]);
				descleft.push_back(desc1temp.row(j));
				descright.push_back(desc2temp.row(j));
			}
		}
	}
	return kpleft.size();
}

int test_singlematch_pnp(Mat camleft, Mat camright, Mat left_a, Mat right_a, Mat left_b, Point3f & Tout, Mat & Rout)
{

	//http://www.mrt.kit.edu/z/publ/download/2013/GeigerAl2013IJRR.pdf
	// camera 0.54 metere.
	// � Camera: x = right, y = down, z = forward
	// um no?
	// GPS / IMU: x = forward, y = left, z = up
	// gps to camera X = -y, Y = -z, Z = x // only raw data.
	Mat K, rot, trans;
	decomposeProjectionMatrix(camright, K, rot, trans);
	
	vector<Point3f> points3d;
	vector<KeyPoint> kpleft_a;
	vector<KeyPoint> kpright_a;
	vector<KeyPoint> kpleft_b;
	Mat descleft_a;
	Mat descright_a;
	Mat descleft_b;
	Mat allColors(0, 0, CV_8UC3);

	grid_params gp;
	gp.detector = ORB::create(200);
	gp.cellcols = 16;
	gp.cellrows = 6;
	gp.percell = 20;
	gp.maxdist = 2;

	int pointsfound = test_triangulate_linear(camleft, camright, 
												left_a, right_a, 
												kpleft_a, kpright_a, 
												descleft_a, descright_a, 
												gp, points3d, allColors, TRIANG_LINEAR);

	if (pointsfound <= 0) return 0;

	// some parameters to take into the table (performance vs speed)
	lib_extract_grid_features_params(left_b, kpleft_b, gp);
	gp.detector->compute(left_b, kpleft_b, descleft_b);

	//find possigle matches
	vector<DMatch> matches1;
	lib_match_descriptors(descleft_b, descleft_a, matches1);
	lib_filter_matches(kpleft_b, kpleft_a, matches1);

	Mat desc1filt, desc2filt;
	for (int i = 0; i < matches1.size(); i++)
	{
		desc1filt.push_back(descleft_b.row(matches1[i].queryIdx));
		desc2filt.push_back(descleft_a.row(matches1[i].trainIdx));
	}

	vector<Point3f> poi3dfor3;
	for (int j = 0; j < matches1.size(); j++)
	{
		Point3f p3f = points3d[matches1[j].trainIdx];
		poi3dfor3.push_back(p3f);
	}

	vector<Point2f> imgapoints;
	KeyPoint::convert(kpleft_b, imgapoints);
	Mat rvec, tvec;
	Mat pnpmask;
	solvePnPRansac(poi3dfor3, imgapoints, K, noArray(), rvec, tvec, false, 600, 1, 0.9989999, pnpmask, SOLVEPNP_ITERATIVE); //CV_P3P
	Point3f tvecp = Point3f(-tvec.at<double>(0, 0), -tvec.at<double>(1, 0), -tvec.at<double>(2, 0));
	Tout = Point3f(tvecp);
	Rodrigues(rvec, Rout);
	//cout << "rvec: " << rvec << "\n";
	//cout << "tvec: " << tvecp << endl;
	
	if (0) {
		viz::Viz3d myWindow("Coordinate Frame");
		/// Add coordinate axes
		myWindow.showWidget("Coordinate Widget", viz::WCoordinateSystem());
		Mat testt(points3d);
		viz::WCloud cws(testt, allColors);
		cws.setRenderingProperty(viz::POINT_SIZE, 3);
		myWindow.showWidget("CloudWidget2", cws);
		/// Add coordinate axes
		myWindow.showWidget("Coordinate Widget", viz::WCoordinateSystem());
		/// Let's assume camera has the following properties
		Mat rotm;
		Rodrigues(rvec, rotm);
		//cout << rotm << endl;
		Mat dir = Mat(3, 1, CV_64F);
		dir.at<double>(0, 0) = 0;
		dir.at<double>(1, 0) = 0;
		dir.at<double>(2, 0) = 1;
		Mat view = rotm*dir;
		// - tvec because solvepnp tvec points toward the origin
		// http://stackoverflow.com/questions/17423302/opencv-solvepnp-tvec-units-and-axes-directions
		//"Tvec points to the origin of the world coordinates in which You placed the calibration object. So if You placed the first object point in (0,0), thats where tvec will point to."
		Vec3d cam_pos(-tvec.at<double>(0, 0), -tvec.at<double>(1, 0), -tvec.at<double>(2, 0));
		Vec3d cam_focal_point(-tvec.at<double>(0, 0) + view.at<double>(0, 0),
			-tvec.at<double>(1, 0) + view.at<double>(1, 0),
			-tvec.at<double>(2, 0) + view.at<double>(2, 0));
		Vec3d cam_y_dir(0.0f, 1.0f, 0.0f);
		/// We can get the pose of the cam using makeCameraPose
		Affine3f cam_pose = viz::makeCameraPose(cam_pos, cam_focal_point, cam_y_dir);
		viz::WCameraPosition cpw(0.5); // Coordinate axes
		viz::WCameraPosition cpw_frustum(Vec2f(0.889484, 0.523599)); // Camera frustum
		myWindow.showWidget("CPW", cpw, cam_pose);
		myWindow.showWidget("CPW_FRUSTUM", cpw_frustum, cam_pose);
		myWindow.spinOnce(1, true);

		Mat towhow;
		drawKeypoints(left_a, kpleft_a, towhow);
		imshow("left_a img", towhow);

		Mat rightshow;
		drawKeypoints(right_a, kpright_a, rightshow);
		//imshow("right_a img", rightshow);

		Mat nextimg;
		drawKeypoints(left_b, kpleft_b, nextimg);
		imshow("left_b img", nextimg);

		waitKey(0);
	}

	return 1;
}