#ifndef THREADEDVERSION_H
#define THREADEDVERSION_H

#include "mylib.h"
#include "opencv2/core.hpp"
#include "opencv2/core/utility.hpp"

#define STEREO_SAD 1
#define STEREO_BF 2

class PreCalc
{
public:
	int imgidx;
	cv::Mat pose;
	std::vector<cv::KeyPoint> kp1;
	std::vector<cv::KeyPoint> kp2;
	std::vector<cv::Point3f> pointsnear;
	std::vector<cv::Point3f> pointsfar;

	cv::Mat points3d;
	cv::Mat desc1;
	cv::Mat desc2;
	PreCalc() {  }
};

struct ThreadConf
{
	grid_params gp;
	cv::Mat camleft;
	cv::Mat camright;
	int KSLAM_seq_length = 15;
	double KSLAM_u = 0.32;
	double KSLAM_thresh = 41;
	int PNP_method = cv::SOLVEPNP_ITERATIVE;
	int PNP_iterations = 1000;
	int use_sub_pixecl = 0;
	int use_kalman = 0;
	int use_pr = 1;
	double PNP_projection_error = 1;
	double PNP_confidence = 0.9899999;
	int stereo_matching_method = STEREO_BF;
	int lookback = 300;
	int cuttoffz = 6000;
	int use_refinement = 1;
};

int init_system(std::string filea, ThreadConf thc);
int step_first(cv::Mat leftimg, cv::Mat rightimg);
int triangulate_thread(const cv::Mat leftimg, const cv::Mat rightimg, std::vector<cv::KeyPoint> kp1, cv::Mat desc1, PreCalc * pcfill, ThreadConf * opts);
int write_all_to_file();

#endif // DEBUG