#include <stdio.h>
#include <iostream>
#include <fstream>
#include <chrono>
#include <time.h>
#include <ctime>
#include <math.h>
#include <thread>

#include "opencv2/core.hpp"
#include "opencv2/core/utility.hpp"
#include "opencv2/features2d.hpp"
#include "opencv2/xfeatures2d.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/calib3d.hpp"
#include "opencv2/video/tracking.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/viz.hpp"

#include "ThreadVersion.h"
#include "Odometry.h"
#include "SeqSLAM.h"
#include "KSLAM.h"
#include "Tests.h"
#include "FullSystem1.h"
#include "mylib.h"
#include "trainandrun.h"
#include "ThreadVersion.h"

using namespace cv;
using namespace std;
using namespace std::chrono;

static Mat K, rot, trans;

static vector<PreCalc> trained_points;
static Mat R = Mat::eye(3, 3, CV_64FC1);
static Mat t = Mat::zeros(3, 1, CV_64FC1);

static int imageidx = 0;
static PreCalc * prev;
static ofstream finalf;
static char text[400];
static ThreadConf tc;
static PreCalc *pc;
static KSLAM *kslam;
static int matchcount = 0;
static int ret = 0;
static string filename;

static KalmanFilter KF;         // instantiate Kalman Filter

// Converts a given Rotation Matrix to Euler angles
cv::Mat rot2euler(const cv::Mat & rotationMatrix)
{
	cv::Mat euler(3, 1, CV_64F);

	double m00 = rotationMatrix.at<double>(0, 0);
	double m02 = rotationMatrix.at<double>(0, 2);
	double m10 = rotationMatrix.at<double>(1, 0);
	double m11 = rotationMatrix.at<double>(1, 1);
	double m12 = rotationMatrix.at<double>(1, 2);
	double m20 = rotationMatrix.at<double>(2, 0);
	double m22 = rotationMatrix.at<double>(2, 2);

	double x, y, z;

	// Assuming the angles are in radians.
	if (m10 > 0.998) { // singularity at north pole
		x = 0;
		y = CV_PI / 2;
		z = atan2(m02, m22);
	}
	else if (m10 < -0.998) { // singularity at south pole
		x = 0;
		y = -CV_PI / 2;
		z = atan2(m02, m22);
	}
	else
	{
		x = atan2(-m12, m11);
		y = asin(m10);
		z = atan2(-m20, m00);
	}

	euler.at<double>(0) = x;
	euler.at<double>(1) = y;
	euler.at<double>(2) = z;

	return euler;
}

// Converts a given Euler angles to Rotation Matrix
cv::Mat euler2rot(const cv::Mat & euler)
{
	cv::Mat rotationMatrix(3, 3, CV_64F);

	double x = euler.at<double>(0);
	double y = euler.at<double>(1);
	double z = euler.at<double>(2);

	// Assuming the angles are in radians.
	double ch = cos(z);
	double sh = sin(z);
	double ca = cos(y);
	double sa = sin(y);
	double cb = cos(x);
	double sb = sin(x);

	double m00, m01, m02, m10, m11, m12, m20, m21, m22;

	m00 = ch * ca;
	m01 = sh*sb - ch*sa*cb;
	m02 = ch*sa*sb + sh*cb;
	m10 = sa;
	m11 = ca*cb;
	m12 = -ca*sb;
	m20 = -sh*ca;
	m21 = sh*sa*cb + ch*sb;
	m22 = -sh*sa*sb + ch*cb;

	rotationMatrix.at<double>(0, 0) = m00;
	rotationMatrix.at<double>(0, 1) = m01;
	rotationMatrix.at<double>(0, 2) = m02;
	rotationMatrix.at<double>(1, 0) = m10;
	rotationMatrix.at<double>(1, 1) = m11;
	rotationMatrix.at<double>(1, 2) = m12;
	rotationMatrix.at<double>(2, 0) = m20;
	rotationMatrix.at<double>(2, 1) = m21;
	rotationMatrix.at<double>(2, 2) = m22;

	return rotationMatrix;
}
void initKalmanFilter(KalmanFilter &KF, int nStates, int nMeasurements, int nInputs, double dt)
{

	KF.init(nStates, nMeasurements, nInputs, CV_64F);                 // init Kalman Filter

	setIdentity(KF.processNoiseCov, Scalar::all(1e-5));       // set process noise
	setIdentity(KF.measurementNoiseCov, Scalar::all(1e-2));   // set measurement noise
	setIdentity(KF.errorCovPost, Scalar::all(1));             // error covariance


	/** DYNAMIC MODEL **/

	//  [1 0 0 dt  0  0 dt2   0   0 0 0 0  0  0  0   0   0   0]
	//  [0 1 0  0 dt  0   0 dt2   0 0 0 0  0  0  0   0   0   0]
	//  [0 0 1  0  0 dt   0   0 dt2 0 0 0  0  0  0   0   0   0]
	//  [0 0 0  1  0  0  dt   0   0 0 0 0  0  0  0   0   0   0]
	//  [0 0 0  0  1  0   0  dt   0 0 0 0  0  0  0   0   0   0]
	//  [0 0 0  0  0  1   0   0  dt 0 0 0  0  0  0   0   0   0]
	//  [0 0 0  0  0  0   1   0   0 0 0 0  0  0  0   0   0   0]
	//  [0 0 0  0  0  0   0   1   0 0 0 0  0  0  0   0   0   0]
	//  [0 0 0  0  0  0   0   0   1 0 0 0  0  0  0   0   0   0]
	//  [0 0 0  0  0  0   0   0   0 1 0 0 dt  0  0 dt2   0   0]
	//  [0 0 0  0  0  0   0   0   0 0 1 0  0 dt  0   0 dt2   0]
	//  [0 0 0  0  0  0   0   0   0 0 0 1  0  0 dt   0   0 dt2]
	//  [0 0 0  0  0  0   0   0   0 0 0 0  1  0  0  dt   0   0]
	//  [0 0 0  0  0  0   0   0   0 0 0 0  0  1  0   0  dt   0]
	//  [0 0 0  0  0  0   0   0   0 0 0 0  0  0  1   0   0  dt]
	//  [0 0 0  0  0  0   0   0   0 0 0 0  0  0  0   1   0   0]
	//  [0 0 0  0  0  0   0   0   0 0 0 0  0  0  0   0   1   0]
	//  [0 0 0  0  0  0   0   0   0 0 0 0  0  0  0   0   0   1]

	// position
	KF.transitionMatrix.at<double>(0, 3) = dt;
	KF.transitionMatrix.at<double>(1, 4) = dt;
	KF.transitionMatrix.at<double>(2, 5) = dt;
	KF.transitionMatrix.at<double>(3, 6) = dt;
	KF.transitionMatrix.at<double>(4, 7) = dt;
	KF.transitionMatrix.at<double>(5, 8) = dt;
	KF.transitionMatrix.at<double>(0, 6) = 0.5*pow(dt, 2);
	KF.transitionMatrix.at<double>(1, 7) = 0.5*pow(dt, 2);
	KF.transitionMatrix.at<double>(2, 8) = 0.5*pow(dt, 2);

	// orientation
	KF.transitionMatrix.at<double>(9, 12) = dt;
	KF.transitionMatrix.at<double>(10, 13) = dt;
	KF.transitionMatrix.at<double>(11, 14) = dt;
	KF.transitionMatrix.at<double>(12, 15) = dt;
	KF.transitionMatrix.at<double>(13, 16) = dt;
	KF.transitionMatrix.at<double>(14, 17) = dt;
	KF.transitionMatrix.at<double>(9, 15) = 0.5*pow(dt, 2);
	KF.transitionMatrix.at<double>(10, 16) = 0.5*pow(dt, 2);
	KF.transitionMatrix.at<double>(11, 17) = 0.5*pow(dt, 2);


	/** MEASUREMENT MODEL **/

	//  [1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]
	//  [0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]
	//  [0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]
	//  [0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0]
	//  [0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0]
	//  [0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0]

	KF.measurementMatrix.at<double>(0, 0) = 1;  // x
	KF.measurementMatrix.at<double>(1, 1) = 1;  // y
	KF.measurementMatrix.at<double>(2, 2) = 1;  // z
	KF.measurementMatrix.at<double>(3, 9) = 1;  // roll
	KF.measurementMatrix.at<double>(4, 10) = 1; // pitch
	KF.measurementMatrix.at<double>(5, 11) = 1; // yaw

}

/**********************************************************************************************************/
void updateKalmanFilter(KalmanFilter &KF, Mat &measurement,
	Mat &translation_estimated, Mat &rotation_estimated)
{

	// First predict, to update the internal statePre variable
	Mat prediction = KF.predict();

	// The "correct" phase that is going to use the predicted value and our measurement
	Mat estimated = KF.correct(measurement);

	// Estimated translation
	translation_estimated.at<double>(0) = estimated.at<double>(0);
	translation_estimated.at<double>(1) = estimated.at<double>(1);
	translation_estimated.at<double>(2) = estimated.at<double>(2);

	// Estimated euler angles
	Mat eulers_estimated(3, 1, CV_64F);
	eulers_estimated.at<double>(0) = estimated.at<double>(9);
	eulers_estimated.at<double>(1) = estimated.at<double>(10);
	eulers_estimated.at<double>(2) = estimated.at<double>(11);

	// Convert estimated quaternion to rotation matrix
	rotation_estimated = euler2rot(eulers_estimated);

}

/**********************************************************************************************************/
void fillMeasurements(Mat &measurements,
	const Mat &translation_measured, const Mat &rotation_measured)
{
	// Convert rotation matrix to euler angles
	Mat measured_eulers(3, 1, CV_64F);
	measured_eulers = rot2euler(rotation_measured);

	// Set measurement to predict
	measurements.at<double>(0) = translation_measured.at<double>(0); // x
	measurements.at<double>(1) = translation_measured.at<double>(1); // y
	measurements.at<double>(2) = translation_measured.at<double>(2); // z
	measurements.at<double>(3) = measured_eulers.at<double>(0);      // roll
	measurements.at<double>(4) = measured_eulers.at<double>(1);      // pitch
	measurements.at<double>(5) = measured_eulers.at<double>(2);      // yaw
}
static Mat measurements;

ofstream matchfile;
int init_system(string filea, ThreadConf thc)
{
	tc = thc;

	trained_points.clear();
	trained_points.reserve(5000);
	R = Mat::eye(3, 3, CV_64FC1);
	t = Mat::zeros(3, 1, CV_64FC1);
	imageidx = 0;

	decomposeProjectionMatrix(tc.camright, K, rot, trans);
	filename = filea;
	//relative.open(fileb);

	kslam = new KSLAM();
	kslam->part_of_seq_threshold = tc.KSLAM_thresh;
	kslam->min_frames_for_seq = tc.KSLAM_seq_length;
	long filetime = std::time(0);
	char myfile_path[200];
	sprintf_s(myfile_path, "C:/Matlab/custom_kslam_relative/matches_%d.csv", filetime);
	matchfile.open(myfile_path);
	int nStates = 18;            // the number of states
	int nMeasurements = 6;       // the number of measured states
	int nInputs = 0;             // the number of control actions
	double dt = 0.125;           // time between measurements (1/FPS)
	initKalmanFilter(KF, nStates, nMeasurements, nInputs, dt);    // init function
	measurements = Mat(nMeasurements, 1, CV_64F);
	measurements.setTo(Scalar(0));

	return 1;
}

int run_kslam(Mat img)
{
	high_resolution_clock::time_point t1 = high_resolution_clock::now();
	ret = kslam->step_custom(img);
	high_resolution_clock::time_point t2 = high_resolution_clock::now();
	auto duratation = duration_cast<milliseconds>(t2 - t1).count();
	cout << "KSLAM: " << duratation << "ms\n";
	return ret;
	/*if (waitKey(1) == 's')
	{
		kslam->show_system_image();
		waitKey(0);
	}

	if (kslam->correct_matches->size() > 0)
	{
		int numMatches = kslam->correct_matches->size();
		for (int j = 0; j < numMatches; j++)
		{
			singleMatch *sm = &(kslam->correct_matches->at(j));
			//cout << sm->a << "," << sm->b << endl;
		}
	}
	high_resolution_clock::time_point t2 = high_resolution_clock::now();
	auto duratation = duration_cast<milliseconds>(t2 - t1).count();
	cout << "KSLAM: "<< duratation << "ms\n";
	return 1;*/
}

int correct_map(int idxprev, int idxnow)
{
	return 1;
}

int find_corr_points_using_sad(vector<KeyPoint> & kp1, vector<Point2f> * kp2,Mat & disc, Mat left, Mat right)
{
	Mat diff;
	int ll = 3;
	int rr = 4;
	vector<KeyPoint> kp1new;
	Mat filtdisc;
	for (int i = 0; i < kp1.size(); i++)
	{
		Point2f kpl = kp1[i].pt;
		kpl.x = (int)kpl.x;
		kpl.y = (int)kpl.y;
		if (kpl.y < left.rows - 4 && kpl.y > 4){
			Mat left_patch = left.colRange(kpl.x - ll, kpl.x + rr).rowRange(kpl.y - ll, kpl.y + rr);
			//imshow("patch", left_patch);
			double min_sad = 9999999;
			int min_x = 0;
			int loopcounter = 0;
			int minidx = 0;
			int prev_min_x = -1;
			int prev_min_sad = -1;
			int counter = 0;
			vector<double> sads;
			for (int j = max(kpl.x - 60, 10.0f); j < min(kpl.x + 5, (float)right.cols - 7); j++)
			{
				Mat right_patch = right.colRange(j - ll, j + rr).rowRange(kpl.y - ll, kpl.y + rr);
				if (right_patch.cols != left_patch.cols || right_patch.rows != left_patch.rows){
					cout << "patch size error\n";
				}
				//imshow("patch_r", right_patch);
				absdiff(left_patch, right_patch, diff);
				double d = sum(diff)[0];
				sads.push_back(d);
				//cout << d << endl;
				if (d < min_sad){
					prev_min_sad = min_sad;
					min_sad = d;
					prev_min_x = min_x;
					min_x = j;
					minidx = counter;
				}
				counter++;
				//waitKey(0);
			}

			double dist = min_x;
			if (minidx > 0 && minidx < sads.size() - 1)
			{
				double c1 = sads[minidx - 1];
				double c2 = sads[minidx];
				double c3 = sads[minidx + 1];
				//dist = (double)min_x - 0.5 * ((c3 - c1) / (c1 - 2 * c2 + c3));
			}

			// ratio test
			if (min_sad > 0 && (float)min_sad / (float)prev_min_sad < 0.9)
			{
				Point2f newp;
				newp.x = dist;
				newp.y = kpl.y;
				kp2->push_back(newp);
				kp1new.push_back(kp1[i]);
				filtdisc.push_back(disc.row(i));
			}
			//cout << "kp: " << i+1 << "/" << kp1.size() << endl;
		}
	}
	kp1 = kp1new;
	disc = filtdisc;
	return 1;
}

int triangulate_thread(const Mat leftimg, const Mat rightimg, vector<KeyPoint> kp1, Mat desc1, PreCalc * pcfill, ThreadConf * opts)
{
	high_resolution_clock::time_point t1 = high_resolution_clock::now();
	vector<KeyPoint> kp1new(kp1);
	vector<KeyPoint> kp2;
	Mat desc2;
	vector<DMatch> matches1;
	Mat desc1new(desc1);
	Mat desc1filt, desc2filt;

	if (opts->stereo_matching_method == STEREO_SAD)
	{

		//cout << "SAD\n";
		vector<Point2f> *kpp2 = new vector<Point2f>();
		find_corr_points_using_sad(kp1new, kpp2, desc1new, leftimg, rightimg);
		KeyPoint::convert(*kpp2, kp2);
		desc1filt = desc1new;
		//cout << "SADEND\n";
	}
	else
	{
		lib_extract_grid_features_params(rightimg, kp2, opts->gp);
		//gpnew.detector->detect(rightimg,kp2);
		opts->gp.detector->compute(rightimg, kp2, desc2);
		lib_match_descriptors(desc1new, desc2, matches1);
		lib_filter_matches(kp1new, kp2, matches1);
		for (int i = 0; i < matches1.size(); i++)
		{
			desc1filt.push_back(desc1new.row(matches1[i].queryIdx));
			//desc2filt.push_back(desc2.row(matches1[i].trainIdx));
		}
	}

	int cuttoffz = 50;//opts->cuttoffz;
	// stereo match filter and triangulation
	Size winSize = Size(5, 5);
	Size zeroZone = Size(-1, -1);
	TermCriteria criteria = TermCriteria(CV_TERMCRIT_EPS + CV_TERMCRIT_ITER, 40, 0.001);
	/// Calculate the refined corner locations
	if (tc.use_sub_pixecl > 0) {
		vector<Point2f> imgapoints2;
		KeyPoint::convert(kp2, imgapoints2);
		cornerSubPix(rightimg, imgapoints2, winSize, zeroZone, criteria);
		KeyPoint::convert(imgapoints2, kp2);
	}
	std::vector<cv::KeyPoint> kp1f;
	std::vector<cv::KeyPoint> kp2f;
	Mat desc1f;
	Mat desc2f;
	Mat p1(3, 1, CV_64F);
	Mat p2(3, 1, CV_64F);
	for (int j = 0; j < kp1new.size(); j++)
	{
		Point2f pp1 = kp1new[j].pt;
		Point2f pp2 = kp2[j].pt;
		if (pp1.y > pp2.y - 5 && pp1.y < pp2.y + 5 && pp1.x >= pp2.x && pp1.x <= pp2.x + 60) {
			p1.at<double>(0, 0) = pp1.x;
			p1.at<double>(1, 0) = pp1.y;
			p1.at<double>(2, 0) = 1;
			p2.at<double>(0, 0) = pp2.x;
			p2.at<double>(1, 0) = pp2.y;
			p2.at<double>(2, 0) = 1;
			Mat hoo = lib_triangulate_linear_ls(opts->camleft, opts->camright, p1, p2);
			float s = hoo.at<double>(3, 0);
			float x = hoo.at<double>(0, 0) / s;
			float y = hoo.at<double>(1, 0) / s;
			float z = hoo.at<double>(2, 0) / s;
			// one more parameter to think of
			if (z>3 && z<cuttoffz) {
				//pc->points3d.push_back(Point3f(x, y, z));
				pcfill->pointsnear.push_back(Point3f(x, y, z));
				//cout << "x: " << x << "y: " << y << "z: " << z << endl;
				pcfill->kp1.push_back(kp1new[j]);
				pcfill->kp2.push_back(kp2[j]);
				pcfill->desc1.push_back(desc1filt.row(j));
				//pc->desc2.push_back(desc2filt.row(j));
			}
		}
	}
	/*
	Mat imagge;
	cvtColor(leftimg, imagge, CV_GRAY2RGB);
	for (int i = 0; i < pcfill->kp1.size(); i++)
	{
		float z = pcfill->pointsnear[i].z / cuttoffz;

		float r = 255*(z);
		float g = 0;
		float b = 255*(1-z);
		circle(imagge, pcfill->kp1[i].pt, 2, Scalar(r, g, b), CV_FILLED);
	}
	imshow("deapth", imagge);
	waitKey(0);
	imagge.release();
	*/
	high_resolution_clock::time_point t2 = high_resolution_clock::now();
	auto duratation = duration_cast<milliseconds>(t2 - t1).count();
	cout << "Find Matches: " << duratation << "ms\n";
	return 1;
}

int step_first(Mat leftimg, Mat rightimg)
{
	thread loop_detector;
	if (tc.use_pr)
	{
		loop_detector = thread(run_kslam, leftimg);
	}

	pc = new PreCalc();

	vector<KeyPoint> kp1;
	Mat desc1;

	//cout << "EXTRACT left\n";
	lib_extract_grid_features_params(leftimg, kp1, tc.gp);
	//gpnew.detector->detect(leftimg, kp1);

	/*Mat withk;
	drawKeypoints(leftimg, kp1, withk);
	imshow("new", withk);
	waitKey(1);*/

	vector<KeyPoint> kp1new(kp1);
	tc.gp.detector->compute(leftimg, kp1, desc1);
	
	/// Calculate the refined corner locations
	if (tc.use_sub_pixecl){
		Size winSize = Size(5, 5);
		Size zeroZone = Size(-1, -1);
		TermCriteria criteria = TermCriteria(CV_TERMCRIT_EPS + CV_TERMCRIT_ITER, 40, 0.001);
		vector<Point2f> imgapoints2;
		cout << "SUBPIX\n";
		KeyPoint::convert(kp1, imgapoints2);
		cornerSubPix(leftimg, imgapoints2, winSize, zeroZone, criteria);
		KeyPoint::convert(imgapoints2, kp1);
	}
	if (tc.use_pr)
	{
		loop_detector.join();
	}
	if (ret > 0){
		matchfile << trained_points.size() << "," << ret << endl;
	}
	//ret = 0;
	if (ret == -2 && imageidx > 0){
		PreCalc *pcprev1 = &(trained_points[trained_points.size() - 1]);
		trained_points.push_back(*pcprev1);
		return 1;
	}

	//cout << "START thread\n";
	thread tria = thread(triangulate_thread, leftimg, rightimg, kp1, desc1, pc, &tc);

	high_resolution_clock::time_point t1 = high_resolution_clock::now();

	int inliers = 0;
	if (imageidx > 0)
	{
		PreCalc * pcprev = 0;
		bool useprev = false;
		if (ret > 0){
			matchcount = 0;
			pcprev = &(trained_points[ret]);
			pcprev->pose.rowRange(0, 3).colRange(0, 3).copyTo(R);
			pcprev->pose.col(3).copyTo(t);
		}
		/*if (kslam->current_seq_streak >= kslam->min_frames_for_seq && kslam->current_seq_without_fail > 5 && tc.use_pr)
		{
			matchcount++;
			//useprev = true;
			singleMatch *sm = &(kslam->current_seq_matches->at(kslam->current_seq_matches->size() - 1));
			cout << "using: " << sm->a << " - " << sm->b << endl;
			pcprev = &(trained_points[sm->b]);

			// stop loop closures with to big of an error; 300m
			if (abs(pcprev->pose.at<double>(2, 3) - t.at<double>(2, 0)) > 200)
			{
				matchcount = 0;
				useprev = false;
				pcprev = &(trained_points[imageidx - 1]);
				kslam->current_seq_without_fail = 0;
				kslam->current_seq_streak = 0;
				kslam->current_seq_matches->clear();
			}
			//useprev = true;
			//ret = (*kslam->current_seq_matches)[kslam->current_seq_matches->size() - 1].a;
			//pcprev = &(trained_points[ret]);
			char path[300];
			sprintf_s(path, "C:/Matlab/datasets/kitti/%02d/image_0/%06d.png", 0, ret);
			Mat templeft = imread(path, IMREAD_GRAYSCALE);
			imshow("hasdas", templeft);
			imshow("now", leftimg);
			waitKey(0);
		}*/
		else
		{
			matchcount = 0;
			pcprev = &(trained_points[trained_points.size() - 1]);
		}

		//cout << "GETPREV\n";
		//pcprev = &(trained_points[trained_points.size() - 1]);
		vector<DMatch> matches;

		//cout << "MATCH desc\n";
		lib_match_descriptors(pcprev->desc1, desc1, matches);
		vector<KeyPoint> kpf;
		vector<Point3f> poi3dfor3;
		cout << "Matches: " << matches.size() << endl;
		for (int i = 0; i < matches.size(); i++)
		{
			KeyPoint pm1 = kp1[matches[i].trainIdx];
			kpf.push_back(pm1);
			Point3f p3f = pcprev->pointsnear[matches[i].queryIdx];
			poi3dfor3.push_back(p3f);
		}


		//cout << "START PNP\n";
		vector<Point2f> imgapoints;
		KeyPoint::convert(kpf, imgapoints);
		
		/*double average = 0;
		for (int i = 0; i < matches.size(); i++)
		{
			average += abs(pcprev->kp2[matches[i].queryIdx].pt.y - kp1[matches[i].trainIdx].pt.y);
		}
		average = average / kp1.size();
		for (int i = 0; i < imgapoints.size(); i++)
		{
			imgapoints[i].y -= average;
		}*/

		Mat rvec, tvec;
		Mat pnpmask;

		Mat CR;
		Mat Ct(3, 1, CV_64FC1);
		Mat told;
		Point3f T;
		T.x = 0;
		T.y = 0;
		T.z = 0;
		bool pnpb = false;
		if (imgapoints.size() > 100)
		{
			pnpb = solvePnPRansac(poi3dfor3, imgapoints, K, noArray(), rvec, tvec, false, tc.PNP_iterations, tc.PNP_projection_error, tc.PNP_confidence, pnpmask, tc.PNP_method);
			T.x = -tvec.at<double>(0, 0);
			T.y = -tvec.at<double>(1, 0);
			T.z = -tvec.at<double>(2, 0);

			//cout << rvec << endl;
			//rvec.at<double>(0) = 0;// rvec.at<double>(0)*0.0001;
			//T.y = 0;
			Rodrigues(rvec, CR);
			CR = CR.t();
			Ct.at<double>(0, 0) = T.x;
			Ct.at<double>(1, 0) = T.y;
			Ct.at<double>(2, 0) = T.z;

			Rodrigues(rvec, CR);
			CR = CR.t();

		
			//cout << "tx: " << tx << " ty: " << ty << " tz: " << tz;
			
			//tx = tx * 0.1;


			//CR = Mat::eye(3, 3, CV_64FC1);

			inliers = pnpmask.rows;
			t.copyTo(told);

		}
		double distance = norm(T);
		if (imgapoints.size() < 100 || inliers < 15 || distance > 6 || distance < 0.05)
		{
			cout << "bad pose to few inliers " << endl;
			cout << T << endl;
			Mat withk;
			drawKeypoints(leftimg, kp1, withk);
			imshow("new", withk);
			waitKey(1);
			pcprev->pose.rowRange(0, 3).colRange(0, 3).copyTo(R);
			pcprev->pose.col(3).copyTo(t);
			useprev = true;
		}
		else{
			if (tc.use_refinement) // refine pose using pnp
			{
				vector<Point2f> inliers_img;
				vector<Point3f> inliers_3d;
				for (int inliers_index = 0; inliers_index < pnpmask.rows; ++inliers_index)
				{
					int n = pnpmask.at<int>(inliers_index);         // i-inlier
					Point2f point2d = imgapoints[n];               // i-inlier point 2D
					inliers_img.push_back(point2d);               // add i-inlier to list
					inliers_3d.push_back(poi3dfor3[n]);
				}
				solvePnP(inliers_3d, inliers_img, K, noArray(), rvec, tvec, false, SOLVEPNP_ITERATIVE);

				/*Mat withk;
				vector<KeyPoint> keypoints;
				KeyPoint::convert(inliers_img, keypoints);
				drawKeypoints(leftimg, keypoints, withk, Scalar(255, 0, 0));
				imshow("inliers", withk);
				waitKey(1);*/

				T = Point3f(-tvec.at<double>(0, 0), -tvec.at<double>(1, 0), -tvec.at<double>(2, 0));
				Ct.at<double>(0, 0) = T.x;
				Ct.at<double>(1, 0) = T.y;
				Ct.at<double>(2, 0) = T.z;

				Rodrigues(rvec, CR);

				double tx = atan2(CR.at<double>(2, 1), CR.at<double>(2, 2));
				double ty = atan2(-CR.at<double>(2, 0), sqrt(pow(CR.at<double>(2, 1), 2) + pow(CR.at<double>(2, 2), 2)));
				double tz = atan2(CR.at<double>(1, 0), CR.at<double>(0, 0));

				Mat XX = Mat::zeros(3, 3, CV_64FC1);
				XX.at<double>(0, 0) = 1;
				XX.at<double>(1, 1) = cos(tx);
				XX.at<double>(1, 2) = -sin(tx);
				XX.at<double>(2, 1) = sin(tx);
				XX.at<double>(2, 2) = cos(tx);

				Mat YY = Mat::zeros(3, 3, CV_64FC1);
				YY.at<double>(0, 0) = cos(ty);
				YY.at<double>(0, 2) = sin(ty);
				YY.at<double>(1, 1) = 1;
				YY.at<double>(2, 0) = -sin(ty);
				YY.at<double>(2, 2) = cos(ty);

				Mat ZZ = Mat::zeros(3, 3, CV_64FC1);
				ZZ.at<double>(0, 0) = cos(tz);
				ZZ.at<double>(0, 1) = -sin(tz);
				ZZ.at<double>(1, 0) = sin(tz);
				ZZ.at<double>(1, 1) = cos(tz);
				ZZ.at<double>(2, 2) = 1;
				//CR = Mat(YY);
				CR = CR.t();

				distance = norm(T);
				if (distance > 6 || distance < 0.05 || abs(ty) > 3.14/8)
				{
					cout << "bad pose to few inliers " << endl;
					cout << T << endl;
					Mat withk;
					drawKeypoints(leftimg, kp1, withk);
					imshow("new", withk);
					waitKey(1);
					pcprev->pose.rowRange(0, 3).colRange(0, 3).copyTo(R);
					pcprev->pose.col(3).copyTo(t);
					useprev = true;
				}

				
			}
			if (useprev)
			{
				pcprev->pose.rowRange(0, 3).colRange(0, 3).copyTo(R);
				pcprev->pose.col(3).copyTo(t);
			}
			else{
				if (tc.use_kalman){
					fillMeasurements(measurements, Ct, CR);

					// Instantiate estimated translation and rotation
					Mat translation_estimated(3, 1, CV_64F);
					Mat rotation_estimated(3, 3, CV_64F);

					// update the Kalman filter with good measurements
					updateKalmanFilter(KF, measurements,
						translation_estimated, rotation_estimated);
					CR = rotation_estimated;
					Ct = translation_estimated;
				}
				Mat ttt = R*Ct;
				t = t + ttt;
				//if (!useprev)
				R = CR * R;
			}
			

			if (useprev)
			{
				if (matchcount == 1 && 0)
				{
					Mat tprev = pcprev->pose.col(3);
					Mat diff = tprev - told;
					cout << diff << endl;
					int pr = 0;
					int lookback = 300;
					double min = imageidx - 1 - lookback >= 0 ? imageidx - 1 - lookback : 0;
					cout << min << endl;
					int range = imageidx - min;
					cout << range << endl;
					for (int i = min; i < imageidx; i++){
						PreCalc * temp = &(trained_points[i]);
						Mat colll = temp->pose.col(3);
						Mat toAdd = (diff * (pr / (range*1.0)));
						temp->pose.col(3) = colll + toAdd;
						pr++;
					}
				}
			}
		}
	}
	high_resolution_clock::time_point t2 = high_resolution_clock::now();
	auto duratation = duration_cast<milliseconds>(t2 - t1).count();
	cout << "rest: "<< duratation << "ms\n";

	tria.join();

	if (0)
	{
		/*Mat withk;
		drawKeypoints(rightimg, pc->kp2, withk);
		imshow("right", withk);
		Mat withk2;
		drawKeypoints(leftimg, pc->kp1, withk2);
		imshow("left", withk2);

		viz::Viz3d myWindow("Coordinate Frame");
		/// Add coordinate axes
		myWindow.showWidget("Coordinate Widget", viz::WCoordinateSystem());
		Mat testt(pc->pointsnear);
		viz::WCloud cws(testt);
		cws.setRenderingProperty(viz::POINT_SIZE, 3);
		myWindow.showWidget("CloudWidget2", cws);
		/// Add coordinate axes
		myWindow.showWidget("Coordinate Widget", viz::WCoordinateSystem());
		/// Let's assume camera has the following properties
		//cout << rotm << endl;
		Mat dir = Mat(3, 1, CV_64F);
		dir.at<double>(0, 0) = 0;
		dir.at<double>(1, 0) = 0;
		dir.at<double>(2, 0) = 1;
		Mat view = R*dir;
		// - tvec because solvepnp tvec points toward the origin
		// http://stackoverflow.com/questions/17423302/opencv-solvepnp-tvec-units-and-axes-directions
		//"Tvec points to the origin of the world coordinates in which You placed the calibration object. So if You placed the first object point in (0,0), thats where tvec will point to."
		Vec3d cam_pos(0,0,0);
		Vec3d cam_focal_point(0,0,1);
		Vec3d cam_y_dir(0.0f, 1.0f, 0.0f);
		/// We can get the pose of the cam using makeCameraPose
		Affine3f cam_pose = viz::makeCameraPose(cam_pos, cam_focal_point, cam_y_dir);
		viz::WCameraPosition cpw(0.5); // Coordinate axes
		viz::WCameraPosition cpw_frustum(Vec2f(0.889484f, 0.523599f)); // Camera frustum
		myWindow.showWidget("CPW", cpw, cam_pose);
		myWindow.showWidget("CPW_FRUSTUM", cpw_frustum, cam_pose);
		myWindow.spinOnce(1, true);
		waitKey(0);*/
	}

	pc->imgidx = imageidx;
	pc->pose = Mat(R);
	hconcat(pc->pose, t, pc->pose);
	if (pc->kp1.size() < 5)  {
		cout << "error!" << endl;
		return 1;
	}
	trained_points.push_back(*pc);
	cout << "idx: " << imageidx << endl;
	imageidx++;
	cout << "inliers: " <<inliers << endl << endl;
	return 1;
}

int write_all_to_file()
{
	finalf.open(filename);
	for (int i = 0; i < trained_points.size(); i++){
		PreCalc *pc = &(trained_points[i]);
		Mat P = pc->pose;
		sprintf_s(text, "%.4f %.4f %.4f %.4f %.4f %.4f %.4f %.4f %.4f %.4f %.4f %.4f\n",
			P.at<double>(0, 0), P.at<double>(0, 1), P.at<double>(0, 2), P.at<double>(0, 3),
			P.at<double>(1, 0), P.at<double>(1, 1), P.at<double>(1, 2), P.at<double>(1, 3),
			P.at<double>(2, 0), P.at<double>(2, 1), P.at<double>(2, 2), P.at<double>(2, 3));
		finalf << text;
	}
	finalf.flush();
	finalf.close();
	return 1;
}

/*

*/