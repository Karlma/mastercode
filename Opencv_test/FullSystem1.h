#ifndef FULLS1_H
#define FULLS1_H

#include "opencv2/core.hpp"
#include "opencv2/core/utility.hpp"
#include "opencv2/features2d.hpp"
#include "opencv2/xfeatures2d.hpp"
int relative_no_scale_test();
int extract_grid_features(cv::Ptr<cv::ORB> detector, cv::Mat img, std::vector<cv::KeyPoint>& kpt, int cellx, int celly, int percell);

#endif // DEBUG