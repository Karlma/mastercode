#include <stdio.h>
#include <iostream>
#include <fstream>
#include <chrono>
#include <time.h>
#include <ctime>
#include "opencv2/core.hpp"
#include "opencv2/core/utility.hpp"
#include "opencv2/features2d.hpp"
#include "opencv2/xfeatures2d.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/calib3d.hpp"
#include "opencv2/video/tracking.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/viz.hpp"
#include "Odometry.h"
#include "SeqSLAM.h"
#include "Tests.h"
#include "FullSystem1.h"
#include "mylib.h"

using namespace cv;
using namespace std;
using namespace std::chrono;

static vector<int> va;
static vector<int> vb;
static Ptr<ORB> detector = ORB::create(4000);
static vector<Point3d> scores;
static vector<Mat> camMats;
static ofstream myfile;
static ofstream myfile2;
static Mat traj = Mat::zeros(800, 800, CV_8UC3);
static int xoffset = 280;// 280;
static int yoffset = 580;//580;
static int num_matches;
static double focal = 718.8560;
static Point2d PP(607.1928, 185.2157);

static char imagepath[100];

int test_stereo_pnp_match_init()
{
	lib_read_matches(va, vb, "C:/Matlab/matches_00_25100.csv");
	num_matches = (int)va.size();
	lib_read_gps_data_kitti(scores, "C:/Matlab/datasets/kitti/data_odometry_poses/dataset/poses/00.txt");
	lib_read_kitti_poses(camMats, "C:/Matlab/datasets/kitti/data_odometry_poses/dataset/poses/00.txt");
	myfile.open("C:/Matlab/relativeposes_on_matches.csv");
	myfile2.open("C:/Matlab/relativeposes_on_matches2.csv");
	sprintf_s(imagepath, "C:/Matlab/datasets/kitti/00/image_0");
	return 1;
}

static void convertFromHom(Mat tri, vector<Point3f> *points3d)
{
	points3d->clear();
	for (int i = 0; i < tri.cols; i++) {
		float s = tri.at<float>(3, i);
		float x = tri.at<float>(0, i) / s;
		float y = tri.at<float>(1, i) / s;
		float z = tri.at<float>(2, i) / s;
		points3d->push_back(Point3f(x, y, z));
	}
}

Mat triangulate_Linear_LS(Mat mat_P_l, Mat mat_P_r, Mat warped_back_l, Mat warped_back_r)
{
	Mat A(4, 3, CV_64FC1), b(4, 1, CV_64FC1), X(3, 1, CV_64FC1), X_homogeneous(4, 1, CV_64FC1), W(1, 1, CV_64FC1);
	W.at<double>(0, 0) = 1.0;
	A.at<double>(0, 0) = (warped_back_l.at<double>(0, 0) / warped_back_l.at<double>(2, 0))*mat_P_l.at<double>(2, 0) - mat_P_l.at<double>(0, 0);
	A.at<double>(0, 1) = (warped_back_l.at<double>(0, 0) / warped_back_l.at<double>(2, 0))*mat_P_l.at<double>(2, 1) - mat_P_l.at<double>(0, 1);
	A.at<double>(0, 2) = (warped_back_l.at<double>(0, 0) / warped_back_l.at<double>(2, 0))*mat_P_l.at<double>(2, 2) - mat_P_l.at<double>(0, 2);
	A.at<double>(1, 0) = (warped_back_l.at<double>(1, 0) / warped_back_l.at<double>(2, 0))*mat_P_l.at<double>(2, 0) - mat_P_l.at<double>(1, 0);
	A.at<double>(1, 1) = (warped_back_l.at<double>(1, 0) / warped_back_l.at<double>(2, 0))*mat_P_l.at<double>(2, 1) - mat_P_l.at<double>(1, 1);
	A.at<double>(1, 2) = (warped_back_l.at<double>(1, 0) / warped_back_l.at<double>(2, 0))*mat_P_l.at<double>(2, 2) - mat_P_l.at<double>(1, 2);
	A.at<double>(2, 0) = (warped_back_r.at<double>(0, 0) / warped_back_r.at<double>(2, 0))*mat_P_r.at<double>(2, 0) - mat_P_r.at<double>(0, 0);
	A.at<double>(2, 1) = (warped_back_r.at<double>(0, 0) / warped_back_r.at<double>(2, 0))*mat_P_r.at<double>(2, 1) - mat_P_r.at<double>(0, 1);
	A.at<double>(2, 2) = (warped_back_r.at<double>(0, 0) / warped_back_r.at<double>(2, 0))*mat_P_r.at<double>(2, 2) - mat_P_r.at<double>(0, 2);
	A.at<double>(3, 0) = (warped_back_r.at<double>(1, 0) / warped_back_r.at<double>(2, 0))*mat_P_r.at<double>(2, 0) - mat_P_r.at<double>(1, 0);
	A.at<double>(3, 1) = (warped_back_r.at<double>(1, 0) / warped_back_r.at<double>(2, 0))*mat_P_r.at<double>(2, 1) - mat_P_r.at<double>(1, 1);
	A.at<double>(3, 2) = (warped_back_r.at<double>(1, 0) / warped_back_r.at<double>(2, 0))*mat_P_r.at<double>(2, 2) - mat_P_r.at<double>(1, 2);
	b.at<double>(0, 0) = -((warped_back_l.at<double>(0, 0) / warped_back_l.at<double>(2, 0))*mat_P_l.at<double>(2, 3) - mat_P_l.at<double>(0, 3));
	b.at<double>(1, 0) = -((warped_back_l.at<double>(1, 0) / warped_back_l.at<double>(2, 0))*mat_P_l.at<double>(2, 3) - mat_P_l.at<double>(1, 3));
	b.at<double>(2, 0) = -((warped_back_r.at<double>(0, 0) / warped_back_r.at<double>(2, 0))*mat_P_r.at<double>(2, 3) - mat_P_r.at<double>(0, 3));
	b.at<double>(3, 0) = -((warped_back_r.at<double>(1, 0) / warped_back_r.at<double>(2, 0))*mat_P_r.at<double>(2, 3) - mat_P_r.at<double>(1, 3));
	solve(A, b, X, DECOMP_SVD);
	vconcat(X, W, X_homogeneous);
	return X_homogeneous;
}

int test_stereo_pnp_relative_pose_using_matches_matching()
{
	char path[100];
	sprintf_s(path, "%s/%06d.png", imagepath, 1);
	Mat imgleft_b = imread(path, IMREAD_GRAYSCALE);
	//resize(imgleft_a, imgleft_a, Size(620, 188));
	Mat combined = Mat(imgleft_b.rows * 2, imgleft_b.cols, imgleft_b.type());
	/// Create a window
	viz::Viz3d myWindow("Coordinate Frame");
	/// Add coordinate axes
	myWindow.showWidget("Coordinate Widget", viz::WCoordinateSystem());

	char myfile_path[200];
	long filetime = std::time(0);
	sprintf_s(myfile_path, "C:/Matlab/rel_on_matchfile_%d.csv", filetime);
	ofstream myfile;
	myfile.open(myfile_path);

	for (int i = 0; i < num_matches; i++)
	{
		int now = va[i];
		int match = vb[i];
		sprintf_s(path, "%s/%06d.png", imagepath, now);
		imgleft_b = imread(path, IMREAD_GRAYSCALE);
		//resize(imga, imga, Size(620, 188));
		sprintf_s(path, "%s/%06d.png", imagepath, match);
		//sprintf_s(path, "C:/Matlab/datasets/kitti/00/image_1/%06d.png", now);
		Mat imgleft_a = imread(path, IMREAD_GRAYSCALE);
		//resize(imgb, imgb, Size(620, 188));
		sprintf_s(path, "C:/Matlab/datasets/kitti/00/image_1/%06d.png", match);
		Mat imgright_a = imread(path, IMREAD_GRAYSCALE);
		//resize(imgc, imgc, Size(620, 188));
		//lib_combine_two_images(imga, imgb, combined);

		Mat poseb;
		camMats[match].copyTo(poseb);
		Mat row = Mat::zeros(1, 4, CV_64F);
		row.at<double>(0, 3) = 1;
		poseb.push_back(row);
		Mat posb;
		poseb.col(3).copyTo(posb);
		//cout << posb << endl;

		Mat posea;
		camMats[now].copyTo(posea);
		row = Mat::zeros(1, 4, CV_64F);
		row.at<double>(0, 3) = 1;
		posea.push_back(row);
		Mat posa;
		posea.col(3).copyTo(posa);
		//cout << posa << endl;
		Mat relative = posea.inv()*posb;
		Point3f rel = Point3f(relative.at<double>(0, 0), relative.at<double>(1, 0), relative.at<double>(2, 0));
		//cout << relative << endl;

		vector<Point3f> points3d;
		Mat allColors(0, 0, CV_8UC3);
		vector<KeyPoint> kp1;
		vector<KeyPoint> kp2;
		vector<KeyPoint> kp3;
		Mat desc1;
		Mat desc2;
		Mat desc3;

		double P0[3 * 4] = { 7.188560000000e+02, 0.000000000000e+00, 6.071928000000e+02, 0.000000000000e+00,
			0.000000000000e+00, 7.188560000000e+02, 1.852157000000e+02, 0.000000000000e+00,
			0.000000000000e+00, 0.000000000000e+00, 1.000000000000e+00, 0.000000000000e+00 };
		double P1[3 * 4] = { 7.188560000000e+02, 0.000000000000e+00, 6.071928000000e+02, -3.861448000000e+02,
			0.000000000000e+00, 7.188560000000e+02, 1.852157000000e+02, 0.000000000000e+00,
			0.000000000000e+00, 0.000000000000e+00, 1.000000000000e+00, 0.000000000000e+00 };

		Mat camleft = Mat(3, 4, CV_64F, &P0);
		Mat camright = Mat(3, 4, CV_64F, &P1);
		Mat K, rot, trans;
		decomposeProjectionMatrix(camright, K, rot, trans);

		grid_params gp;
		gp.detector = ORB::create(2000);
		gp.cellcols = 16;
		gp.cellrows = 16;
		gp.percell = 40;
		gp.maxdist = 2;
		test_triangulate_linear(camleft, camright, imgleft_a, imgright_a, kp1, kp2, desc1, desc2, gp, points3d, allColors, 0);

		lib_extract_better_features(detector, imgleft_b, kp3, 31, 16, 6, 20, 5);
		detector->compute(imgleft_b, kp3, desc3);
		vector<DMatch> matches1;
		lib_match_descriptors(desc3, desc1, matches1);
		lib_filter_matches(kp3, kp1, matches1);

		vector<Point2f> imgapoints;
		KeyPoint::convert(kp3, imgapoints);
		Mat rvec, tvec;
		Mat pnpmask;
		vector<Point3f> poi3dfor3;
		for (int j = 0; j < matches1.size(); j++)
		{
			Point3f p3f = points3d[matches1[j].trainIdx];
			poi3dfor3.push_back(p3f);
		}
		solvePnPRansac(poi3dfor3, imgapoints, K, noArray(), rvec, tvec, false, 100, 6.0, 0.9899999, pnpmask, CV_P3P); //CV_P3P
		cout << "rel: " << rel << "\n";
		cout << "rvec: " << rvec << "\n";
		cout << "tvec: " << tvec << "\n";


		char text[300];
		sprintf_s(text, "%.4f,%.4f,%.4f,%d,%d\n", tvec.at<double>(0), tvec.at<double>(1), tvec.at<double>(2), match, i);
		myfile << text;

		vector<KeyPoint> kpfa;
		vector<KeyPoint> kpfb;
		cout<< "pnpmask type: " <<lib_type2str(pnpmask.type()) << endl;
		for (int j = 0; j < pnpmask.rows; j++)
		{
			int idx = pnpmask.at<int>(j, 0);
			kpfa.push_back(kp3[idx]);
			kpfb.push_back(kp1[idx]);
		}

		Mat lines;
		//computeCorrespondEpilines(testpoints, 1, F, lines);
		//cout << lines << endl;
		/*for (int k = 0; k < lines.rows; k++)
		{
			Vec3f vect = lines.at<Vec3f>(k);
			float a = vect.val[0];
			float b = vect.val[1];
			float c = vect.val[2];
			//line(imgc, Point(0, -(c / b)), Point(imgc.cols, (imgc.cols*a + c) / (-b)),Scalar(0,0,0));
			
		}*/
		//line(imgc,)
		Mat testt(poi3dfor3);
		viz::WCloud cws(testt, allColors);
		cws.setRenderingProperty(viz::POINT_SIZE, 3);
		myWindow.showWidget("CloudWidget2", cws);
		/// Add coordinate axes
		myWindow.showWidget("Coordinate Widget", viz::WCoordinateSystem());
		/// Let's assume camera has the following properties
		Vec3d cam_pos(1.0f, 0.0f, 0.0f), cam_focal_point(1, 0, 1), cam_y_dir(0.0f, 1.0f, 0.0f);
		/// We can get the pose of the cam using makeCameraPose
		Affine3f cam_pose = viz::makeCameraPose(cam_pos, cam_focal_point, cam_y_dir);
		viz::WCameraPosition cpw(0.5); // Coordinate axes
		viz::WCameraPosition cpw_frustum(Vec2f(0.889484, 0.523599)); // Camera frustum
		myWindow.showWidget("CPW", cpw, cam_pose);
		myWindow.showWidget("CPW_FRUSTUM", cpw_frustum, cam_pose);
		myWindow.spinOnce(1, true);

		Mat imga, imgb, imgc;
		//drawKeypoints(imgb, kp1, imgb);
		drawKeypoints(imgleft_a, kpfa, imga);
		drawKeypoints(imgleft_b, kpfb, imgb);
		
		//drawKeypoints(imgc, kp2, imgc);
		//lib_combine_two_images(imga, imgb, combined);
		imshow("Match_a", imga);
		//imshow("Match_c", imgc);
		imshow("Match_b", imgb);
		waitKey(0);
	}
	return 1;
}

