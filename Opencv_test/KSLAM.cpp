#include <stdio.h>
#include <iostream>
#include <fstream>
#include "opencv2/core.hpp"
#include "opencv2/core/utility.hpp"
#include "opencv2/features2d.hpp"
#include "opencv2/xfeatures2d.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/calib3d.hpp"
#include "opencv2/video/tracking.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include "SeqSLAM.h"
#include "mylib.h"
#include "KSLAM.h"

using namespace cv;
using namespace std;

int show_img_normalized(Mat img)
{
	double min;
	double max;
	minMaxIdx(img, &min, &max);
	Mat adjMap;
	convertScaleAbs(img, adjMap, 255 / max);
	imshow("normimg", adjMap);
	return 0;
}
// verion one, does not know the start and end of a sequence
int kslam_seq_search_seq(Mat diffmat, vector<double> &S, Mat sumimg, double uu)
{
	int backlook = diffmat.cols;
	Mat tosearch;
	diffmat.copyTo(tosearch);
	double min, max;
	minMaxIdx(tosearch, &min, &max);
	tosearch = tosearch - min;
	double vel_dt = 4;
	double vel_min = 4;
	double vel_max = backlook;
	int starta, startb;
	for (int i = 0; i <diffmat.rows - vel_max; i++)
	{
		double minsum = 1000000000000000000;
		for (int j = i + vel_min; j <= i + vel_max; j += vel_dt)
		{
			double dy = (j - i) / (double)backlook;
			double y = i;
			double sum = 0;
			for (int k = 0; k < backlook; k++)
			{
				sum += tosearch.at<double>((int)y, k);
				y += dy;
			}
			if (sum < minsum && sum > 0)
			{
				minsum = sum;
				starta = i;
				startb = j;
			}
		}
		S.push_back(minsum / backlook);
	}

	return 1;
}
// verion one, does not know the start and end of a sequence
int kslam_seqslam(Mat diffmat, vector<Point2f> &S, Mat sumimg, double uu)
{
	int backlook = diffmat.cols;

	Mat tosearch;
	diffmat.copyTo(tosearch);
	double min, max;
	minMaxIdx(tosearch, &min, &max);
	tosearch = tosearch - min;
	double vel_min = 0.6;
	double vel_step = 0.2;
	double vel_max = 1.4;
	double stop = vel_max * diffmat.cols;
	int starta;
	vector<double> sums;
	vector<double> mindys;
	for (int i = 0; i <tosearch.rows - stop; i++)
	{
		double minsum = 1000000000000000000;
		double mindy = 0;
		for (double dy = vel_min; dy <= vel_max; dy += vel_step)
		{
			double y = i;
			double sum = 0;
			for (int k = 0; k < tosearch.cols; k++)
			{
				sum += tosearch.at<double>((int)y, k);
				y += dy;
			}
			if (sum < minsum && sum > 0)
			{
				minsum = sum;
				starta = i;
				mindy = dy;
			}
		}
		mindys.push_back(mindy);
		sums.push_back(minsum);
	}
	double average = 0;
	Mat m(sums.size(), 1, CV_64F);
	int minidxx = 0;
	for (int i = 0; i < sums.size(); i++)
	{
		m.at<double>(i, 0) = sums[i];
	}
	double diffmin, diffmax;
	int minidx[2] = { 0, 0 }, maxidx[2] = { 0, 0 };
	minMaxIdx(m, &diffmin, &diffmax, minidx, maxidx);
	minidxx = minidx[0];
	m = m - diffmin;
	m = m / (diffmax - diffmin);

	Scalar s = mean(m);
	average = s(0);
	double y = minidxx;
	for (int k = 0; k < tosearch.cols; k++)
	{
		y += mindys[minidxx];
		Point2f pp = Point2f(k, (int)y);
		tosearch.at<double>(y, k) = 0;
		S.push_back(pp);
	}
	
	//imshow("tosearch", tosearch);

	int minnn = m.rows < sumimg.cols - 1 ? m.rows : sumimg.cols - 1;
	line(sumimg, Point(0, 98), Point(sumimg.cols - 1, 98), Scalar(150));
	line(sumimg, Point(0, 302), Point(sumimg.cols - 1, 302), Scalar(150));
	//line(sumimg, Point(0, 100 + average * 200), Point(sumimg.cols - 1, 100 + average * 200), Scalar(150));
	//line(sumimg, Point(0, 100 + uu * 200), Point(sumimg.cols - 1, 100 + uu * 200), Scalar(50));
	for (int i = 1; i < minnn; i++)
	{
		double f = m.at<double>(i, 0);
		f = f < 0 ? 0 : (f>1 ? 1 : f);
		if (f != f) f = 1;
		sumimg.at<double>(100 +f * 200, i) = 255;
	}

	//imshow("testst", sumimg);
	if (waitKey(10) == 'a')
	{
		ofstream finalf;
		finalf.open("C:/Matlab/malag/seqslam_graph.txt");
		char text[200];
		for (int i = 0; i < m.rows; i++)
		{
			sprintf_s(text, "%.4f\n", m.at<double>(i, 0));
			finalf << text;
		}
		finalf.flush();
		finalf.close();
	}


	int Rwin = 15;
	int start = minidxx - Rwin >= 0 ? minidxx - Rwin : 0;
	int end = minidxx + Rwin < m.rows ? minidxx + Rwin : m.rows - 1;
	for (int i = start; i < end; i++)
	{
		m.at<double>(i, 0) = 0.9;
	}

	minMaxIdx(m, &diffmin, &diffmax, minidx, maxidx);
	if (diffmin > uu)
	{
		return 1;
	}
	return 0;
}

int kslam_check_seq_pyramid(Mat diffmat, vector<Point2f> &S, Mat sumimg, double uu)
{
	Mat tofix;
	diffmat.copyTo(tofix);
	Mat idxs = Mat::zeros(tofix.rows,tofix.cols,CV_32S);
	for (int i = tofix.cols - 2; i >= 0; i--)
	{
		for (int j = 0; j < tofix.rows - ((tofix.cols - 1) - i); j++)
		{
			double a = tofix.at<double>(j, i + 1);
			double b = tofix.at<double>(j + 1, i + 1);
			double c = tofix.at<double>(j, i);
			double d = c + (a < b ? a : b);
			idxs.at<int>(j, i) = a < b ? j : j + 1;
			tofix.at<double>(j, i) = d;
			double e = tofix.at<double>(j, i);
		}
	}
	Mat therow = tofix.col(0).rowRange(0,tofix.rows- tofix.cols);
	
	double diffmin, diffmax;
	int minidx[2] = { 0, 0 }, maxidx[2] = { 0, 0 };
	minMaxIdx(therow, &diffmin, &diffmax, minidx, maxidx);
	int minindex = minidx[0];
	int nowidx = minindex;
	for (int i = 1; i < tofix.cols; i++)
	{
		Point2f pp = Point2f(0, nowidx);
		nowidx = idxs.at<int>(nowidx, i);
		S.push_back(pp);
	}
	
	therow = (therow-diffmin) / (diffmax-diffmin);
	
	double average = 0;
	for (int i = 0; i < therow.rows; i++)
	{
		average += therow.at<double>(i, 0);
	}
	average = average / therow.rows;
	int Rwin = tofix.cols + 15;
	int start = minidx[0] - Rwin >= 0 ? minidx[0] - Rwin : 0;
	int end = minidx[0] + Rwin < therow.rows ? minidx[0] + Rwin : therow.rows - 1;
	for (int i = start; i < end; i++)
	{
		therow.at<double>(i, 0) = 0.9;
	}
	minMaxIdx(therow, &diffmin, &diffmax, minidx, maxidx);
	if (0)
	{
		int minnn = therow.rows < sumimg.cols-1 ? therow.rows : sumimg.cols-1;
		line(sumimg, Point(0, 100), Point(sumimg.cols - 1, 100), Scalar(150));
		line(sumimg, Point(0, 300), Point(sumimg.cols - 1, 300), Scalar(150));
		line(sumimg, Point(0, 100 + average * 200), Point(sumimg.cols - 1, 100 + average * 200), Scalar(150));
		line(sumimg, Point(0, 100 + 0.65 * 200), Point(sumimg.cols - 1, 100 + 0.65 * 200), Scalar(50));
		for (int i = 1; i < minnn; i++)
		{
			sumimg.at<double>(100 + therow.at<double>(i,0) * 200, i) = 255;
		}
	}
	
	tofix.release();
	if (average > uu) // diffmin
	{
		return 1;
	}
	return 0;
}

// verion one, does not know the start and end of a sequence
int kslam_check_seq_custom(Mat diffmat, vector<Point2f> &S, Mat sumimg)
{
	int backlook = diffmat.cols;

	Mat tosearch;
	diffmat.copyTo(tosearch);
	double diffmin, diffmax;
	minMaxIdx(tosearch, &diffmin, &diffmax);
	tosearch = tosearch - diffmin;
	double bestsum = 999999999;
	int bestidx = 0;
	vector<Point2f> bestp;
	vector<double> allsums;
	int iddddd = 0;
	for (int i = 0; i <tosearch.rows - backlook*2; i+=3)
	{
		vector<Point2f> tempp;
		double sum = tosearch.at<double>(i, 0);
		int currsrow = i;
		int outoff = 0;
		Mat toshow;
		tempp.push_back(Point2f(0, i));
		//diffmat.copyTo(toshow);
		for (int j = 1; j < tosearch.cols; j++)
		{
			if (currsrow + 4 > tosearch.rows - backlook*2){
				outoff = 1;
				break;
			}
			Mat coll = tosearch.col(j).rowRange(currsrow, currsrow + 3);
			int minidx[2] = { 0, 0 }, maxidx[2] = { 0, 0 };
			double tempmin, tempmax;
			minMaxIdx(coll, &tempmin, &tempmax, minidx, maxidx);
			sum += tempmin;
			currsrow = currsrow + minidx[0];
			tempp.push_back(Point2f(currsrow, i));
			//toshow.at<double>(currsrow, j) = 0;
		}
		if (!outoff)
		{
			iddddd++;
			allsums.push_back(sum);
		}
		if (!outoff && sum < bestsum) {
			bestsum = sum;
			bestp = tempp;
			bestidx = iddddd-1;
		}
		//imshow("testing", toshow);
		//waitKey(0);
	}
	if (allsums.size() == 0){
		return 0;
	}
	double totalmax = 0;
	double totalmin = 99999999999;
	for (int i = 0; i < allsums.size(); i++)
	{
		if (allsums[i] > totalmax)
		{
			totalmax = allsums[i];
		}
		if (allsums[i] < totalmin)
		{
			totalmin = allsums[i];
		}
	}
	S = vector<Point2f>(bestp);
	double average = 0;
	double fmax = 0;
	double fmin =  100000;
	for (int i = 0; i < allsums.size(); i++)
	{
		allsums[i] = (allsums[i] - totalmin) / totalmax;

		if (allsums[i] > fmax)
		{
			fmax = allsums[i];
		}
		if (allsums[i] < fmin)
		{
			fmin = allsums[i];
		}
		average += allsums[i];
	}
	average = average / allsums.size();

	if (allsums[bestidx] < average*0.15 && average > 0.6){
		return 1;
	}

	/*int minnn = allsums.size() < sumimg.cols ? allsums.size() : sumimg.cols;
	for (int i = 0; i < minnn; i++)
	{
		sumimg.at<double>(allsums[i] * 200, i) = 255;
	}*/
	return 0;
}


// takes patchnormalized image and return comparasion vector.
int KSLAM::generate_vect(Mat img, Mat& vect)
{
	//int N = prevImgs.size();
	int N = vect.rows;
	if (N > 1)
	{
		Mat vv = Mat::zeros(N, 1, CV_64F);
		for (int i = 0; i < N; i++)
		{
			Mat m = prev_imgs[i];
			double score = compareImages(img, m);
			int roff, coff;
			//double score = seq_comp_smart(img, m, 0, 2, &coff, &roff);
			vv.at<double>(i) = score;
		}
		vv.copyTo(vect);
	}
	return 1;
}

int KSLAM::show_system_image()
{
	double min;
	double max;
	minMaxIdx(system, &min, &max);
	Mat adjMap;
	convertScaleAbs(system, adjMap, 255 / max);

	imshow("system", adjMap);
	/*vector<Point2f> asd;
	Mat imm = system.rowRange(0, prev_imgs.size() - look_back * 2).colRange(prev_imgs.size() - 40 - look_back, prev_imgs.size() - look_back);
	Mat sumimage = Mat::zeros(400, 1200, CV_64F);
	kslam_check_seq_custom(imm, asd, sumimage);
	imshow("system", adjMap.rowRange(0, prev_imgs.size() - look_back).colRange(prev_imgs.size() - 40 - look_back, prev_imgs.size() - look_back));*/
	return 0;
}
int KSLAM::generate_diff_mat(Mat img)
{
	if (prev_imgs.size() <= look_back)
	{
		return -1;
	}
	int N = prev_imgs.size();
	Mat vect = system.col(N - look_back).rowRange(0, N - look_back);
	generate_vect(img, vect);
	double min, max;
	Point min_loc, max_loc;
	minMaxLoc(vect, &min, &max, &min_loc, &max_loc);
	return min_loc.y;
}

int KSLAM::step_seq(Mat currimg)
{
	Mat normalizedimg;
	preprocessImage(currimg, normalizedimg);

	// check if image is similar to previous image.
	image_counter++;
	if (prev_imgs.size() > 0)
	{
		Mat m = prev_imgs[cirrent_img_after_filter - 1];
		if (compareImages(normalizedimg, m)<20){ return 0; }
	}
	int match = generate_diff_mat(normalizedimg);
	prev_imgs_idx.push_back(image_counter);

	if (cirrent_img_after_filter - look_back > min_frames_for_seq) 
	{
		vector<double> S;
		Mat diff = system.colRange(cirrent_img_after_filter - look_back - min_frames_for_seq, cirrent_img_after_filter - look_back).
			rowRange(0, cirrent_img_after_filter - look_back);
		show_img_normalized(diff);
		//kslam_seq_search_seq(diff,S);
	}

	prev_imgs.push_back(normalizedimg);
	cirrent_img_after_filter++;
}

int KSLAM::pre_train_step(Mat currimg)
{
	Mat normalizedimg;
	preprocessImage(currimg, normalizedimg);
	image_counter++;
	prev_imgs.push_back(normalizedimg);
	cirrent_img_after_filter++;
	return 1;
}
int KSLAM::add_normalized_image(Mat currimg)
{
	image_counter++;
	prev_imgs.push_back(currimg);
	cirrent_img_after_filter++;
	return 1;
}

int KSLAM::find_best_from_train(Mat currimg)
{
	Mat normalizedimg;
	preprocessImage(currimg, normalizedimg);
	Mat vect(prev_imgs.size(),1, CV_64F);
	generate_vect(normalizedimg, vect);
	double min, max;
	Point min_loc, max_loc;
	int end = prev_find + 20 <= prev_imgs.size() ? prev_find + 20 : prev_imgs.size();
	int start = prev_find - 1 >= 0 ? prev_find - 1 : prev_find;
	Mat cut = vect.rowRange(start, end);
	minMaxLoc(cut, &min, &max, &min_loc, &max_loc);
	int temp = prev_find;
	prev_find = temp + min_loc.y;
	return temp + min_loc.y;
}

static Mat sumimage = Mat::zeros(400, 1200, CV_64F);
int KSLAM::step_new(Mat currimg)
{

	// remove high values, not nessesary?
	//threshold(currimg, currimg, 253, 255, 4);

	Mat normalizedimg;
	//preprocessImage(currimg, normalizedimg);
	currimg.copyTo(normalizedimg);

	double score = 100000000000;
	// check if image is similar to previous image.
	if (cirrent_img_after_filter > 0)
	{
		Mat m = prev_imgs[cirrent_img_after_filter - 1];
		score = compareImages(normalizedimg, m);
	}
	//image is to similar, skipping it
	if (score < 12) { 
		return -2; 
	}
	image_counter++;
	int match = generate_diff_mat(normalizedimg);
	prev_imgs_idx.push_back(image_counter);

	if (prev_imgs.size() > look_back + 120)
	{
		Mat imm = system.rowRange(0, prev_imgs.size() - look_back).colRange(prev_imgs.size() -  min_frames_for_seq - look_back, prev_imgs.size() - look_back);
		vector<Point2f> asd;
		sumimage.setTo(Scalar(0));
		//Mat sumimage;
		//if (kslam_check_seq_custom(imm, asd, sumimage)){
		if (prev_imgs.size()>0){
			double min, max;
			minMaxIdx(system, &min, &max);
			//Mat adjMap;
			//convertScaleAbs(imm, adjMap, 255 / max);
			//imshow("diffmat", adjMap);
			int toadd = 1;
			if (prev_imgs.size() % toadd == 0 && kslam_seqslam(imm, asd, sumimage, uu)) {
				//waitKey(0);

				for (int i = asd.size() - toadd; i < asd.size(); i++)
				{
					singleMatch *sm = new singleMatch(image_counter - toadd + (i - (asd.size() - toadd)), asd[i].y, false);
					correct_matches->push_back(*sm);
				}
				prev_imgs.push_back(normalizedimg);
				cirrent_img_after_filter++;
				//imshow("thetesting", sumimage);
				//waitKey(1);
				return asd[asd.size()-1].y;
			}
			//imshow("thetesting", sumimage);
			//waitKey(1);
		}
	}
	prev_imgs.push_back(normalizedimg);
	cirrent_img_after_filter++;
	return -1;
}
int KSLAM::step_seqslam(Mat currimg)
{
	Mat normalizedimg;
	preprocessImage(currimg, normalizedimg);
	//currimg.copyTo(normalizedimg);
	double score = 100000000000;
	// check if image is similar to previous image.
	if (prev_imgs.size() > 0)
	{
		Mat m = prev_imgs[prev_imgs.size() - 1];
		score = compareImages(normalizedimg, m);
	}
	//image is to similar, skipping it
	if (score < 15) {
		//return -2;
	}
	int match = generate_diff_mat(normalizedimg);

	if (prev_imgs.size() > look_back + 100 + min_frames_for_seq)
	{
		Mat imm = system.rowRange(0, prev_imgs.size() - look_back - min_frames_for_seq).colRange(prev_imgs.size() - min_frames_for_seq - look_back, prev_imgs.size() - look_back);
		//imshow("immm", imm);
		//waitKey(0);
		vector<Point2f> asd;
		sumimage.setTo(Scalar(0));
		//Mat sumimage;
		//if (kslam_check_seq_custom(imm, asd, sumimage)){
		if (prev_imgs.size()>0){
			double min, max;
			minMaxIdx(system, &min, &max);
			//Mat adjMap;
			//convertScaleAbs(imm, adjMap, 255 / max);
			//imshow("diffmat", adjMap);
			//if (kslam_seqslam(imm, asd, sumimage, uu)) {
			if (kslam_check_seq_pyramid(imm, asd, sumimage, uu)){
				//waitKey(0);

				/*for (int i = asd.size(); i < asd.size(); i++)
				{
					singleMatch *sm = new singleMatch(prev_imgs.size() - 1 + (i - (asd.size()-1)), asd[i].y, false);
					correct_matches->push_back(*sm);
				}*/
				singleMatch *sm = new singleMatch(prev_imgs.size() - 1, asd[asd.size() - 1].y, false);
				correct_matches->push_back(*sm);
				prev_imgs.push_back(normalizedimg);
				//imshow("thetesting", sumimage);
				//waitKey(0);
				return asd[asd.size() - 1].y;
			}
			if (prev_imgs.size() % 100 == 0){
				//imshow("thetesting", sumimage);
				//waitKey(1);
			}
			//imshow("thetesting", sumimage);
			//waitKey(1);
		}
	}
	prev_imgs.push_back(normalizedimg);
	return -1;
}

int KSLAM::step_custom(Mat currimg)
{
	// remove high values, not nessesary?
	//threshold(currimg, currimg, 253, 255, 4);

	Mat normalizedimg;
	preprocessImage(currimg, normalizedimg);
	//currimg.copyTo(normalizedimg);

	double score = 100000000000;
	// check if image is similar to previous image.
	image_counter++;
	if (cirrent_img_after_filter > 0) 
	{
		Mat m = prev_imgs[cirrent_img_after_filter - 1];
		score = compareImages(normalizedimg, m);
	}
	// image is to similar, skipping it
	//if (score < 20) { return 0; }

	int match = generate_diff_mat(normalizedimg);
	prev_imgs_idx.push_back(image_counter);

	if (0 && prev_imgs.size() > look_back + 300 + min_frames_for_seq){
		Mat imm = system.rowRange(0, prev_imgs.size() - look_back - min_frames_for_seq).colRange(prev_imgs.size() - min_frames_for_seq - look_back, prev_imgs.size() - look_back);
		vector<Point2f> asd;
		//Mat sumimage = Mat::zeros(400, 1200, CV_64F);
		Mat sumimage;
		if (kslam_check_seq_custom(imm, asd, sumimage)){
			//waitKey(0);
		}
		//imshow("thetesting", sumimage);
	}
	singleMatch *sm = NULL;
	int added_a_match = 0;
	if (match >= 0)
	{
		(*best_match_global)[image_counter] = prev_imgs_idx[match];
		double vall = system.at<double>(match, cirrent_img_after_filter - look_back);
		currval = vall;
		int matchdiff = match - prev_best_match_idx;
		if (vall < part_of_seq_threshold || (seq_started && vall < part_of_seq_threshold && matchdiff >= 0 && matchdiff < 5))
		{
			seq_started = 1;
			sm = new singleMatch(image_counter, prev_imgs_idx[match], false);
			current_seq_matches->push_back(*sm);
			current_seq_streak++;
			prev_best_match_idx = match;
			current_seq_without_fail++;
			if (current_seq_without_fail > 5)
			{
				current_seq_fail = 0;
			}
			if (current_seq_streak >= min_frames_for_seq)
			{
				for (int jal = 0; jal < current_seq_matches->size(); jal++)
				{
						singleMatch smold = (*current_seq_matches)[jal];
						sm = new singleMatch(smold.a, smold.b, smold.fail);
						//correct_matches->push_back(*sm);
						added_a_match = 1;
				}
				//current_seq_matches->clear();
				sm = NULL;
			}
		}
		else
		{
			if (seq_started)
			{
				sm = new singleMatch(image_counter, prev_imgs_idx[match],true);
				current_seq_matches->push_back(*sm);
				current_seq_fail++;
				current_seq_without_fail = 0;
			}
			if (current_seq_fail >= max_frames_to_fail) // || (current_seq_fail >= max_frames_to_fail / 2)
			{
				bool firstreal = false;
				if (current_seq_streak >= min_frames_for_seq) 
				{
					for (int jal = current_seq_matches->size() - 1; jal >= 0; jal--)
					{
						if (firstreal) 
						{
							singleMatch smold = (*current_seq_matches)[jal];
							sm = new singleMatch(smold.a, smold.b, smold.fail);
							correct_matches->push_back(*sm);
						}
						else if (!(*current_seq_matches)[jal].fail)
						{
							firstreal = true;
						}
					}
					sm = NULL;
				}
				seq_started = 0;
				current_seq_streak = 0;
				current_seq_fail = 0;
				current_seq_matches->clear();
			}
		}
	}
	prev_imgs.push_back(normalizedimg);
	cirrent_img_after_filter++;

	if (added_a_match){
		return match;
	}
	else{
		return -1;
	}
}