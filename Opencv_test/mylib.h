#ifndef MYLIB_H
#define MYLIB_H

#include <string>
#include "opencv2/core.hpp"
#include "opencv2/core/utility.hpp"
#include "opencv2/features2d.hpp"
#include "opencv2/xfeatures2d.hpp"

struct grid_params
{
	cv::Ptr<cv::ORB> detector; // the detector for the detecting
	int cellcols; // number of columns to split into
	int cellrows;
	int percell; // number of features allowed per cell
	float maxdist; // minmum distance between features
};

// read matches single csv file
int lib_read_matches(std::vector<int>& va, std::vector<int>& vb, char *path);
// check if determinant is more than 0
int lib_check_if_essential_matrix_is_valid(cv::Mat E);
//get relative pose from point matches
int lib_get_relative_pose_from_points(std::vector<cv::Point2f> keypoints1, std::vector<cv::Point2f> keypoints2, 
	float focal, cv::Point2d pp, cv::Mat& R, cv::Mat& t, cv::Mat& mask);
//get relative pose without scale from two images
int lib_get_relative_pose(cv::Ptr<cv::ORB>& detector, cv::Mat& imga, cv::Mat& imgb, cv::Mat& t, cv::Mat& R);
// using bruteforce matcher
int lib_match_descriptors(cv::Mat& desc1, cv::Mat& desc2, std::vector<cv::DMatch>& matches);
// filter out matches
int lib_filter_matches(std::vector<cv::KeyPoint>& kp1, std::vector<cv::KeyPoint>& kp2, std::vector<cv::DMatch>& matches);
// extract features and filter too close ones
//int lib_extract_grid_features(cv::Ptr<cv::ORB> detector, cv::Mat img, std::vector<cv::KeyPoint>& kpt, 
//	int cellx, int celly, int percell, float maxdist);
// grid features with parameter struct
int lib_extract_grid_features_params(cv::Mat img, std::vector<cv::KeyPoint>& kpt, grid_params params);
//EXTRACT FOR Each grid cell makeing more features
int lib_extract_better_features(cv::Ptr<cv::ORB> detector, cv::Mat img, std::vector<cv::KeyPoint>& kpt, 
	int patchsize, int cellcols, int cellrows, int percell, float maxdist);
// read only location from kitti poses
void lib_read_gps_data_kitti(std::vector<cv::Point3d>& scores, char *path);
// read transformation matrices from pose kitti
void lib_read_kitti_poses(std::vector<cv::Mat>& Pss, char *path);
// track and filter points using pyrlk
void lib_feature_tracking(cv::Mat img_1, cv::Mat img_2, std::vector<cv::Point2f>& points1, std::vector<cv::Point2f>& points2, std::vector<uchar>& status);
//combine two images verticaly
int lib_combine_two_images(cv::Mat a, cv::Mat b, cv::Mat& out);
// patch normalize a image
int lib_patch_normalize(cv::Mat& img);
// Fundamental matrix from P
int lib_compute_F_from_P(cv::Mat P1, cv::Mat P2, cv::Mat &F);

void lib_read_times(std::vector<double>& Pss, char *path);

//caculates histogram for grayscale image
cv::Mat lib_get_histogram(cv::Mat img);
cv::Mat lib_draw_histogram(cv::Mat hist);
cv::Mat lib_triangulate_linear_ls(cv::Mat mat_P_l, cv::Mat mat_P_r, cv::Mat warped_back_l, cv::Mat warped_back_r);
std::string lib_type2str(int type);

#endif // MYLIB_H