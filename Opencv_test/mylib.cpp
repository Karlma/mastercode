#include <stdio.h>
#include <iostream>
#include <fstream>
#include "opencv2/core.hpp"
#include "opencv2/core/utility.hpp"
#include "opencv2/features2d.hpp"
#include "opencv2/xfeatures2d.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/calib3d.hpp"
#include "opencv2/video/tracking.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "mylib.h"

using namespace cv;
using namespace std;
using namespace cv::xfeatures2d;


int lib_patch_normalize(cv::Mat& img)
{
	int patchSIze = 8;
	int cols = img.cols;
	int rows = img.rows;

	for (int i = 0; i < cols / patchSIze; i++)
	{
		for (int j = 0; j < rows / patchSIze; j++)
		{
			Mat sub = img(cv::Range(j * patchSIze, (j + 1)*patchSIze),
				cv::Range(i * patchSIze, (i + 1)*patchSIze));
			double min, max;
			minMaxLoc(sub, &min, &max);
			double f = max - min;
			f = 255.0 / f;
			f = f <= 0 ? 1 : f;
			sub = sub - min;
			sub = sub * f;
		}
	}
	return 1;
}

// read kitti 00 matches generated with seqslam
int lib_read_matches(vector<int>& va, vector<int>& vb, char *path)
{
	ifstream infile(path);
	int a, b;
	char c;
	while ((infile >> a >> c >> b) && (c == ','))
	{
		va.push_back(a);
		vb.push_back(b);
	}
	return 1;
}

// check if determinant is greater than 0
int lib_check_if_essential_matrix_is_valid(Mat E)
{
	if (determinant(E) > 1e-07)
	{
		cout << "Invalid E det(E) != 0" << endl;
		return 0;
	}
	return 1;
}

int lib_get_relative_pose_from_points(vector<Point2f> keypoints1, vector<Point2f> keypoints2, float focal, Point2d pp, Mat& R, Mat& t, Mat& mask)
{
	// calculate essential matrix with ransac and 5-point algorithm?
	// TODO add iterations as argument
	Mat E = findEssentialMat(keypoints1, keypoints2, focal, pp, RANSAC, 0.99, 4.0, mask);
	//Mat E = findEssentialMat(good_points1, good_points2, 1, Point2d(0,0), RANSAC, 0.999, 1.0, mask);
	lib_check_if_essential_matrix_is_valid(E);
	return recoverPose(E, keypoints1, keypoints2, R, t, focal, pp, mask);
}

int lib_retain_best_matches(vector<KeyPoint>& kp1, vector<KeyPoint>& kp2, 
							vector<Point2f> pout1, vector<Point2f>& pout2, vector<DMatch>& matches)
{
	for (int i = 0; i < matches.size(); i++)
	{
		Point2f pm1 = kp1[matches[i].queryIdx].pt;
		Point2f pm2 = kp2[matches[i].trainIdx].pt;
		pout1.push_back(pm1);
		pout2.push_back(pm2);
	}
	return 1;
}

int lib_filter_matches(vector<KeyPoint>& kp1, vector<KeyPoint>& kp2, vector<DMatch>& matches)
{
	vector<KeyPoint> new1, new2;
	for (int i = 0; i < matches.size(); i++)
	{
		KeyPoint pm1 = kp1[matches[i].queryIdx];
		KeyPoint pm2 = kp2[matches[i].trainIdx];
		new1.push_back(pm1);
		new2.push_back(pm2);
	}
	kp1 = new1;
	kp2 = new2;
	return 1;
}

int lib_match_descriptors(Mat& desc1, Mat& desc2, vector<DMatch>& matches)
{
	BFMatcher matcher(NORM_HAMMING, true);
	//FlannBasedMatcher matcher2(new flann::LshIndexParams(20, 10, 2));
	//matcher2.match(descriptor1,descriptor2,matches);
	matcher.match(desc1, desc2, matches);
	//matcher.knnMatch(descriptor1, descriptor2, matches);
	return 1;
}

// get relative pose using essential matrix and pyrlk tracking
int lib_get_relative_pose(Ptr<ORB>& detector, Mat& imga, Mat& imgb, Mat& t, Mat& R)
{
	//todo as argument?
	double focal = 718.8560;
	Point2d PP(607.1928, 185.2157);

	vector<KeyPoint> kp1;
	vector<KeyPoint> kp2;
	Mat desc1;
	Mat desc2;
	vector<Point2f> poi1;
	vector<Point2f> poi2;
	lib_extract_better_features(detector, imgb, kp2, 31, 16, 6, 30,5);
	KeyPoint::convert(kp2, poi2, vector<int>());
	vector<uchar> status;
	lib_feature_tracking(imgb, imga, poi2, poi1, status);
	Mat mask;
	int inliers = lib_get_relative_pose_from_points(poi1, poi2, focal, PP, R, t, mask);
	return inliers;
}

// compare two keypoints
bool lib_response_comparator(const KeyPoint& p1, const KeyPoint& p2)
{
	return p1.response > p2.response;
}

// simpler interface for clean code.
int lib_extract_grid_features_params(cv::Mat img, std::vector<cv::KeyPoint>& kpt, grid_params params)
{
	return lib_extract_better_features(params.detector, img, kpt, 31, params.cellcols, params.cellrows, params.percell, params.maxdist);
}

// extract features and take only the best in a grid, 
// and remove points to close to eachother
// insipiered by opencv goodfeaturestotrack function.
int lib_extract_grid_features(Ptr<ORB> detector, Mat img, vector<KeyPoint>& kpt, int cellx, int celly, int percell, float maxdist)
{
	vector<KeyPoint> kpt2;
	//FAST(img, kpt2, 30, true, FastFeatureDetector::TYPE_7_12);
	detector->detect(img, kpt2);
	vector<KeyPoint> filtered;
	float cellw = img.cols / (float)cellx;
	float cellh = img.rows / (float)celly;

	std::sort(kpt2.begin(), kpt2.end(), lib_response_comparator);
	std::vector<std::vector<KeyPoint>> v(cellx*celly, std::vector<KeyPoint>());
	for (int k = 0; k < kpt2.size(); k++)
	{
		KeyPoint key = kpt2[k];
		int xx = key.pt.x / cellw;
		int yy = key.pt.y / cellh;
		int x1 = xx - 1;
		int y1 = yy - 1;
		int x2 = xx + 1;
		int y2 = yy + 1;
		// boundary check
		x1 = std::max(0, x1);
		y1 = std::max(0, y1);
		x2 = std::min(cellx - 1, x2);
		y2 = std::min(celly - 1, y2);
		bool good = true;
		for (int yy1 = y1; yy1 <= y2; yy1++){
			for (int xx1 = x1; xx1 <= x2; xx1++){
				//printf("%f, %f\n", key.pt.x, key.pt.y);
				vector<KeyPoint> temp = v[yy1*cellh + xx1];
				for (int i = 0; i < temp.size(); i++)
				{
					KeyPoint key2 = temp[i];
					float dx = key.pt.x - key2.pt.x;
					float dy = key.pt.y - key2.pt.y;
					if (dx*dx + dy*dy < maxdist*maxdist){
						good = false;
						goto endhere;
					}
				}
			}
		}
	endhere:
		if (good){
			v[yy*cellh + xx].push_back(key);
		}
	}

	for (int k = 0; k < v.size(); k++)
	{
		int s = v[k].size();
		s = s >= percell ? percell : s;
		for (int j = 0; j < s; j++)
		{
			kpt.push_back(v[k][j]);
		}
	}
	return 1;
}

int lib_extract_better_features(Ptr<ORB> detector, Mat img, vector<KeyPoint>& kpt, int patchsize, int cellcols, int cellrows, int percell, float maxdist)
{
	detector->setMaxFeatures(percell + 30);
	vector<KeyPoint> filtered;
	float cellw = img.cols / (float)cellcols;
	float cellh = img.rows / (float)cellrows;
	//1241 x 376
	int wincols = (img.cols - (patchsize * 2)) / cellcols;
	int winrows = (img.rows - (patchsize * 2)) / cellrows;
	//imshow("image", img);
	for (int cols = 0; cols < cellcols; cols++)
	{
		for (int rows = 0; rows < cellrows; rows++)
		{
			Mat window = img.colRange(cols * wincols, (cols + 1) * wincols + patchsize * 2).
					rowRange(rows * winrows, (rows + 1) * winrows + patchsize * 2);
			//imshow("window", window);
			//waitKey(0);
			vector<KeyPoint> kpt2temp;
			detector->detect(window, kpt2temp);
			int th = detector->getFastThreshold();
			Mat wikey;
			//drawKeypoints(window, kpt2temp, wikey);

			//imshow("window", wikey);
			//if (kpt2temp.size() < percell){
			//	detector->setFastThreshold(th-9);
			//	detector->detect(window, kpt2temp);
			//	detector->setFastThreshold(th);
				//drawKeypoints(window, kpt2temp, wikey);
				//imshow("window", wikey);
				//waitKey(0);
			//}
			//printf("%f, %f\n", key.pt.x, key.pt.y);
			sort(kpt2temp.begin(), kpt2temp.end(), lib_response_comparator);
			int ss = 0;
			for (int i = 0; i < kpt2temp.size(); i++)
			{
				KeyPoint key = kpt2temp[i];
				bool good = true;
				for (int j = i+1; j < kpt2temp.size(); j++)
				{
					KeyPoint key2 = kpt2temp[j];
					float dx = key.pt.x - key2.pt.x;
					float dy = key.pt.y - key2.pt.y;
					if (dx*dx + dy*dy < maxdist*maxdist){
						good = false;
						break;
					}
				}
				if (good) {
					ss++;
					KeyPoint newk = KeyPoint(key);
					newk.pt.x = newk.pt.x + cols * wincols;
					newk.pt.y = newk.pt.y + rows * winrows;
					kpt.push_back(newk);
					if (ss == percell){
						break;
					}
				}
			}
		}
	}
	return kpt.size();
}

Mat lib_get_histogram(Mat img)
{
	Mat hist;
	int hist_size = 256;
	float range[] = { 0, 256 };
	const float *histRange = { range };
	int channels = 0;
	calcHist(&img, 1, &channels, noArray(), hist, 1, &hist_size, &histRange);
	return hist;
}
Mat lib_draw_histogram(Mat hist)
{
	double max;
	minMaxLoc(hist, 0, &max);
	Mat histImg = Mat::zeros(256, 256, CV_8UC3);
	for (int h = 0; h < hist.rows; h++){
		float val = hist.at<float>(h, 0);
		val = val / max;
		val = val * 255;
		rectangle(histImg, Point(h, 255), Point(h, val), Scalar::all(255), CV_FILLED);
	}
	return histImg;
}


int lib_estimate_trifocal_tensor(vector<Point2f> keypoints1, vector<Point2f> keypoints2, vector<Point2f> keypoints3, float focal, Point2d pp, Mat& tri, Mat& mask)
{
	//1) get intrest points in each image (done)
	//2) Compute F for image 1&2 and 2&3 and filter points
	//3) Join the 2 sets into a three-view corespondances.
	//4) RANSAC: repeat N times where N is from 4.5(p121):
	//   a) Select 6 point corespondances, and compute trifocal tensor using algorithm 20.1(p511). one or 3 solutions.
	return 1;
}

double lib_hcon_det(Mat a, Mat b)
{
	Mat c;
	vconcat(a, b, c);
	return determinant(c);
}
int lib_compute_F_from_P(Mat P1, Mat P2, Mat &F)
{
	/*

	X1 = P1([2 3], :);
	X2 = P1([3 1], :);
	X3 = P1([1 2], :);
	Y1 = P2([2 3], :);
	Y2 = P2([3 1], :);
	Y3 = P2([1 2], :);

	F = [det([X1; Y1]) det([X2; Y1]) det([X3; Y1])
		det([X1; Y2]) det([X2; Y2]) det([X3; Y2])
		det([X1; Y3]) det([X2; Y3]) det([X3; Y3])];*/

	Mat X1 = P1.row(1);
	X1.push_back(P1.row(2));
	Mat X2 = P1.row(2);
	X2.push_back(P1.row(0));
	Mat X3 = P1.row(0);
	X3.push_back(P1.row(1));

	Mat Y1 = P2.row(1);
	Y1.push_back(P2.row(2));
	Mat Y2 = P2.row(2);
	Y2.push_back(P2.row(0));
	Mat Y3 = P2.row(0);
	Y3.push_back(P2.row(1));

	F = Mat::zeros(3, 3, CV_64F);
	F.at<double>(0, 0) = lib_hcon_det(X1, Y1);
	F.at<double>(0, 1) = lib_hcon_det(X2, Y1);
	F.at<double>(0, 2) = lib_hcon_det(X3, Y1);

	F.at<double>(1, 0) = lib_hcon_det(X1, Y2);
	F.at<double>(1, 1) = lib_hcon_det(X2, Y2);
	F.at<double>(1, 2) = lib_hcon_det(X3, Y2);

	F.at<double>(2, 0) = lib_hcon_det(X1, Y3);
	F.at<double>(2, 1) = lib_hcon_det(X2, Y3);
	F.at<double>(2, 2) = lib_hcon_det(X3, Y3);
	return 1;
}

// read only position kitt 00
void lib_read_gps_data_kitti(vector<Point3d>& scores, char *path)
{
	string line;
	int i = 0;
	ifstream myfile(path);
	double x = 0, y = 0, z = 0;
	double p = 0;
	if (myfile.is_open())
	{
		while (getline(myfile, line))
		{
			std::istringstream in(line);
			//cout << line << '\n';
			for (int j = 0; j<12; j++)  {
				in >> p;
				if (j == 11) z = p;
				if (j == 7)  y = p;
				if (j == 3)  x = p;
			}
			Point3d p3(x, y, z);
			scores.push_back(p3);
		}
		myfile.close();
	}
	else {
		cout << "Unable to open file";
	}
}

// read poses for kitti 00
void lib_read_kitti_poses(vector<Mat>& Pss, char *path)
{
	string line;
	int i = 0;
	ifstream myfile(path);
	double p = 0;
	if (myfile.is_open())
	{
		while (getline(myfile, line))
		{
			std::istringstream in(line);
			double data[12];
			//cout << line << '\n';
			for (int j = 0; j<12; j++)  {
				in >> p;
				data[j] = p;
			}
			Mat P = Mat(3, 4, CV_64F, &data);
			Pss.push_back(P.clone());
		}
		myfile.close();
	}
	else {
		cout << "Unable to open file";
	}
}
// read times for path length
void lib_read_times(vector<double>& Pss, char *path)
{
	string line;
	int i = 0;
	ifstream myfile(path);
	double p = 0;
	if (myfile.is_open())
	{
		while (getline(myfile, line))
		{
			std::istringstream in(line);
			//cout << line << '\n';
			in >> p;
			Pss.push_back(p);
		}
		myfile.close();
	}
	else {
		cout << "Unable to open file";
	}
}

// taken from odometry
void lib_feature_tracking(Mat img_1, Mat img_2, vector<Point2f>& points1, vector<Point2f>& points2, vector<uchar>& status)
{
	//this function automatically gets rid of points for which tracking fails
	vector<float> err;
	Size winSize = Size(21, 21);
	TermCriteria termcrit = TermCriteria(TermCriteria::COUNT + TermCriteria::EPS, 30, 0.01);
	calcOpticalFlowPyrLK(img_1, img_2, points1, points2, status, err, winSize, 3, termcrit, 0, 0.001);
	//getting rid of points for which the KLT tracking failed or those who have gone outside the frame
	int indexCorrection = 0;
	for (int i = 0; i<status.size(); i++)
	{
		Point2f pt = points2.at(i - indexCorrection);
		if ((status.at(i) == 0) || (pt.x<0) || (pt.y<0))	{
			if ((pt.x<0) || (pt.y<0))	{
				status.at(i) = 0;
			}
			points1.erase(points1.begin() + (i - indexCorrection));
			points2.erase(points2.begin() + (i - indexCorrection));
			indexCorrection++;
		}
	}
}

int lib_combine_two_images(Mat a, Mat b, Mat& out)
{
	a.copyTo(out.colRange(0, a.cols).rowRange(0, a.rows));
	b.copyTo(out.colRange(0, a.cols).rowRange(a.rows, a.rows * 2));
	return 1;
}

Mat lib_triangulate_linear_ls(Mat mat_P_l, Mat mat_P_r, Mat warped_back_l, Mat warped_back_r)
{
	Mat A(4, 3, CV_64FC1), b(4, 1, CV_64FC1), X(3, 1, CV_64FC1), X_homogeneous(4, 1, CV_64FC1), W(1, 1, CV_64FC1);
	W.at<double>(0, 0) = 1.0;
	A.at<double>(0, 0) = (warped_back_l.at<double>(0, 0) / warped_back_l.at<double>(2, 0))*mat_P_l.at<double>(2, 0) - mat_P_l.at<double>(0, 0);
	A.at<double>(0, 1) = (warped_back_l.at<double>(0, 0) / warped_back_l.at<double>(2, 0))*mat_P_l.at<double>(2, 1) - mat_P_l.at<double>(0, 1);
	A.at<double>(0, 2) = (warped_back_l.at<double>(0, 0) / warped_back_l.at<double>(2, 0))*mat_P_l.at<double>(2, 2) - mat_P_l.at<double>(0, 2);
	A.at<double>(1, 0) = (warped_back_l.at<double>(1, 0) / warped_back_l.at<double>(2, 0))*mat_P_l.at<double>(2, 0) - mat_P_l.at<double>(1, 0);
	A.at<double>(1, 1) = (warped_back_l.at<double>(1, 0) / warped_back_l.at<double>(2, 0))*mat_P_l.at<double>(2, 1) - mat_P_l.at<double>(1, 1);
	A.at<double>(1, 2) = (warped_back_l.at<double>(1, 0) / warped_back_l.at<double>(2, 0))*mat_P_l.at<double>(2, 2) - mat_P_l.at<double>(1, 2);
	A.at<double>(2, 0) = (warped_back_r.at<double>(0, 0) / warped_back_r.at<double>(2, 0))*mat_P_r.at<double>(2, 0) - mat_P_r.at<double>(0, 0);
	A.at<double>(2, 1) = (warped_back_r.at<double>(0, 0) / warped_back_r.at<double>(2, 0))*mat_P_r.at<double>(2, 1) - mat_P_r.at<double>(0, 1);
	A.at<double>(2, 2) = (warped_back_r.at<double>(0, 0) / warped_back_r.at<double>(2, 0))*mat_P_r.at<double>(2, 2) - mat_P_r.at<double>(0, 2);
	A.at<double>(3, 0) = (warped_back_r.at<double>(1, 0) / warped_back_r.at<double>(2, 0))*mat_P_r.at<double>(2, 0) - mat_P_r.at<double>(1, 0);
	A.at<double>(3, 1) = (warped_back_r.at<double>(1, 0) / warped_back_r.at<double>(2, 0))*mat_P_r.at<double>(2, 1) - mat_P_r.at<double>(1, 1);
	A.at<double>(3, 2) = (warped_back_r.at<double>(1, 0) / warped_back_r.at<double>(2, 0))*mat_P_r.at<double>(2, 2) - mat_P_r.at<double>(1, 2);
	b.at<double>(0, 0) = -((warped_back_l.at<double>(0, 0) / warped_back_l.at<double>(2, 0))*mat_P_l.at<double>(2, 3) - mat_P_l.at<double>(0, 3));
	b.at<double>(1, 0) = -((warped_back_l.at<double>(1, 0) / warped_back_l.at<double>(2, 0))*mat_P_l.at<double>(2, 3) - mat_P_l.at<double>(1, 3));
	b.at<double>(2, 0) = -((warped_back_r.at<double>(0, 0) / warped_back_r.at<double>(2, 0))*mat_P_r.at<double>(2, 3) - mat_P_r.at<double>(0, 3));
	b.at<double>(3, 0) = -((warped_back_r.at<double>(1, 0) / warped_back_r.at<double>(2, 0))*mat_P_r.at<double>(2, 3) - mat_P_r.at<double>(1, 3));
	solve(A, b, X, DECOMP_SVD);
	vconcat(X, W, X_homogeneous);
	return X_homogeneous;
}


//from stackoverflow
string lib_type2str(int type)
{
	string r;

	uchar depth = type & CV_MAT_DEPTH_MASK;
	uchar chans = 1 + (type >> CV_CN_SHIFT);

	switch (depth) {
	case CV_8U:  r = "8U"; break;
	case CV_8S:  r = "8S"; break;
	case CV_16U: r = "16U"; break;
	case CV_16S: r = "16S"; break;
	case CV_32S: r = "32S"; break;
	case CV_32F: r = "32F"; break;
	case CV_64F: r = "64F"; break;
	default:     r = "User"; break;
	}

	r += "C";
	r += (chans + '0');

	return r;
}