
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <chrono>
#include "opencv2/core.hpp"
#include "opencv2/core/utility.hpp"
#include "opencv2/features2d.hpp"
#include "opencv2/xfeatures2d.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/calib3d.hpp"
#include "opencv2/video/tracking.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "Odometry.h"
#include "mylib.h"

using namespace cv;
using namespace cv::xfeatures2d;
using namespace std;
using namespace std::chrono;

static vector<Mat> positions;
static vector<Mat> rotations;
static vector<Mat> globalPositions;
static vector<Mat> globalPose;
static vector<Mat> seqImgs;
static vector<Mat> globalRotation;

int doOdometryWithMatching()
{
	// image for ploting path
	Mat traj = Mat::zeros(600, 600, CV_8UC3);
	namedWindow("Trajectory", WINDOW_AUTOSIZE);
	double focal = 718.8560;
	Point2d PP(607.1928, 185.2157);
	vector<Point3d> scores;
	lib_read_gps_data_kitti(scores, "C:/Matlab/datasets/kitti/data_odometry_poses/dataset/poses/00.txt");
	vector<KeyPoint> prevKPoints, curKey;
	Mat prevDesc, cDesc;
	char images[100];
	sprintf(images, "C:/Matlab/datasets/kitti/00/image_0/%06d.png", 0);
	Mat previmg = imread(images, IMREAD_GRAYSCALE);
	//resize(previmg, previmg, Size(), 0.5, 0.5);
	//extractGoodFeatures(previmg, prevPoints, prevDesc);
	Ptr<ORB> detector = ORB::create(1500);
	lib_extract_better_features(detector, previmg, prevKPoints, 31, 16, 6, 5, 15);
	detector->compute(previmg, prevKPoints, prevDesc);
	ofstream myfile;
	myfile.open("C:/Matlab/mydata2.csv");

	Mat R = Mat::eye(3, 3, CV_64FC1);
	Mat t = Mat::zeros(3, 1, CV_64FC1);
	positions.push_back(t);
	rotations.push_back(R);
	vector<cv::DMatch> matches;
	for (int i = 1; i < 500; i++)
	{
		high_resolution_clock::time_point t1 = high_resolution_clock::now();
		sprintf(images, "C:/Matlab/datasets/kitti/00/image_0/%06d.png", i);
		Mat cimg = imread(images, IMREAD_GRAYSCALE);
		high_resolution_clock::time_point t2 = high_resolution_clock::now();
		auto duratation = duration_cast<milliseconds>(t2 - t1).count();
		cout << "readimg: " << duratation << "ms\n";
		//resize(cimg, cimg, Size(),0.5,0.5);
		//extractGoodFeatures(cimg, cPoints, cDesc);
		vector<uchar> status;
		curKey.clear();
		t1 = high_resolution_clock::now();
		lib_extract_better_features(detector, cimg, curKey, 31, 16, 6, 6, 10);
		t2 = high_resolution_clock::now();
		duratation = duration_cast<milliseconds>(t2 - t1).count();
		cout << "extract grid: " << duratation << "ms\n";
		detector->compute(cimg, curKey, cDesc);
		t1 = high_resolution_clock::now();
		lib_match_descriptors(prevDesc, cDesc, matches);
		t2 = high_resolution_clock::now();
		duratation = duration_cast<milliseconds>(t2 - t1).count();
		cout << "matching: " << duratation << "ms\n";
		vector<KeyPoint> new1, new2;
		for (int i = 0; i < matches.size(); i++)
		{
			KeyPoint pm1 = prevKPoints[matches[i].queryIdx];
			KeyPoint pm2 = curKey[matches[i].trainIdx];
			new1.push_back(pm1);
			new2.push_back(pm2);
		}
		vector<Point2f> good_points1, good_points2;
		KeyPoint::convert(new1, good_points1);
		KeyPoint::convert(new2, good_points2);

		Mat mask;
		Mat CR, Ct;
		// calculate essential matrix with ransac and 5-point algorithm?
		t1 = high_resolution_clock::now();
		lib_get_relative_pose_from_points(good_points1, good_points2, focal, PP, CR, Ct, mask);
		t2 = high_resolution_clock::now();
		duratation = duration_cast<milliseconds>(t2 - t1).count();
		cout << "pose: " << duratation << "ms\n";

		positions.push_back(Ct);
		rotations.push_back(CR);
		Point3d dist = scores[i] - scores[i - 1];
		double scale = norm(dist);
		printf("scale: %f\n", scale);
		t = t + scale * (R*Ct);
		globalPositions.push_back(t);
		R = CR * R;
		globalRotation.push_back(R);

		int x = int(t.at<double>(0)) + 300;
		int y = int(t.at<double>(2)) + 550;
		circle(traj, Point(x, y), 1, CV_RGB(255, 0, 0), 2);
		rectangle(traj, Point(10, 30), Point(550, 50), CV_RGB(0, 0, 0), CV_FILLED);
		char text[100];
		sprintf(text, "%.4f %.4f %.4f\n", t.at<double>(0), t.at<double>(1), t.at<double>(2));
		myfile << text;
		//sprintf(text, "Coordinates: x = %.2f y = %01f z = %.2f  Points: %d", t.at<double>(0), t.at<double>(1), t.at<double>(2), prevPoints.size());
		printf("%s", text);
		putText(traj, text, Point(10, 50), FONT_HERSHEY_PLAIN, 1, Scalar::all(255), 1, 8);
		imshow("Trajectory", traj);
		//drawKeypoints(cimg, new2, cimg);
		//imshow("image", cimg);
		previmg = cimg.clone();
		prevKPoints = curKey;
		prevDesc = cDesc.clone();
		waitKey(5);
	}
	myfile.close();
	return 1;
}

