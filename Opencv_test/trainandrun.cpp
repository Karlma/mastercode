
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <chrono>
#include <time.h>
#include <ctime>
#include <math.h>
#include "opencv2/core.hpp"
#include "opencv2/core/utility.hpp"
#include "opencv2/features2d.hpp"
#include "opencv2/xfeatures2d.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/calib3d.hpp"
#include "opencv2/video/tracking.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/viz.hpp"
#include "Odometry.h"
#include "mylib.h"
#include "SeqSLAM.h"
#include "KSLAM.h"
#include "Tests.h"
#include "FullSystem1.h"
#include "trainandrun.h"

using namespace cv;
using namespace std;
using namespace std::chrono;

grid_params gp;
Mat camleft;
Mat camright;
Mat K;
Mat rot, trans;
int traincounter = 0;
vector<PreProImg> trained_points;
vector<Mat> trained_normalized_left_images;
vector<Mat> relativePoses;
vector<Mat> relativeRotation;
char savepath[400];
char myfile_path[400];
ofstream myfile;
KSLAM *kslam = new KSLAM();

static double P0[3 * 4] = { 7.188560000000e+02, 0.000000000000e+00, 6.071928000000e+02, 0.000000000000e+00,
							0.000000000000e+00, 7.188560000000e+02, 1.852157000000e+02, 0.000000000000e+00,
							0.000000000000e+00, 0.000000000000e+00, 1.000000000000e+00, 0.000000000000e+00 };
static double P1[3 * 4] = { 7.188560000000e+02, 0.000000000000e+00, 6.071928000000e+02, -3.861448000000e+02,
							0.000000000000e+00, 7.188560000000e+02, 1.852157000000e+02, 0.000000000000e+00,
							0.000000000000e+00, 0.000000000000e+00, 1.000000000000e+00, 0.000000000000e+00 };

static void write(FileStorage& fs, const std::string&, const PreProImg& x)
{
	x.write(fs);
}
static void read(const FileNode& node, PreProImg& x, const PreProImg& default_value = PreProImg()){
	if (node.empty())
		x = default_value;
	else
		x.read(node);
}
// This function will print our custom class to the console
static ostream& operator<<(ostream& out, const PreProImg& m)
{
	out << "{ id = " << m.imgidx << ", ";
	out << "kp1 = " << m.kp1.size() << ", ";
	out << "kp2 = " << m.kp2.size() << "}";
	return out;
}


PreProImg read_one_file(char *path)
{
	FileStorage fs;
	fs.open(path, FileStorage::READ);
	PreProImg ppi;
	fs["MyData"] >> ppi;
	return ppi;
}

// run this first
// path dows not wend with a /
int init_runner(char *trainsavepath)
{
	// grid feature detection parameters
	gp.detector = ORB::create(2000);
	gp.cellcols = 16;
	gp.cellrows = 6;
	gp.percell = 40;
	gp.maxdist = 2;

	// left and right camera matric for kitti dataset
	
	camleft = Mat(3, 4, CV_64F, &P0);
	camright = Mat(3, 4, CV_64F, &P1);
	
	decomposeProjectionMatrix(camleft, K, rot, trans);

	sprintf_s(savepath, "%s", trainsavepath);
	long filetime = std::time(0);
	sprintf_s(myfile_path, "C:/Matlab/pnp_odometry/fullruntest_%d.csv", filetime);
	traincounter = 0;
	myfile.open(myfile_path);
	return 1;
}

int read_training_data()
{
	if (trained_normalized_left_images.size() > 0){
		cout << "error read after a train" << endl;
		return 0;
	}
	char filename[300];
	char imagename[300];
	for (int i = 0; i < 1000000; i++)
	{
		sprintf_s(filename, "%s/stereo%06d.yml", savepath, traincounter);
		sprintf_s(imagename, "%s/image%06d.png", savepath, traincounter);
		cout << i << endl;
		Mat img = imread(imagename, IMREAD_GRAYSCALE);
		if (!img.data)
		{
			cout << "Read " << traincounter << " files" << endl;
			return 1;
		}
		trained_normalized_left_images.push_back(img);
		kslam->add_normalized_image(img);
		PreProImg ppi =  read_one_file(filename);
		trained_points.push_back(ppi);
		traincounter++;
	}
	return 1;
}
// return index of saved triangulation data in trained_points.
// also normalizes the LEFT image.
int triangulate_and_save(Mat left_img, Mat right_img, Mat left_pose)
{
	high_resolution_clock::time_point t1 = high_resolution_clock::now();
	char filename[300];
	char imagename[300];
	sprintf_s(filename, "%s/stereo%06d.yml", savepath, traincounter);

	Mat patchnormalized;
	// Add a parameter here too
	preprocessImage(left_img, patchnormalized);
	trained_normalized_left_images.push_back(patchnormalized);
	sprintf_s(imagename, "%s/image%06d.png", savepath, traincounter);
	imwrite(imagename, patchnormalized);
	kslam->add_normalized_image(patchnormalized);

	FileStorage fs(filename, FileStorage::WRITE);

	PreProImg ppi = PreProImg();
	ppi.imgidx = traincounter;
	ppi.width = left_img.cols;
	ppi.height = left_img.rows;
	left_pose.copyTo(ppi.pose);

	vector<Point3f> points3d;
	Mat allColors(0, 0, CV_8UC3);
	int pointsfound = test_triangulate_linear(Mat(camleft), Mat(camright),
		left_img, right_img,
		ppi.kp1, ppi.kp2,
		ppi.desc1, ppi.desc2,
		gp, points3d, allColors, TRIANG_LINEAR);
	if (pointsfound < 10){
		cout << "error triangulating stereo" << endl;
		return -1;
	}
	ppi.points3d = Mat(points3d); // convert 3d point vector to a matrix 3 cols N rows
	fs << "MyData" << ppi; // write to file.
	fs.release();
	trained_points.push_back(ppi);
	high_resolution_clock::time_point t2 = high_resolution_clock::now();
	auto duratation = duration_cast<milliseconds>(t2 - t1).count();
	cout << duratation << "ms\n";
	traincounter++;
	return traincounter-1;
}

int processed_imgs = 0;
int do_single_step(Mat img)
{
	char filename[400];
	int best = kslam->find_best_from_train(img);
	if (best >= 0)
	{
		//int numMatches = kslam->correct_matches->size();
		//singleMatch *sm = &(kslam->correct_matches->at(kslam->correct_matches->size()-1));

		sprintf_s(filename, "C:/Matlab/savetest/stereo%06d.yml", best);
		//PreProImg ppi = read_one_file(filename);
		PreProImg ppi = trained_points[best];

		vector<KeyPoint> kp;
		Mat desc;
		// some parameters to take into the table (performance vs speed)
		lib_extract_grid_features_params(img, kp, gp);
		gp.detector->compute(img, kp, desc);
		//find possigle matches
		vector<DMatch> matches1;
		lib_match_descriptors(ppi.desc1, desc, matches1);
		lib_filter_matches(ppi.kp1, kp, matches1);

		Mat desc1filt, desc2filt;
		for (int i = 0; i < matches1.size(); i++)
		{
			desc1filt.push_back(ppi.desc1.row(matches1[i].queryIdx));
			desc2filt.push_back(desc.row(matches1[i].trainIdx));
		}

		vector<Point3f> poi3dfor3;
		for (int j = 0; j < matches1.size(); j++)
		{
			int match = matches1[j].queryIdx;
			Point3f p3f;
			p3f.x = ppi.points3d.at<float>(match, 0);
			p3f.y = ppi.points3d.at<float>(match, 1);
			p3f.z = ppi.points3d.at<float>(match, 2);
			poi3dfor3.push_back(p3f);
		}

		vector<Point2f> imgapoints;
		KeyPoint::convert(kp, imgapoints);
		Mat rvec, tvec;
		Mat pnpmask;
		solvePnPRansac(poi3dfor3, imgapoints, K, noArray(), rvec, tvec, false, 400, 4, 0.998999, pnpmask, SOLVEPNP_P3P); //CV_P3P
		Point3f tvecp = Point3f(-tvec.at<double>(0, 0), -tvec.at<double>(1, 0), -tvec.at<double>(2, 0));
		Mat Tout = Mat();
		Mat Rout;
		Rodrigues(rvec, Rout);
		relativePoses.push_back(Tout);
		relativeRotation.push_back(Rout);
		char text[300];
		sprintf_s(text, "%.4f,%.4f,%.4f, %d,%d\n", tvecp.x, tvecp.y, tvecp.z, best, processed_imgs);
		myfile << text;
		myfile.flush();

		cout << text << endl;
		//kslam->correct_matches->clear();
		processed_imgs++;

		if (0){
			/*viz::Viz3d myWindow("Coordinate Frame");
			/// Add coordinate axes
			myWindow.showWidget("Coordinate Widget", viz::WCoordinateSystem());
			Mat testt(ppi.points3d);
			viz::WCloud cws(testt);
			cws.setRenderingProperty(viz::POINT_SIZE, 3);
			myWindow.showWidget("CloudWidget2", cws);
			/// Add coordinate axes
			myWindow.showWidget("Coordinate Widget", viz::WCoordinateSystem());
			/// Let's assume camera has the following properties
			Mat rotm;
			Rodrigues(rvec, rotm);
			//cout << rotm << endl;
			Mat dir = Mat(3, 1, CV_64F);
			dir.at<double>(0, 0) = 0;
			dir.at<double>(1, 0) = 0;
			dir.at<double>(2, 0) = 1;
			Mat view = rotm*dir;
			// - tvec because solvepnp tvec points toward the origin
			// http://stackoverflow.com/questions/17423302/opencv-solvepnp-tvec-units-and-axes-directions
			//"Tvec points to the origin of the world coordinates in which You placed the calibration object. So if You placed the first object point in (0,0), thats where tvec will point to."
			Vec3d cam_pos(-tvec.at<double>(0, 0), -tvec.at<double>(1, 0), -tvec.at<double>(2, 0));
			Vec3d cam_focal_point(-tvec.at<double>(0, 0) + view.at<double>(0, 0),
				-tvec.at<double>(1, 0) + view.at<double>(1, 0),
				-tvec.at<double>(2, 0) + view.at<double>(2, 0));
			Vec3d cam_y_dir(0.0f, 1.0f, 0.0f);
			/// We can get the pose of the cam using makeCameraPose
			Affine3f cam_pose = viz::makeCameraPose(cam_pos, cam_focal_point, cam_y_dir);
			viz::WCameraPosition cpw(0.5); // Coordinate axes
			viz::WCameraPosition cpw_frustum(Vec2f(0.889484, 0.523599)); // Camera frustum
			myWindow.showWidget("CPW", cpw, cam_pose);
			myWindow.showWidget("CPW_FRUSTUM", cpw_frustum, cam_pose);
			myWindow.spinOnce(1, true);*/

			Mat towhow;
			drawKeypoints(img, kp, towhow);
			imshow("left_a img", towhow);
			char rightpath[400];
			sprintf(rightpath, "C:/Matlab/datasets/kitti/00/image_0/%06d.png", 407 + best);
			Mat toright;
			Mat rightimggg = imread(rightpath, IMREAD_GRAYSCALE);
			drawKeypoints(rightimggg, ppi.kp1, toright);
			imshow("rigigna", toright);
			waitKey();
		}
	}
	return 1;
}

int test_read_one_file(char *path, int idx)
{
	char filename[100];
	sprintf(filename, "C:/Matlab/savetest/stereo%06d.yml", idx);
	PreProImg ppi = read_one_file(filename);
	cout << ppi << endl;

	/// Create a window
	/*viz::Viz3d myWindow("Coordinate Frame");
	/// Add coordinate axes
	myWindow.showWidget("Coordinate Widget", viz::WCoordinateSystem());
	
	viz::WCloud cws(ppi.points3d);
	cws.setRenderingProperty(viz::POINT_SIZE, 3);
	myWindow.showWidget("CloudWidget2", cws);
	/// Add coordinate axes
	myWindow.showWidget("Coordinate Widget", viz::WCoordinateSystem());
	/// Let's assume camera has the following properties
	Vec3d cam_pos(1.0f, 0.0f, 0.0f), cam_focal_point(1, 0, 1), cam_y_dir(0.0f, 1.0f, 0.0f);
	/// We can get the pose of the cam using makeCameraPose
	Affine3f cam_pose = viz::makeCameraPose(cam_pos, cam_focal_point, cam_y_dir);
	viz::WCameraPosition cpw(0.5); // Coordinate axes
	viz::WCameraPosition cpw_frustum(Vec2f(0.889484, 0.523599)); // Camera frustum
	myWindow.showWidget("CPW", cpw, cam_pose);
	myWindow.showWidget("CPW_FRUSTUM", cpw_frustum, cam_pose);
	myWindow.spinOnce(1, true);*/
	
	char pathrightimg[100];
	sprintf(pathrightimg, "C:/Matlab/datasets/kitti/00/image_0/%06d.png", ppi.imgidx);
	Mat leftimg = imread(pathrightimg, IMREAD_GRAYSCALE);
	drawKeypoints(leftimg, ppi.kp1, leftimg);
	imshow("left",leftimg);
	waitKey(2);
	return 1;
}
