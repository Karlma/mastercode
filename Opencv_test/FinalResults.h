#ifndef FINAL_H
#define FINAL_H

#include "opencv2/core.hpp"
#include "opencv2/core/utility.hpp"


int run_orb_grid_results(cv::Mat image, int imgidx, int maxfeaturs, int fromcols, int tocols, float mindist);
int verify_stereo_matching(cv::Mat camera_left, cv::Mat camera_right, cv::Mat left, cv::Mat right, int imgidx);
int verify_triangulation(cv::Mat camera_left, cv::Mat camera_right, cv::Mat left, cv::Mat right);
int verify_pnp_with_odometry(cv::Mat camleft, cv::Mat camright);
int verify_odometry_threaded_kitti(cv::Mat camleft, cv::Mat camright);
int verify_odometry_threaded_diplod();
int verify_odometry_threaded_malaga();
#endif