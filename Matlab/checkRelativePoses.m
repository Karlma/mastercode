%% read data
data = textread('C:\Matlab\datasets\kitti\data_odometry_poses\dataset\poses\00.txt','','delimiter',' ');

[N,~] = size(data);
mats = zeros(3,4,N);

xyz = zeros(N,3);

for i = 1:N
   mats(:,:,i) = vec2mat(data(i,:),4); 
   xyz(i,:) = mats(:,4,i);
end
%%
transMats = zeros(4,4,N);

for i = 1:N
   transMats(1:3,1:4,i) = mats(:,:,i);
   transMats(4,1:4,i) = [0,0,0,1];
end

xdir = [1;0;0;1];
xdir3 = [1;0;0];
for i = 1:N
   dirs(:,i) = transMats(:,:,i)*xdir;
   dirs2(:,i) = xyz(i,1:3)' + transMats(1:3,1:3,i)*xdir3;
end

plot(xyz(:,1), xyz(:,3));
hold on;
plot(dirs2(1,:), dirs2(3,:)); 

%%
csv = csvread('C:\Matlab\rel_on_match_test_1454598501.csv');
poses = csv(:,1:3);
idxs = csv(:,4:5);
fasitp1 = [xyz(idxs(:,1),1),xyz(idxs(:,1),3)];
fasitp2 = [xyz(idxs(:,2),1),xyz(idxs(:,2),3)];

poseA = mats(:,:,idxs(:,1));
poseB = mats(:,:,idxs(:,2));
calcs = zeros(size(poses));
for i = 1:size(poses,1)
    curpose = [poses(i,:),1];
    frompose = [poseA(:,:,i);0,0,0,1];
    final = frompose * curpose';
    calcs(i,:) = final(1:3,1)';
end

plot(fasitp1(:,1),fasitp1(:,2),'r')
hold on
plot(calcs(:,1),calcs(:,3),'g')
hold on
plot(fasitp2(:,1),fasitp2(:,2),'b')

%% plot for stereo test
%plot(fasitp1(:,1),fasitp1(:,2))
hold on
plot(dirs2(1,idxs(:,1)),dirs2(3,idxs(:,1)));
plot(fasitp1(:,1)-poses(:,1),fasitp1(:,2)-poses(:,3))


%%
%3412,407
%3841,936
csv = csvread('C:/Matlab/pnp_odometry/fullruntest_1455192803.csv');
poses = csv(:,1:3);
idxs = csv(:,4:5);
idxs(:,1) = 407 + idxs(:,1);
idxs(:,2) = 3412 + idxs(:,2);
fasitp1 = [xyz(idxs(:,1),1),xyz(idxs(:,1),3)];
fasitp2 = [xyz(idxs(:,2),1),xyz(idxs(:,2),3)];

poseA = mats(:,:,idxs(:,1));
poseB = mats(:,:,idxs(:,2));
calcs = zeros(size(poses));
for i = 1:size(poses,1)
    curpose = [poses(i,:),1];
    frompose = [poseA(:,:,i);0,0,0,1];
    final = frompose * curpose';
    calcs(i,:) = final(1:3,1)';
end

plot(fasitp1(:,1),fasitp1(:,2),'r')
hold on
plot(calcs(:,1),calcs(:,3),'g')
hold on
plot(fasitp2(:,1),fasitp2(:,2),'b')




