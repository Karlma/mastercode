img = imread('C:\Users\Karl\Google Drive\Masteroppgave\bilder\leftSAD.png');
img2 = imread('C:\Users\Karl\Google Drive\Masteroppgave\bilder\rightSAD.png');
left = double(rgb2gray(img));
right = double(rgb2gray(img2));
figure(1);
imshow(left,[]);
title('left');

figure(2);
imshow(right,[]);
title('right');

start = [113, 42];
eend = [229, 42];

SADS = zeros(eend(1)-start(1),1);
leftp = left(start(2):start(2)+9, 168:168+9);
for i = start(1):eend(1)
    rightp = right(start(2):start(2)+9, i:i+9);
    abss = abs(leftp-rightp);
    SADS(i-start(1)+1) = sum(abss(:));
end
rightp = right(start(2):start(2)+9, start(1) + 12:start(1) + 12+9);
figure(3);
plot(SADS);
title('SAD scores');
ylabel('Search window offset');
xlabel('SAD score');