%% plot path
%data = textread('C:\Users\Karl\Google Drive\Masteroppgave\results\fullSubPixel\00.txt','','delimiter',' ');
data = textread('C:\Matlab\results\full\plot_path\00.txt','','delimiter',' ');

figure(1);
%plot(data(:,1),data(:,2));
hold on
plot(data(:,3),data(:,4));
title('Without loopclosure');
ylabel('Z [m]');
xlabel('X [m]');


%% plot path

path = 'C:\Matlab\results\noloop\plot_error\00';

data = textread([path '_rl.txt'],'','delimiter',' ');
figure(2)
subplot(2,2,1);
plot(data(:,1),data(:,2)*57.3);
title('Rotation Error');
ylabel('Rotation Error [deg/m]');
xlabel('Path Length [m]');

data = textread([path '_tl.txt'],'','delimiter',' ');
subplot(2,2,2);
plot(data(:,1),data(:,2)*100);
title('Translation Error');
ylabel('Translation Error %');
xlabel('Path Length [m]');

data = textread([path '_rs.txt'],'','delimiter',' ');
subplot(2,2,3);
plot(data(:,1)*3.6,data(:,2)*100);
title('Rotation Error');
ylabel('Rotation Error [deg/m]');
xlabel('Speed [km/h]');

data = textread([path '_ts.txt'],'','delimiter',' ');
subplot(2,2,4);
plot(data(:,1)*3.6,data(:,2)*57.3);
title('Translation Error');
ylabel('Translation Error %');
xlabel('Speed [km/h]');


