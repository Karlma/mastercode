

%% 00 02 05
data = textread('C:\Matlab\datasets\kitti\data_odometry_poses\dataset\poses\00.txt','','delimiter',' ');

[N,~] = size(data);
mats = zeros(3,4,N);

xyz = zeros(N,3);

for i = 1:N
   mats(1,:,i) = data(i,1:4); 
   mats(2,:,i) = data(i,5:8);
   mats(3,:,i) = data(i,9:12); 
   xyz(i,:) = mats(:,4,i);
end
plot(xyz(:,1),xyz(:,3));
%%
bestm = cell(size(xyz,1),1);
mindist = 3;
for i = 1:size(xyz,1)
    xy1 = [xyz(i,1),xyz(i,3)];
    for j = 1:i-100
        xy2 = [xyz(j,1),xyz(j,3)];
        dist = norm(xy1-xy2);
        if dist < mindist
            bestm{i} = [bestm{i},j];
        end
    end
end

%%

csv = csvread('C:\\Matlab\\kslamsseq00\\matches_5_360_1461652333.csv');
csv = csvread('C:\\Matlab\\kslamspyr00\\matches_5_610_1461714319.csv');
%csv = csvread('C:\\Matlab\\ksalm\\matches_15_44_1456358588.csv');

figure(1);
%subplot(1,2,2);
plot(xyz(:,1),xyz(:,3));
hold on
for i = 1 : size(csv,1)
    if csv(i,1) < N
        a = [xyz(csv(i,1)+1,1),xyz(csv(i,2)+1,1)];
        b = [xyz(csv(i,1)+1,3),xyz(csv(i,2)+1,3)];
        plot(a,b);
    end
end
title('KSLAM');
ylabel('Z [m]');
xlabel('X [m]');


%%
allcorrmatchcount = 0;
for i = 1:size(bestm,1)
    if size(bestm{i},2) > 0
        allcorrmatchcount = allcorrmatchcount+1;
    end
end

%Seqslam does not find the BEST match but only the on in a trajectory from
%the search,,...... 

%%
data = textread('C:\Matlab\datasets\kitti\data_odometry_poses\dataset\poses\00.txt','','delimiter',' ');

[N,~] = size(data);
mats = zeros(3,4,N);

xyz = zeros(N,3);

for i = 1:N
   mats(1,:,i) = data(i,1:4); 
   mats(2,:,i) = data(i,5:8);
   mats(3,:,i) = data(i,9:12); 
   xyz(i,:) = mats(:,4,i);
end
plot(xyz(:,1),xyz(:,3));

allcorrmatchcount = 0;
for i = 1:size(bestm,1)
    if size(bestm{i},2) > 0
        allcorrmatchcount = allcorrmatchcount+1;
    end
end

ii = 0;
allpres = [];
allrecs = [];
%for fi = 280 : 40 : 520
for fi = 500 : 10 : 640
%for fi = 34 : 1 : 49
    %st = sprintf('C:\\Matlab\\kslamsseq00\\matches_20_%d_1461280435.csv',fi);
    %st = sprintf('C:\\Matlab\\kslamsseq00\\matches_5_%d_1461620838.csv',fi);
    %st = sprintf('C:\\Matlab\\kslamsseq00\\matches_10_%d_1461622475.csv',fi);
    st = sprintf('C:\\Matlab\\kslamspyr00\\matches_15_%d_1461714319.csv',fi);
    %st = sprintf('C:\\Matlab\\kslamsseq02\\matches_20_%d_1461277096.csv',fi);
    %st = sprintf('C:\\Matlab\\kslamsseq05\\matches_20_%d_1461279088.csv',fi);
    %st = sprintf('C:\\Matlab\\kslamsseq00\\matches_20_%d_1461284575.csv',fi);
    csv=csvread(st);
    csv = csv + 1;
    %plot(xyz(:,1),xyz(:,3));
    wrongmatch  = 0;
    nomatchwrong = 0;
    correctcount = 0;
    %hold on
    for i = 1 : size(csv,1)
        if csv(i,1) < N 
            if size(bestm{csv(i,1)},2) > 0 
                if any(bestm{csv(i,1)} == csv(i,2))
                    correctcount = correctcount+1;
                else
                    wrongmatch = wrongmatch + 1;
                end
            else
                nomatchwrong = nomatchwrong + 1;
            end
        else
            nomatchwrong = nomatchwrong + 1;
        end
        %a = [xyz(csv(i,1),1),xyz(csv(i,2),1)];
        %b = [xyz(csv(i,1),3),xyz(csv(i,2),3)];
        %plot(a,b);
    end
    totalwrong = wrongmatch + nomatchwrong;
    precition = ((correctcount)/(size(csv,1)))*100
    recall = (correctcount/allcorrmatchcount)*100
    allrecs = [allrecs,recall];
    allpres = [allpres,precition];
end


plot(allrecs,allpres,'-o');
%axis([0,95,48,102]);
title('Precision/Recall for pyramid');
xlabel('Recall');
ylabel('Precision');

%%
plot(r00,p00,'-o');
axis([30,100,48,102]);
hold on
plot(r02,p02,'-o');
plot(r05,p05,'-o');
title('Precision/Recall of KSLAM');
xlabel('Recall');
ylabel('Precision');
legend('Seq00','Seq02','Seq05');
%% eval Lip 6 dataset
load('C:\Matlab\datasets\Lip6OutdoorDataSet\Lip6OutdoorGroundTruth.mat');
allcorrmatchcount = 600; %sum(truth(:)>0);
ii = 0;
allpres = [];
allrecs = [];

for fi = 0 : 9
    image = zeros(size(truth));
    st = sprintf('C:\\Matlab\\ksalm\\test2\\matches_lip6_%d.csv',fi);
    csv=csvread(st);
    %plot(xyz(:,1),xyz(:,3));
    correctcount = 0;
    wrongmatch  = 0;
    nomatchwrong = 0;
    %hold on
    for i = 1 : size(csv,1)
        image(csv(i,1)+1,csv(i,2)+1) = 1;
        if truth(csv(i,1)+1,csv(i,2)+1) > 0
            correctcount = correctcount+1;
        else
            wrongmatch = wrongmatch + 1;
        end
        %a = [xyz(csv(i,1),1),xyz(csv(i,2),1)];
        %b = [xyz(csv(i,1),3),xyz(csv(i,2),3)];
        %plot(a,b);
    end
    imshow(image);
    pause;
    totalwrong = wrongmatch + nomatchwrong;
    precition = ((correctcount)/(correctcount + wrongmatch))*100
    recall = (correctcount/allcorrmatchcount)*100
    allrecs = [allrecs,recall];
    allpres = [allpres,precition];
end


plot(allrecs,allpres,'-o');
%axis([0,9,0,10]);
title('Precision/Recall of KSLAM');
xlabel('Recall');
ylabel('Precision');
