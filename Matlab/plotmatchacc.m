data = textread('C:\Matlab\mathcing\stereo_vary_grid_BF_12_1_24.csv','','delimiter',',');
plot(data(:,2),(data(:,3)./data(:,2)).*100);
hold on
data = textread('C:\Matlab\mathcing\stereo_vary_grid_SAD_12_1_24.csv','','delimiter',',');
plot(data(:,2),(data(:,3)./data(:,2)).*100);

title('Percentage of matches correct 12*6 cells');
ylabel('Precition [%]');
xlabel('Features');
ylim([0 100])
legend('Brute force','SAD');
%%
data = textread('C:\Matlab\mathcing\stereo_vary_grid_SAD_sp_12_1_24.csv','','delimiter',',');
plot(data(:,3),data(:,5));
hold on
data = textread('C:\Matlab\mathcing\stereo_vary_grid_SAD_12_1_24.csv','','delimiter',',');
plot(data(:,3),data(:,5));

title('Percentage of matches correct 12*6 cells');
ylabel('Projection error');
xlabel('Features');
legend('Brute force','SAD');
%% plot many

data = [];
corrmatch = zeros(42,34);
totalmatch = zeros(42,34);
reerr = zeros(42,34);
for i = 0:42
    st = sprintf('C:\\Matlab\\mathcing\\stereo_vary_grid_BF_%d.csv',i*15);
    data = textread(st,'','delimiter',','); 
    corrmatch(i+1,:) = data(:,3);
    totalmatch(i+1,:) = data(:,2);
    reerr(i+1,:) = data(:,5);
end

%%
errorbar(data(:,1),mean(reerr),std(reerr));
title('Reprojection error');
xlabel('feature per cell') % label x-axis
ylabel('Error in pixels') % label left y-axis

%%
errorbar(data(:,1),mean((corrmatch./totalmatch)*100),std((corrmatch./totalmatch)*100));
title('Percentage correct matches');
xlabel('feature per cell') % label x-axis
ylabel('correct matches [%]') % label left y-axis
