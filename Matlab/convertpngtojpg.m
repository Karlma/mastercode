
% Get list of all BMP files in this directory
% DIR returns as a structure array.  You will need to use () and . to get
% the file names.
imagefiles = dir('C:/Matlab/datasets/kitti/00/image_0/*.png');
nfiles = length(imagefiles);    % Number of files found
for ii=1:nfiles
   currentfilename = imagefiles(ii).name;
   currentimage = imread(strcat('C:/Matlab/datasets/kitti/00/image_0/',currentfilename));
   imwrite(currentimage, strcat('C:/Matlab/datasets/kitti/00/image_0_jpg/',strrep(currentfilename,'png','jpg')));
   ii
end
%% Lip 6 dataset ppm -> png
imagefiles = dir('C:/Matlab/datasets/Lip6OutdoorDataSet/Images/*.ppm');
nfiles = length(imagefiles);    % Number of files found
for ii=1:nfiles
   currentfilename = imagefiles(ii).name;
   currentimage = imread(strcat('C:/Matlab/datasets/Lip6OutdoorDataSet/Images/',currentfilename));
   imshow(currentimage,[]);
   imwrite(currentimage, strcat('C:/Matlab/datasets/Lip6OutdoorDataSet/Images_png/',strrep(currentfilename,'ppm','png')));
   ii
end