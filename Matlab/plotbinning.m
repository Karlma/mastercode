%% plot results of binning

data = textread('C:\Matlab\results\grid\grid_data_1_24_0_2000.csv','','delimiter',',');

figure(1);
[ax,p1,p2] = plotyy(data(:,1),data(:,6),data(:,1),data(:,7));
title('Binning of points');
xlabel(ax(1),'Grid columns') % label x-axis
ylabel(ax(1),'Grid count STD') % label left y-axis
ylabel(ax(2),'Time [ms]') % label right y-axis

%% plot several

data = [];
data2 = zeros(100,11);
for i = 0:99
    st = sprintf('C:\\Matlab\\results\\grid\\grid_data_%d.csv',i);
    data = textread(st,'','delimiter',','); 
    data2(i+1,:) = data(:,6);
end

figure(1);
errorbar(data(:,1),mean(data2),std(data2));
title('Binning of points');
xlabel('Grid columns') % label x-axis
ylabel('Grid count STD') % label left y-axis
xlim([0 21])

%% testing


left = [7.188560000000e+02, 0.000000000000e+00, 6.071928000000e+02, 0.000000000000e+00;
		0.000000000000e+00, 7.188560000000e+02, 1.852157000000e+02, 0.000000000000e+00;
		0.000000000000e+00, 0.000000000000e+00, 1.000000000000e+00, 0.000000000000e+00];
    
right = [7.188560000000e+02, 0.000000000000e+00, 6.071928000000e+02, -3.861448000000e+02;
		0.000000000000e+00, 7.188560000000e+02, 1.852157000000e+02, 0.000000000000e+00;
		0.000000000000e+00, 0.000000000000e+00, 1.000000000000e+00, 0.000000000000e+00];
 
X = [0;0;20;1];
x = right*X;
x = x/x(3)


