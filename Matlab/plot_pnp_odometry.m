%% read data
data = textread('C:\Matlab\datasets\kitti\data_odometry_poses\dataset\poses\00.txt','','delimiter',' ');

[N,~] = size(data);
mats = zeros(3,4,N);

xyz = zeros(N,3);

for i = 1:N
   mats(1,:,i) = data(i,1:4); 
   mats(2,:,i) = data(i,5:8); 
   mats(3,:,i) = data(i,9:12); 
   xyz(i,:) = mats(:,4,i);
end
plot(xyz(:,1), xyz(:,3));
%% a
transMats = zeros(4,4,N);

for i = 1:N
   transMats(1:3,1:4,i) = mats(:,:,i);
   transMats(4,1:4,i) = [0,0,0,1];
end

xdir = [1;0;0;1];
xdir3 = [1;0;0];
for i = 1:N
   dirs(:,i) = transMats(:,:,i)*xdir;
   dirs2(:,i) = xyz(i,1:3)' + transMats(1:3,1:3,i)*xdir3;
end

plot(xyz(:,1), xyz(:,3));
hold on;
plot(dirs2(1,:), dirs2(3,:)); 
%%
%csv = csvread('C:\Matlab\pnp_odometry\test_1455484521.csv');
%csv = csvread('C:\Matlab\pnp_odometry\test_1455154581.csv');
csv = csvread('C:\Users\Karl\Google Drive\Masteroppgave\results\fullSubPixel\00.txt');

poses = csv(:,7:9);
idxs = csv(:,10:11);
fasit = [xyz(idxs(:,2)+1,1),xyz(idxs(:,2)+1,2),xyz(idxs(:,2)+1,3)];
poseA = mats(:,:,idxs(:,1)+1);

plot(fasit(:,1),fasit(:,3),'r')
hold on
plot(-poses(:,1),poses(:,3),'g')
%% plot relative poses with matches
csv = csvread('C:\Matlab\rel_on_match_test_1456403494.csv');
%csv = csvread('C:\Matlab\pnp_odometry\test_1455154581.csv');
csv = sortrows(csv,5);
poses = csv(:,1:3);
idxs = csv(:,4:5);
fasita = [xyz(idxs(:,1)+1,1),xyz(idxs(:,1)+1,2),xyz(idxs(:,1)+1,3)];
fasitb = [xyz(idxs(:,2)+1,1),xyz(idxs(:,2)+1,2),xyz(idxs(:,2)+1,3)];
poseA = mats(:,:,idxs(:,1)+1);

plot(fasita(:,1),fasita(:,3),'r')
hold on
plot(fasitb(:,1),fasitb(:,3),'b')
finalposees = [0,0,0];
for i = 1 : size(poses,1)
   aa = [poses(i,:)';1];
   pp = [mats(:,:,idxs(i,1));0,0,0,1] * aa;
   finalposees = [finalposees;pp(1:3,1)'];
end
plot(finalposees(:,1),finalposees(:,3),'g')
title('relative Pose');
ylabel('Z [m]');
xlabel('X [m]');
legend('First Run','Second Run', 'relativepose');

%%
match = 0
for i = 1:size(bestm)
    if size(bestm{i},2) > 0
       match = i
       break
    end
end

a = match+10;
b = bestm{a}(1);
aa = [xyz(b,1),-poses(a,1)];
bb = [xyz(b,3),poses(a,3)];
hold on
plot(aa,bb);

corr = [xyz(b,1), xyz(b,3)];
my = [-poses(a,1), poses(a,3)];
diffe = corr-my;
corrposes = poses;
corrposes(:,1) = -corrposes(:,1);
counter = 0;
goback = 700;
for i = a-goback:a
   counter = counter + 1;
   corrposes(i,1)= corrposes(i,1) + diffe(1,1)*(counter/goback);
   corrposes(i,3)= corrposes(i,3) + diffe(1,2)*(counter/goback);
end
for i = a+1:size(poses,1)
   corrposes(i,1)= corrposes(i,1) + diffe(1,1);
   corrposes(i,3)= corrposes(i,3) + diffe(1,2);
end

plot(fasit(:,1),fasit(:,3),'r')
hold on
plot(corrposes(:,1),corrposes(:,3),'g')

%%
fasit_rel = csv(:,1:3);
rel = csv(:,4:6);
diff = abs(fasit_rel - rel);
M = mean(diff)
S = std(diff)
MA = max(diff)
MI = min(diff)



%%
%% read data
data = textread('C:\Matlab\datasets\kitti\data_odometry_poses\dataset\poses\00.txt','','delimiter',' ');

[N,~] = size(data);
mats = zeros(3,4,N);

xyz = zeros(N,3);

for i = 1:N
   mats(1,:,i) = data(i,1:4); 
   mats(2,:,i) = data(i,5:8); 
   mats(3,:,i) = data(i,9:12); 
   xyz(i,:) = mats(:,4,i);
end
%plot(xyz(:,1), xyz(:,3));
%mycsv = csvread('C:/masterrepo/mastercode/Results/first/00.txt');  
%mycsv = textread('C:/Matlab/custom_kslam_relative/00_kslam_kalman.txt','','delimiter',' ');
mycsv = textread('C:/Matlab/newrun2/03.txt','','delimiter',' ');
mycsv = textread('C:/Matlab/only_odometry/02.txt','','delimiter',' ');
%mycsv = textread('C:/Matlab/pnp_odometry/relative_00_1455748030.txt','','delimiter',' ');
%mycsv = textread('C:/Matlab/malag/final5.txt','','delimiter',' ');
%mycsv = textread('C:/Matlab/pnp_odometry/final_1455661288.txt','','delimiter',' ');
%mycsv = textread('C:\Users\Karl\Google Drive\Masteroppgave\results\stereo5\00KALMAN.txt','','delimiter',' ');

mymats = zeros(3,4,size(mycsv,1));
myxyz = zeros(size(mycsv,1),3);
for i = 1:size(mycsv,1)
   mymats(1,:,i) = mycsv(i,1:4); 
   mymats(2,:,i) = mycsv(i,5:8);    
   mymats(3,:,i) = mycsv(i,9:12);
   myxyz(i,:) = mymats(:,4,i);
end
%plot(myxyz(:,1),myxyz(:,3));
%plot(myxyz(:,3));
%hold on
%plot(xyz(:,3));
%plot3(myxyz(:,1),myxyz(:,2), myxyz(:,3));


figure(1);
%subplot(2,2,4);
plot(myxyz(:,1),myxyz(:,3));
hold on
plot(xyz(1:size(myxyz,1),1),xyz(1:size(myxyz,1),3));
title('Paths');
ylabel('Y [m]');
xlabel('X [m]');
legend('Odometry','GPS');

figure(2);
plot3(myxyz(:,1),myxyz(:,2), myxyz(:,3));
%hold on;
%plot3(xyz(1:size(myxyz,1),1),xyz(1:size(myxyz,1),2),xyz(1:size(myxyz,1),3));
%title('3d plot');
%legend('Odometry','GPS');


%% relative,  subpixel,  kalman_no_loop
figure(2);
plot(xyz(1:size(myxyz,1),1),xyz(1:size(myxyz,1),3));
hold on
plot(kalman(:,1),kalman(:,3));
plot(subpix(:,1),subpix(:,3));
plot(nomal(:,1),nomal(:,3));
title('Paths');
ylabel('Y [m]');
xlabel('X [m]');
legend('GPS','Kalman','Sub pixel','Standard');
