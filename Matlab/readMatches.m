%% read data
data = textread('C:\Matlab\datasets\kitti\data_odometry_poses\dataset\poses\00.txt','','delimiter',' ');

[N,~] = size(data);
mats = zeros(3,4,N);

xyz = zeros(N,3);

for i = 1:N
   mats(:,:,i) = vec2mat(data(i,:),4); 
   xyz(i,:) = mats(:,4,i);
end

%% 
csv = csvread('C:\Matlab\matches_00_25100.csv');
plot(xyz(:,1),xyz(:,3));
hold on
for i = 1 : size(csv,1)
    a = [xyz(csv(i,1),1),xyz(csv(i,2),1)];
    b = [xyz(csv(i,1),3),xyz(csv(i,2),3)];
    plot(a,b);
end

%% test somethin
P1 = [7.188560000000e+02, 0.000000000000e+00, 6.071928000000e+02, 0.000000000000e+00;
			0.000000000000e+00, 7.188560000000e+02, 1.852157000000e+02, 0.000000000000e+00;
			0.000000000000e+00, 0.000000000000e+00, 1.000000000000e+00, 0.000000000000e+00];
        
P2 = [ 7.188560000000e+02, 0.000000000000e+00, 6.071928000000e+02, -3.861448000000e+02;
			0.000000000000e+00, 7.188560000000e+02, 1.852157000000e+02, 0.000000000000e+00;
			0.000000000000e+00, 0.000000000000e+00, 1.000000000000e+00, 0.000000000000e+00];

F = vgg_F_from_P(P1,P2);
F

vgg_


%% check odometry
csv = csvread('C:\Matlab\mydata2.csv');
plot(xyz(1:499,1),xyz(1:499,3));
hold on;
plot(csv(1:499,1),-csv(1:499,3));
